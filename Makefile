# Top level make targets
build: packages thesis.pdf

# Dependencies for thesis.tex
.PRECIOUS: thesis.tex
thesis.tex: \
    chapter_introduction.tex \
    chapter_magnetostriction.tex \
    chapter_magneto_dislocations.tex \
    chapter_crystal_defects.tex \
    chapter_fem_formulation.tex \
    chapter_electron_holography.tex \
    chapter_discussion_and_conclusions.tex \
    bibliography.bib \
    hyphenations.tex




BUILD_DIR=build

TEXINPUTS=.:./packages/xypic/texinputs:
T1FONTS=.:./packages/xypic/type1:
TFMFONTS=.:./packages/xypic/texfonts:

LATEX=pdflatex
LATEX_FLAGS=-halt-on-error


#
# Build Rules
#

MKDIR_P=mkdir -p
RM=rm -rf
CP=cp -f
TOUCH=touch
UNZIP_O=unzip -o

.PHONY: build
build:

clean:
	$(RM) $(BUILD_DIR)


.SUFFIXES:
.SUFFIXES: .pdf .tex

%.tex:
	$(TOUCH) $@

%.pdf: %.tex
	-$(MKDIR_P) $(BUILD_DIR)
	TEXINPUTS=$$TEXINPUTS:$(TEXINPUTS) \
	TFMFONTS=$$TFMFONTS:$(TFMFONTS) \
	T1FONTS=$$T1FONTS:$(T1FONTS) \
	    $(LATEX) -interaction=nonstopmode -draftmode -output-directory=$(BUILD_DIR) $(LATEX_FLAGS) $<
	cd $(BUILD_DIR); BIBINPUTS=.. biber $*
	TEXINPUTS=$$TEXINPUTS:$(TEXINPUTS) \
	TFMFONTS=$$TFMFONTS:$(TFMFONTS) \
	T1FONTS=$$T1FONTS:$(T1FONTS) \
	    $(LATEX) -interaction=nonstopmode -draftmode -output-directory=$(BUILD_DIR) $(LATEX_FLAGS) $<
	TEXINPUTS=$$TEXINPUTS:$(TEXINPUTS) \
	TFMFONTS=$$TFMFONTS:$(TFMFONTS) \
	T1FONTS=$$T1FONTS:$(T1FONTS) \
	    $(LATEX) -interaction=nonstopmode -output-directory=$(BUILD_DIR) $(LATEX_FLAGS) $<
	$(CP) $(BUILD_DIR)/$@ .

# Setup building packages
.PHONY: packages
packages:

PACKAGES_DIR=./packages
TEXINPUTS=.:
T1FONTS=.:
TFMFONTS=.:

# Install xypic in packages
TEXINPUTS:=$(PACKAGES_DIR)/xypic/texinputs:$(TEXINPUTS)
T1FONTS:=$(PACKAGES_DIR)/xypic/type1:$(T1FONTS)
TFMFONTS:=$(PACKAGES_DIR)/xypic/texfonts:$(TFMFONTS)
XYPIC_DIR=$(PACKAGES_DIR)/xypic
XYPIC_PACKAGE=$(XYPIC_DIR)/texinputs/xypic.sty

packages: $(XYPIC_PACKAGE)

$(XYPIC_PACKAGE):
	cd $(PACKAGES_DIR) && $(UNZIP_O) xypic.zip
