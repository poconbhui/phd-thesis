# Understanding How Rocks Record Changes In The Earth's Magnetic Field

The primary focus of this thesis is including magnetostriction in
micromagnetic models.
With the inclusion of magnetistriction comes the ability to model how
mechanical effects eg. deformation and crystal defects, affect
the magnetic properties of a grain.
Of perticular interest is the effect in the Pseudo-Single Domain (PSD) regime
where the magnetization of a grain is not easily described in terms discrete
regions of uniform magnetization, making it difficult to find analytical
solutions to the governing equations.

Other points of interest include tools developed to generate models from
experimental data, and tools to compare models to experimental data.
