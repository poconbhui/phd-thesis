\chapter{Introduction}

Paleomagnetism is the study of changes in the Earth's magnetic field using
magnetic recordings from rocks.
Rock magnetism is the study of the behaviour of magnetic rocks.
Naturally, rock magnetism is often employed in paleomagnetism to
extract the recordings from the magnetic rocks.
Natural samples tend to be imperfect: they are irregularly shaped, embedded in
larger materials under pressure, twinned with other crystals.
They are part of, and contain, intergrowths of other materials, and
contain dislocations and inclusions.
All of these contribute to internal stresses in the material \cite{Appel1984}.
To account for these, we need techniques for modelling magnetic materials, for
modelling inhomogeneous magnetic materials, and for modelling the effects of
internal stresses and other mechanical deformations on the magnetization.

Ferromagnetic minerals with a cubic crystal structure are an abundant source
of magnetic signal in natural rock samples.
Iron oxide and iron titanium oxide minerals are particularly abundant, as in
the ulv\"ospinel-magnetite and ilmenite-hematite series \cite{Tauxe2010}.
In this thesis, we will be mainly concerned with modelling magnetite and
titanomagnetite with Ti substitution factor of 0.6, or TM60, as these are two
are very commonly found in natural samples.
We will be looking at magnetite and oxidized shells of magnetite for
homogeneous and inhomogeneous materials.
We will then look at TM60 when including the mechanical deformation of
materials into micromagnetic models, as this material has a particularly
strong magneto-mechanical coupling.
Finally, we will look at a magnetite-ilmenite lamellar structure for
an inhomogeneous model which includes internal stresses due to incompatible
lattice spacings of the two materials.

In this chapter, we will present some history and motivating factors for
studying three main topics: micromagnetism, magnetostriction, and electron
holography.
In brief, micromagnetism is the primary tool we will be using to model
magnetic grains, magnetostriction is the phenomenon we will be adding and
using to include imperfections, and electron holography is a tool that can
be used to verify our predictions.



\section{Micromagnetism}
% Micromagnetic modelling
%% Perfect
%% Equations already hard
%%% Demag, Exchange, Anis, Ext

Micromagnetism is a mathematical technique for describing the magnetic state
of a material in terms of a continuous ``magnetization'' \cite{Kittel1949}.
This is distinct from the actual picture of discrete, free electrons each
contributing to the overall magnetic state.
By using a continuous function and adapting the governing equations of the
physics to a continuous function, the problem becomes more tractable.
In particular, it allows the equations of motion of the system to be posed in
terms of a set of partial differential equations (PDE).
In this manner, describing the behaviour of the system, and finding magnetic
states which will remain stable over long periods of time can be expressed
as well studied PDEs.

As a very brief overview, the most basic elements needed to calculate the
energy density, $f$, of a cubic ferromagnetic crystal due to a magnetization
$\Vector{M}$ is
\begin{equation}
\begin{aligned}
    f &=
        -K_1\frac{1}{2}(\alpha_1^4+\alpha_2^4+\alpha_3^4)
        + A (\partial_i \hat{M}_j)(\partial_i \hat{M}_j)
        + (\partial_i \phi) M_i
        + \Vector{H}^\text{z} \cdot \Vector{M}
    \\
    \partial_i \partial_i \phi &= \partial_i M_i
    \\
    \alpha_1 &= \hat{M} \cdot [100]\ ,
    \quad
    \alpha_2 = \hat{M} \cdot [010]\ ,
    \quad
    \alpha_3 = \hat{M} \cdot [001]
    \\
    M_i M_i &= M_s
\end{aligned}
\end{equation}
where
    $K_1$ is the cubic anisotropy constant,
    $A$ is the exchange constant,
    $\phi$ is the magnetic scalar potential,
    $\Vector{H}^\text{z}$ is the Zeeman / external field,
    $\alpha_i$ is the directional cosine of the magnetization with
        the $i$th cubic axis,
    and $M_s$ is the saturation magnetization, a constant.

Each of these terms has a rich and detailed background, important for
understanding the behaviour and stability of these remanent magnetizations.
In this thesis, however, we are more concerned with simply calculating them
and studying the results.
For an in depth review, Kittel's paper \cite{Kittel1949} is essential.
In particular, we are interested in finding remanent magnetizations.
A remanent magnetization is an $\Vector{M}$ which locally minimizes $f$.
By locally minimize, we mean $\Vector{M}$ is a value such that small
perturbations to $\Vector{M}$ result in a larger $f$,
i.e. $f(\Vector{M}+\Vector{\epsilon}\,) \ge f(\Vector{M})$ for any small
$\Vector{\epsilon}$.
A stable remanent magnetization is an $\Vector{M}$ such that the value of $f$
continues to increase even at relatively large perturbations of $\Vector{M}$.
It should be clear from the presented energy, particularly with the presence of
the $\hat{M}$ and $M_i M_i = M_s$ terms representing non-linear components of
the energy, that finding remanent magnetizations is not a straightforward task.

Rock magnetism benefits greatly from micromagnetism.
Initially, micromagnetism allowed analytic solutions to be found for the
behaviour and stability of perfect, regularly shaped grains, with uniform
magnetization as found in work by \textcite{Neel1949} and \textcite{Kittel1949}.
Uniformly magnetized grains are also called single domain (SD).
Of particular interest here are the relaxation equations by \textcite{Neel1949}
which predict the superparamagnetic (SP) threshold.
The SP threshold is a grain size below which the thermal energy of a rock 
is of the same order of magnitude of the energy barrier between remanent
magnetic states.
As a result, the grain may spontaneously change magnetic states at a timescale
which would render the magnetic recordings unreliable.
As SP grains are a subset of SD grains, this means not all SD grains are
useful recorders of magnetic signal.

From there, models such as the Amar model \cite{Amar1958} allowed researchers
to reason about multidomain (MD) grains, and the Morrish and Yu model
\cite{Morrish1955} allowed researchers to reason about single vortex (SV)
states, in regular geometries.
These single vortex states fall under a category of states called
pseudo-single domain (PSD) \cite{Butler1975}, so named because they tend to
behave similarly to single domain states.
In particular, it was found that PSD states, in which two domain (TD) states are
sometimes also included, make up a significant portion of the stable magnetic
signal of natural grains \cite{Moskowitz1980}, and in some cases the SD
range can be very narrow \cite{Butler1975}, leaving PSD states as the dominant
carriers.
This is matched by experimental observations \cite{Day1977}.
These analytic approaches, however, suffer from many simplifications to
make the equations tractable.

The introduction of 3D numerical micromagnetic models to rock magnetism
allowed for reasoning about remanent states
of irregular geometries with non-uniform magnetizations \cite{Williams1989}.
The introduction of the finite element method (FEM) \cite{Davies2011} to
micromagnetic modelling \cite{Fredkin1987} allowed more accurate consideration
of curved surfaces, such as spheres and surface effects.
In recent years, an easy to use micromagnetics package, MERRILL, has been
released, targeted at the paleomagnetic community.

In this thesis, we will derive the energies and effective fields calculated
by MERRILL when looking for remanent magnetizations.
This will act as an introduction to the finite element method, and as
documentation of the code for MERRILL.
It will also present a technique used in MERRILL for handling infinite
domains using finite computer space.
With the derivations done here, it will be easier to see how later derivations
of magnetoelastic and magneto-dislocation energies might be integrated into
MERRILL.

Initially, the energies and effective fields will be derived for a single-phase
material.
That is, a geometry whose material parameters are constant throughout.
We will then derive an extension of these to a multi-phase material.
A multi-phase material has piece-wise constant material parameters.
We will not, however, present the optimization and minimization techniques
used by MERRILL to find the remanent magnetizations.

After the derivation, we will present some example results using MERRILL.
We will present MERRILL's solution to the Standard Problem No. 3 posed by
the $\mu$MAG group at NIST \cite{muMag2017}.
This will act as a verification that MERRILL works as expected for single-phase
materials.
Then we will present a result for a core-shell model of a truncated octahedron
of magnetite with a hematite shell.


\section{Magnetostriction}

Magnetostriction describes a phenomenon where the magnetization can have
an effect on the stress and strain of a magnetic material, and vice versa
\cite{Kittel1949}.
In brief, when a ferromagnet is magnetized along a particular direction,
the material tends to stretch along that direction.
Typical micromagnetic models tend to simplify magnetostriction by including
a term suitable for a uniform magnetization.
In the case of a uniformly magnetized, cubic, ferromagnetic crystal, the effect
of magnetostriction can be bundled into the anisotropy term, as described by
\textcite{Kittel1949}.
However, this approximation is not, as we will show, appropriate for
non-uniformly magnetized materials.

Another approximation used for the application of magnetostriction is
Kittel's effective uniaxial anisotropy for a uniformly magnetized ferromagnet
under a uniaxial tension.
Various treatments have used this approach, substituting the cubic anisotropy
for this effective uniaxial anisotropy, to determine the behaviours of
domain walls for highly magnetostrictive materials

In this thesis, we will derive and present a subset of Brown's magnetostrictive
equations of motion \cite{Brown1966} which should be sufficient for
micromagnetic modelling of nano-scale ferromagnets in low magnetic fields, and
derive a FEM formulation of these.
We will also present some preliminary modelling results, outlining how
magnetostriction deforms materials for non-uniform magnetizations, and
how including a full description of magnetostriction effects remanent
magnetizations.

This formulation of magnetostriction will be the first step needed to model
the natural rock samples we've described.
The effects of material intergrowths, embedding in rocky matrices, materials
under stress, can all be described in terms of mechanical effects, of stresses
and strains on the materials.
These mechanical effects then affect the magnetic behaviour of the material
via magnetostriction.
By coupling this with the multi-phase description of magnetic materials
discussed in the previous section, we can present a theory capable of
modelling imperfect magnetic crystals.


\section{Electron Holography}
% Electron holography
%% Experimentally determine structures
%% Verifying modelling

An important step to ensuring the theoretical predictions of the previously
discussed models are accurate is by comparing predictions to experiment.
Paleomagnetism, again, presents a challenge.
Early studies into rock magnetism were primarily of MD
states with well defined domain walls using techniques like
Bitter patterns \cite{Halgedahl1991}.
These magnetic grains could be tens of microns in size.
However, the SD and PSD ranges are typically on the sub-micron scale,
typically tens, or hundreds of nanometers.
Further, the PSD states represent a rich non-uniform magnetization, not
easily understood by measurements of the outside of the material.

Electron holography an experimental technique, capable of measuring magnetic
fields on the nano-scale, and and from within the material \cite{Tonomura1980}.
In particular, off-axis electron holography is a useful technique for
generating electron holograms, and finding the ``direction'' of the
contours in electron holograms, describing the direction of the in-plane
magnetic field they represent \cite{Lehmann2002}.
This represents a viable technique for making meaningful measurements of
PSD grains.
In particular, with the development of models of electron holography
\cite{Keimpema2006} to use with models of micromagnetism, we have
techniques for quantitatively comparing experimental data with theoretical
predictions \cite{Almeida2016}.
In this thesis, we will derive two techniques for numerical electron
holography simulation for use with the numerical micromagnetic models
developed here.

We will present a range of simple remanent SD, flower-state (FS) and PSD states,
and their electron holograms from various orientations and angles.
This will represent a Rosetta stone for converting from electron holograms
back to remanent states for nano-scale materials, as predicted by numerical
micromagnetic modelling.
This should be a useful reference for reasoning about magnetic states
inferred from electron holograms in physical experiments.
In particular, we will discuss features of electron holograms that are
indicative of various states and orientations, and features that are red
herrings.
We will also discuss features that cannot be distinguished in electron
holograms, either due to lack of feature resolution or due to symmetries
inherent to the process of electron holography.



\section{Notation}

This thesis is rather math-heavy.
We present here the notation used, and the typical meaning of various symbols
used throughout.

Typically, we will prefer to use a functional notation.
For a function $f(x)$, the derivative at $x$ is
$\frac{\partial f}{\partial x}(x)$.
However, this presents a potential source of confusion.
When taking the derivative of $f(x)$, should we take the derivative of
only $f$, or should we apply the chain rule, and take the derivative of $x$
as well?
This is particularly confusing when performing partial integration of
integrals, or changes of coordinates with derivatives of functions.

Unless otherwise states, one may assume functional notation.
We will use the explicit notation $f(x)$ in a number of cases, but it should
be reasonably clear from context what is happening.
This can, however, get a little confusing when multiplying a function by
a brace.
For three functions $f$, $g$, and $h$, we can write, in our notation,
$f \cdot g + f \cdot h$.
The value at $x$ is $(f \cdot g + f \cdot h)(x) = f(x) g(x) + f(x) h(x)$.
However, we can also write $f \cdot g + f \cdot h = f \cdot (g + h)$.
Without the explicit ``$\cdot$'' for function multiplication, we would write
$f(g+h)$ which could be confused for the evaluation of $f$ at a point $g+h$,
or perhaps a function composition of $f$ with $g+h$.

We choose the functional notation, with this in mind, since reasoning about
derivatives is harder than confusion with notation.
Mistakes or ambiguities in notation can be recovered from context, and from
previous lines, while mistakes in derivatives are fundamental errors.

We also use a number of shorthands for vectors, derivatives, and directional
derivatives.
In particular, we will be using Einstein notation for summation, and
four notations for derivatives, each with its own use.
The following are equivalent
\begin{equation*}
\begin{aligned}
    \frac{\partial \phi}{\partial x} +
    \frac{\partial \phi}{\partial y} +
    \frac{\partial \phi}{\partial z}
    &=
    \Vector{\nabla} \phi
    \\
    &=
    \partial_x \phi + \partial_y \phi + \partial_z \phi
    \\
    &=
    \phi_{,x} + \phi_{,y} + \phi_{,z}
\end{aligned}
\end{equation*}
and using implicit indexing and Einstein notation,
\begin{equation*}
\begin{aligned}
    \frac{\partial^2 \phi}{\partial x^2} +
    \frac{\partial^2 \phi}{\partial y^2} +
    \frac{\partial^2 \phi}{\partial z^2} +
    &=
    \nabla^2 \phi
    \\
    &=
    \partial_i \partial_i \phi
    \\
    &=
    \phi_{,ii}
\end{aligned}
\end{equation*}

The $\frac{\partial}{\partial x}$ notation is the familiar derivative notation,
and $\Vector{\nabla}$ should be familiar from vector calculus.
The notation $\partial_x$ is a shorthand for $\frac{\partial}{\partial x}$.
This is particularly useful, since it can be easily indexed.
For example $\sum_i \partial_i = \partial_x + \partial_y + \partial_z$.
It is also useful since it is clearly visible in equations, but still compact.
The final notation, the comma notation $\phi_{,x}$ is typical of tensor
calculus, where one often needs to manage a large number of indices,
and a large number of derivatives applied to a tensor.
In brief, everything after the comma is a derivative.
The expression $\phi_{,x}$ is equivalent to $\frac{\partial \phi}{\partial x}$.
Similar to $\partial_i$, it is easily indexed.
On top of that, it is much clearer which object the derivative applies to.
The expression $\partial_i f \partial_i g$, for example, is ambiguous.
Do we mean $(\partial_i f) \cdot (\partial_i g)$ or
$\partial_i (f \cdot (\partial_i g))$?
By writing this as $f_{,i} \cdot g_{,i}$, it is entirely unambiguous.
It also allows us to move $f_{,i}$ and $g_{,i}$ about the equation without
much thought.


\begin{minipage}{\textwidth}
\null
\subsection{Vectors, Tensors}
\begin{equation*}
\begin{aligned}
f_i\ g_i &= \sum_i f_i\ g_i \quad \mbox{Using Einstein notation}
\\
\Vector{f} &= \mbox{Vector}
\\
\Vector{f} &= (f_1,\ f_2,\ \ldots) \quad \mbox{Implied vector subscripts}
\\
\Tensor{f} &= \mbox{Matrix or tensor. More than 1 index.}
\\
\Vector{f} \cdot \Vector{g}
    &= f_i\ g_i \quad \mbox{Dot product. Vector multiplication.}
\\
\Tensor{f} : \Tensor{g}
    &= f_{i j k l}\, g_{k l} \ \text{or}\  f_{i j k l}\, g_{k l m n}
        \quad \mbox{Tensor double dot product.}
\\
\Vector{f} \times \Vector{g}
    &= \epsilon_{i j k}\ f_j\ g_k \quad \mbox{Cross product}
\end{aligned}
\end{equation*}
\par
\end{minipage}

\begin{minipage}{\textwidth}
\null
\subsection{Functions}
\begin{equation*}
\begin{aligned}
f(x) &= \mbox{Evaluation of $f$ at $x$}
\\
f \cdot g &= \mbox{Function multiplication}
\\
(f \cdot g)(x) &= f(x)\ g(x)
\\
f \circ g &= \mbox{Function composition}
\\
(f \circ g)(x) &= f(g(x))
\end{aligned}
\end{equation*}
\end{minipage}

\begin{minipage}{\textwidth}
\null
\subsection{Derivatives}
\begin{equation*}
\begin{aligned}
\partial_i f&= \frac{\partial f}{\partial x_i}
                \quad \mbox{Derivative shorthand, $i$ an index}
\\
f_{,i} &= \frac{\partial f}{\partial x_i}
            \quad \mbox{Derivative shorthand, $i$ an index}
\\
f_{i,j} &= \frac{\partial f_i}{\partial x_j}
            \quad \mbox{Derivative shorthand, $i$, $j$ are indices}
\\
\Vector{\nabla} &= (\partial_x,\ \partial_y,\ \partial_z)
                    \quad \mbox{Del, or nabla operator}
\\
\Vector{\nabla} f &= \left(
    \frac{\partial f}{\partial x},\ 
    \frac{\partial f}{\partial y},\ 
    \frac{\partial f}{\partial z}
    \right) \quad \mbox{Gradient}
\\
\Vector{\nabla} \cdot \Vector{f} &= \partial_i f_i \quad \mbox{Divergence}
\\
\Vector{\nabla} \times \Vector{f}
    &= \epsilon_{i j k} \partial_j f_k \quad \mbox{Curl}
\end{aligned}
\end{equation*}
\end{minipage}

\begin{minipage}{\textwidth}
\null
\subsection{Integrals}
\begin{equation*}
\begin{aligned}
\int_\Omega f\ dV &= \mbox{3d volume integral of $f$ over $\Omega$}
\\[\parskip]
\int_{\partial\Omega} f_i\ n_i\ dS
    &= \parbox{0.5\textwidth}{
        2d surface integral over $\partial\Omega$, the boundary of $\Omega$, of
        $\Vector{f}$ dotted with the oriented normal of the surface,
        $\Vector{n}$
    }
\\[\parskip]
\int_{C} f_i\ n_i\ dl
    &= \parbox{0.5\textwidth}{
        1d line integral over the curve $C$ of $\Vector{f}$ dotted with the
        oriented tangent of the curve, $\Vector{n}$
    }
\end{aligned}
\end{equation*}
\end{minipage}


\begin{minipage}{\textwidth}
\null
\subsection{Magnetism}
\begin{equation*}
\begin{aligned}
\Vector{M} &= \mbox{Magnetization}
\\
M_s &= \mbox{Saturation Magnetization $|M|$}
\\
\Vector{m} &= \mbox{Unit magnetization $\Vector{M}/M_s$}
\\
\Vector{H} &= \mbox{Effective field}
\\
\phi &= \mbox{Magnetic scalar potential}
\\
\Vector{\nabla}\phi &= \Vector{H}
\\
\nabla^2 \phi &= \Vector{\nabla} \cdot \Vector{M}
\\
\Vector{B} &= \mbox{Magnetic field}
\\
\Vector{A} &= \mbox{Magnetic vector potential}
\\
\Vector{\nabla}\times\Vector{A} &= \Vector{B}
\\
\Vector{\nabla}\times\Vector{\nabla}\times\Vector{A}
    &= \Vector{\nabla}\times\Vector{M}
\\
\Vector{\alpha} &= \parbox{0.55\textwidth}{
        Directional cosines of the magnetization and the crystal axes
    }
\\
B_1, B_2 &= \mbox{Magnetostriction coupling constants}
\\
B^0_{ij} &= \mbox{Magnetostriction coupling tensor}
\\
B^0_{i j} &=
    \begin{cases}
        B_1 \alpha_i^2 & \mbox{if $i=j$}
        \\
        B_2 \alpha_i\alpha_j & \mbox{if $i\ne{}j$}
    \end{cases}
\end{aligned}
\end{equation*}
\end{minipage}

\begin{minipage}{\textwidth}
\null
\subsection{Elasticity}
\begin{equation*}
\begin{aligned}
\sigma_{i j} &= \mbox{Stress}
\\
\varepsilon_{i j} &= \mbox{Strain}
\\
\Vector{u} &= \mbox{Displacement}
\\
\varepsilon_{i j} &= \frac{1}{2}(u_{i, j} + u_{j, i})
\\
C_{i j k l} &= \mbox{Stiffness tensor}
\\
S_{i j k l} &= \mbox{Compliance tensor (inverse of $\Tensor{C}$)}
\\
C_{i j k l} S_{k l m n}
    &= \frac{1}{2}(\delta_{i m}\delta_{j n} + \delta_{j m}\delta_{i n})
\\
\Vector{b} &= \mbox{Burgers Vector}
\\
b_i &= -\oint_C u_i\ dl
    \quad \parbox{0.4\textwidth}{
        with $C$ counter-clockwise about dislocation line
    }
\end{aligned}
\end{equation*}
\end{minipage}
