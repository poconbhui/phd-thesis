\documentclass[a4paper,12pt,openright]{report}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{titling}

\usepackage{palatino}

\usepackage{parskip}

\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{bm}

\usepackage[
    tmargin=4cm,
    bmargin=4cm,
    inner=4cm,
    outer=2.5cm,
    twoside
]{geometry}

\usepackage{setspace}
\onehalfspacing

\usepackage[all]{xy}


% Search paths for input, include, includegraphics
\makeatletter
\def\input@path{{resources/fem_integral_domains/}}
\makeatother

\usepackage{graphicx}
\usepackage{grffile}
\usepackage{graphbox}

\usepackage{ifthen}

% For inkscape pdf_tex files
\usepackage{color}

\usepackage{placeins}

\usepackage{sectsty}
\allsectionsfont{\raggedright\normalfont\sffamily\bfseries}

\newcommand{\Vector}[1]{{\vec{#1}}}
\newcommand{\Tensor}[1]{{\bm{#1}}}

\usepackage[most]{tcolorbox}
\newtcolorbox{asidebox}[1][]{
  enhanced,
  arc=10pt,
  outer arc=10pt,
  colback=gray!8,
  boxrule=0.8pt,
  parbox=false,
  #1
}

\usepackage[most]{tcolorbox}
\newtcolorbox{resultbox}[1][]{
  enhanced,
  arc=10pt,
  outer arc=10pt,
  colback=white,
  boxrule=0.8pt,
  #1
}

\usepackage{array}
\def\arraystretch{1.5}

\usepackage{listings}
\lstset{
    basicstyle=\small\sffamily,
    numbers=left,
    language=Fortran,
    frame=single
}

\usepackage[
    backend=biber,
    doi=true,
    url=false,
    style=authoryear,
    uniquename=false
]{biblatex}

\addbibresource{bibliography.bib}

\let\cite\parencite

\input{hyphenations.tex}
\usepackage{hyphenat}


\title{
    Micromagnetic Modelling \\of Imperfect Crystals
}
\author{P\'adraig \'O Conbhu\'i}
\date{}

\renewcommand{\maketitlehooka}{\sffamily}
\renewcommand{\maketitlehookd}{
\begin{center}
\vfill
\includegraphics[height=6\baselineskip]{resources/uoe_logo/UoE_Centred_Logo_CMYK_v1_160215.eps}
\vfill
Thesis submitted for the degree of Doctor of Philosophy

School of GeoSciences

University of Edinburgh

%2017
2018
\end{center}
}

\begin{document}

\begin{titlepage}
\maketitle
\end{titlepage}

\shipout\null

\pagenumbering{roman}

\pagebreak
\section*{Abstract}
%\begin{abstract}
In paleomagnetism, practical measurements are rarely made using perfect,
isolated, single-phase, ferromagnetic crystals.
Experimental observations are typically made using magnetic materials formed by
a variety of natural processes.
In this thesis, we will look at bridging the gap between current numerical
modelling capability and experimental observations.

First, we work towards micromagnetic modelling of multi-phase magnetic materials,
including magnetostriction, embedded in a rocky matrix, along with crystal
defects.
We present a derivation of the Boundary Element Method formulation used
by the micromagnetics package, MERRILL, and provide an extension of this from
single-phase materials to multi-phase.
After discussing issues with previous approaches to modelling magnetostriction,
we derive and present a more robust and flexible approach.
This model of magnetostriction is suitable for non-uniform magnetizations, for
multi-phase materials, and for arbitrary boundary conditions, and can
be incorporated into MERRILL.
We then outline a method for extending our model to materials embedded in an
infinite elastic matrix of arbitrary elasticity.
Finally, we present a method for modelling the magnetic response of a material
due to crystal defects, along with a concrete example of a magneto-dislocation
coupling energy at a magnetite-ilmenite boundary where stress due to lattice
misfit is eased by regular edge dislocations.

Second, we work towards being able to verify micromagnetic models against
nano-scale experimental data.
To do this, we present two techniques for simulating electron holograms from
micromagnetic modelling results, a technique capable of imaging magnetic
structures at the nano-scale.
We also present example electron holograms of commonly occurring magnetic
structures in nano-scale rock and mineral magnetism, and highlight some
distinguishing features, which may be useful for interpreting experimental
electron holography data.
%\end{abstract}


\pagebreak
\section*{Lay Summary}
Paleomagnetism is the study of how rocks record changes in the earth's
magnetic field.
The recordings made need to remain stable for thousands, millions, or even
billions of years since the rock was formed.
Verifying that a recording is stable for that length of time is an ongoing
problem.
One approach to verification is to use a technique called ``micromagnetic
modelling'' to replicate a physical sample in a computer model, which can then
be probed in a number of ways, and in more detail, than is available to a
physical experiment.
These computer experiments can then present some theoretical predictions of
the stability of the physical recordings.
This approach, however, requires that all the physics of the rock is
adequately considered by the numerical model.
It also requires that modelling results can be verified against physical
samples.

Micromagnetic models in rock and paleomagnetism typically assume a perfect,
isolated, stoichiometric crystal.
This does not adequately account for the mechanical properties of the crystal,
e.g. embedding in a larger rocky matrix, and dislocations due to crystal
intergrowths, and their effect on the stability on its magnetic recording.
The physical phenomena that can account for these
are elasticity, dislocations, and magnetostriction.
This thesis derives and presents the math necessary to model magnetostriction
in a way that is compatible with current state-of-the-art micromagnetic
modelling techniques of non-uniform magnetizations.
It then presents some preliminary results for magnetic crystals using these
modelling techniques.
An extension of this modelling technique for embedding the crystal in a larger
rocky matrix is also presented, along with a method for including dislocations
(e.g. crystal intergrowths) into micromagnetic models.

For experimental verification, this thesis presents a technique for simulating
a widely used experimental technique called ``electron holography''
for producing ``magnetic induction maps.''
These magnetic induction maps
are a measurement of the
magnetization of a crystal at the nanometer scale.
By simulating these
maps for micromagnetic modelling results,
it is possible to directly compare these theoretical measurements to
experimental measurements as evidence the micromagnetic model produces the currect result.

\pagebreak
\section*{Acknowledgements}
There are a number of people I'd like to thank for their help and support
during my PhD.
First, and foremost, I'd like to thank Wyn Williams for his continued
support and input throughout, and for putting up with my messing.
He has been instrumental for putting me on the right track, and for keeping
me there.
I'd like to thank Les Nagy for help and input on the various problems and
codes I've worked on.
Karl Fabian has provided great insight and encouragement on most, if not all,
topics included in this thesis.

As frequent collaborators, Adrian Muxworthy, Trevor Almeida, Richard Harrisson,
and Josh Einsle provided much of the incentive for the work I've done here.
Without their demand, their input, and their data, much of the progress made in
this thesis might not have happened.
Similarly, I've enjoyed and benefited the discussions I've had with Jay Shah
and Miguel Valdez, who are in about the same position as I am, but from a more
experimental side.
I must thank Lawrence Mitchell for introducing me to Wyn, and for his help
early in the PhD, pointing me towards some of the tools that would be
instrumental to implementing the techniques discussed here.
Similarly, Chris Johnson provided some great non-technical feedback and advice.

Most importantly, I need to thank my friends and family, who make
life worthwhile.
I'd like to thank Anna Curran, for being a beb.
She means too much to me to summarize in a sentence.
Ann, Pat, and Hazel Conway are each deserving of a medal for putting up with
me for this long.
Although they didn't really have a choice.
I'm sure Ann will have something similar to say in her own thesis.
Thanks go to my friends in Edinburgh, who made it worth sticking around, Rory
McKavney, Matt Holloway, John Preston, Luca Forresta, and many many others.
And Rory, especially, for encouraging my poor habits (and cleaning up after me
last year).

Finally, many thanks to Adrian Muxworthy and Ian Main for examining
this thesis.
Similarly, apologies to Adrian Muxworthy and Ian Main
for having to parse my particular style of writing, which likely reads more
like code and comments than a flowing narrative.
Not to mention my utter inability to write in a formal register.


\cleardoublepage
\section*{Declaration}
This thesis is my own composition.
The work presented here is my own, except where explicitly referenced.
This work has not been submitted for any other degree or other
professional qualification.
There are no other publications included within this thesis.

\begin{flushright}
P\'adraig \'O Conbhu\'i
\end{flushright}


\tableofcontents



\pagebreak
\shipout\null

\pagenumbering{arabic}

\include{chapter_introduction}

\part{Micromagnetic Modelling}
\include{chapter_fem_formulation}

\part{Magnetostriction}
\include{chapter_magnetostriction}

\include{chapter_magneto_dislocations}

%\include{chapter_crystal_defects}

\part{Electron Holography}
\include{chapter_electron_holography}

\part{Reflections}
\include{chapter_discussion_and_conclusions}

%\bibliography{bibliography}
%\bibliographystyle{apalike}
\printbibliography

\end{document}
