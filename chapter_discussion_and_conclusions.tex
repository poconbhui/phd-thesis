%
% Discussion
%
\chapter{Discussion}

In this chapter, we will discuss some of the results, and implications of the
results of this thesis.
This will be a much more speculative chapter, outlining pieces of the story left
out, potential issues, and future work.

This thesis focused mainly on the {\it scientific} work I've done during my
PhD, rather than the engineering.
In particular, it focuses on the theoretical physics and modelling results.
A general theme has been to present some theoretical derivation of a phenomenon
and then present some computer experiment using results from a model
implementing that derivation.
The missing link between these two chapters, in every instance presented here,
has been the implementation details.

Most of the time that went into the work presented in this thesis has actually
been to implement the numerical models.
Rather than going into detail about the programs written, I decided instead
to focus on presenting the theory behind them, and the results they produce,
as that presented a more coherent story.
A chapter was planned for a number of programs I wrote during my PhD,
particularly some of the meshing software I wrote.
Unfortunately, due to time and space constraints, it wasn't included.

There are two significant pieces of software I wrote:
HoloMag for electron holography simulations of micromagnetic models,
available at
\url{bitbucket.org/poconbhui/holomag}\ , and
MEshRRILL for generating tetrahedral meshed geometries for use with MERRILL,
available at \url{bitbucket.org/poconbhui/meshrrill}\ .
I also made significant contributions to MERRILL, available at \\
\url{bitbucket.org/wynwilliams/merrill} and \url{rockmag.org}\ ,
which I will talk about in more depth here.



\section{Micromagnetism}

MERRILL represents a significant step forward in user friendly micromagnetic
simulation.
It's already in widespread use in the Earth Sciences community.
The addition of multi-phase solvers is, I think, a significant improvement.

Something left out of this thesis is the amount of software engineering work
that went into MERRILL.
As with other implementation details, it didn't add significantly to the
stories I was presenting.
In particular, I spent a long time cleaning it up, making it ready for
contributions from outside sources.
I also got it running orders of magnitude faster than before, to the point where
there was some doubt about whether I had broken it with my changes, until we
found the results were correct.

There is still more work to be done on MERRILL.
There are already others, not just myself, making some changes to the source
code, so I would consider my cleanup of the code to be a success.
Ideally, MERRILL should still be maintainable if I fall out of touch with
the Edinburgh group.

Some parts of MERRILL are still very messy, and I foresee mistakes being
introduced, simply because someone forgot to update the various disparate parts
in various disparate files that control saving and loading of variables.
The matrix storage, as an example, needs to be made more generic, so {\it one}
function can be written to save and load them and {\it one} type can be used
to reference them.
There are a lot of instances of matrices where one variable holds the matrix
data, one variable holds the column indices and one variable holds the column
offsets.
These should be combined into a derived type.
Further, there are instances of arrays which hold arrays of these matrix
variables to ``save'' them for later loading, interpolation etc.
These ``save'' type arrays should be holding derived types representing the
arrays, and {\it one} clear operation should automatically find / save / load
the appropriate parts of each matrix.
And {\it even} further, these ``save'' type arrays should be linked lists, so
they can be dynamically resized with little memory overhead.
And even further {\it again}, the active matrices could be pointers into this
linked list, further reducing memory overhead.
With these in place, running several models in parallel while sharing matrix
data would be reasonably straightforward.

What I hope to convey with this convoluted phrase is that there are some
fundamental architectural issues with MERRILL.
To fix them would require some significant effort, and a change in the
paradigm MERRILL was programmed in.
Specifically, it was initially programmed in a purely procedural style, while
I am advocating an object oriented style here.
However, they payoff would be a faster program with a lower memory footprint,
and easy parallelization.
Unfortunately, however faster the program may be, however smaller the memory
usage, however more maintainable by a software engineer, one must remember the
audience it is aimed at: Earth Scientists.
As it stands, the community has been able to use and edit MERRILL as needed,
and any increase in complexity runs the risk of making it unusable and
unmodifiable to the target audience.


\subsection{Future Work}


\subsubsection{Multi-Phase Modelling}

I would recommend taking full advantage of the multi-phase capabilities in
MERRILL.
%There are plenty of papers to be written on various combinations of various
%materials.
There are numerous problems to be investigated on various combinations of
different materials.
So far, most multi-phase simulations have been on regularly shaped geometries.
We could easily do these now for arbitrary geometries.

First, however, a number of things need to be implemented and researched.
First, we need to implement surface energies.
In particular, surface anisotropies to account for the magneto plastic effect
along the interface planes, and surface exchange energies to account for
the exchange energy of the mixed crystal.
We need an FEM formulation for the surface anisotropy energy and surface
anisotropy effective field.
Given the discussion here and in \textcite{Davies2011}, that should be
reasonably straightforward.
We also need the effective magnetoplastic anisotropy energy.
I was only able to find that here thanks to the work by \textcite{Merwe1950}
and \textcite{ShiveButler1969}.
I suspect for systems that don't resemble the magnetite-ilmenite interface,
this might be a more difficult problem.

%We also need formulations and values for the exchange energies at the interface.
%I believe Charlie Penny is doing DFT work towards finding the exchange energies
%as part of his PhD work, which should be a significant contribution towards this
%goal.
%
%Of course, this lack of information hasn't prevented other authors from
%publishing multi-phase results for finite-difference codes, so there really
%is nothing preventing these studies from being re-done with the FEM code
%with irregular geometries.


\subsubsection{Parallelization}

MERRILL is suitable for parallelization on a shared-memory architecture without
any major change in the implementation approach or the physics.
That is, when running on a computer with, say, 64 cores, it could be
parallelized reasonably well to the 64 cores.
To be clear, a machine like Archer in Edinburgh with over 100,000 cores is a
distributed-memory architecture, made up of a number of shared-memory nodes.
Each shared-memory node on Archer contains 24 cores, which means 24 cores is
the maximum efficient parallelism MERRILL could reach on this machine.
The primary reason it can't be effectively parallelized on a distributed-memory
system is the dense matrix used for the BEM.

I tried to parallelize MERRILL a few times with OpenMP, but couldn't quite find
the bottlenecks at the time.
I believe I know where the issue is now.
The preconditioner for the Poisson FEM matrices does an approximate LDU
factorization.
Solving the L and U matrices, a step in the preconditioned conjugate gradient
solver, doesn't parallelize well.
A simple solution is to swap to a parallel-friendly preconditioner scheme
instead.
After that, parallelization with OpenMP would be trivial.

There exists another possibility for parallelism for MERRILL, which is GPU based
parallelism.
This would allow matrix operations to be parallelized quite well.
However, memory management would be a significant issue here.
In particular, a top-of-the-line (as of the time of writing) Tesla P100 GPU has
around 16~GB of memory.
That means the models that would run well on this platform would need to be
under 16~GB.
Beyond that, managing moving data on and off the card could contribute enough
overhead that any performance gain would be lost.


\subsubsection{Abstraction}

The energy calculators in MERRILL should be extracted into derived types.
Similar to what was mentioned for the matrices, there is a lot of redundant
spaghetti code attached to the energy calculators.
This has already been done to a certain extent, and is used for loading
the magnetostriction solver as a plugin.
However, the core energies --- demag, anisotropy, exchange, Zeeman, NEB fields
(which I don't understand...) --- should be similarly packaged up.

One upside to this would be the ability to add ``dirty'' bits to the energies.
A dirty bit is a flag set to say something has been changed.
For example, we might set a dirty bit for the mesh, the material parameters, or
for the magnetization.
That way, we might ask the energy to do any necessary precomputations (e.g.
building the FEM matrices for the demag calculator) every time it's called,
but only have it run if the pieces it depends upon (i.e. the mesh, the
subdomains, and the material parameters) change.

The application I have in mind is that Brown's full magnetostrictive energy,
including the demag-elastic effect involves the demag field.
While solving for Brown's magnetostriction, it would make sense to calculate
the demag field only once, however, there is no strict guarantee in MERRILL
that the demag field has already been calculated when the magnetostriction
solver runs.

Of course, knowing MERRILL intimately, I know the demag solver runs first,
but there is no guarantee someone won't change that behaviour in the future.
Making the order explicit by calling the demag solver in the magnetostriction
solver, but only have it run once for a given magnetization, would be ideal.



\section{Magnetostriction}

The derivation of the magnetostrictive effect here for micromagnetic models
isn't new math.
However, the FEM derivation suitable for inclusion in a FEM/BEM based model,
along with the implementation is.
However, the approach taken here, generalizing Kittel's model to a non-uniform
magnetization is limited.
In particular, it fails to account for the deformation of a material due to
its demagnetization field.
This is the phenomenon that causes large magnets to tear themselves apart.
It also doesn't account for, say, the change in the exchange interaction due
to the deformation of the material.
Brown's formulation does.

The approach taken here, however, is {\it inspired} by Brown's.
Indeed, including the extra behaviours predicted by Brown's full theory
should be straightforward given the ground work laid out here.
However, I would expect the demag-magnetostrictive effect to be small in
the grains we're interested in, and there aren't many measurements of the
effects of deformation on exchange, anisotropy etc. for the materials we're
interested in.
There are hardly even measurements of the elastic or magnetic constants for the
materials we're interested in!
In short, I'm not sure including the extra effects from Brown's theory
are feasible at the moment, or that they will make a significant difference.
It would still be interesting to do for a material with all the required
material parameters well measured.
As mentioned, some changes to MERRILL should be made before some of these
can be implemented reliably.

The simulations performed here are the first of their kind, as far as I can
find.
In particular, an appropriate simulation of magnetostriction for non-uniformly
magnetized ferromagnets, along with the deformation.
They make direct, quantitative predictions about the deformations due to
flower and single vortex states.
A good follow up for the simulations presented here would be high resolution
measurements in an electron microscope to see if my predictions are accurate.
There is also a prediction made here of a remanent Wiedemann effect, where
I predict the grain will assume a helical torsion due to a vortex magnetization.
This might actually be a way of getting vortex orientations from electron
holograms: information which might otherwise be lost.

The magnetoplastic effect presented here is also quite interesting.
Of course, it is just the simplest thing I think will work, and that makes
sense to me.
The result, however, is that I still don't understand all the consequences and
implications of the equations I've derived.
A lot more work and investigation needs to be done in this direction,
as it's the only promising route I've seen for recovering Kittel's uniaxial
result.


\subsection{Future Work}

\subsubsection{FEM Implementation}

The implementation used for the magnetostriction used FEniCS to build the FEM
matrices.
This has a significant issue that the system must be ``assembled'' on each run.
In particular, if the material parameters remain the same from one solve to
the next, the elasticity matrix can be kept as-is.
However, the magnetoelastic matrix is build with contributions from the
displacement vector.
So any change to the magnetization implies a change to the displacement vector,
meaning the FEM matrix for the magnetoelastic fields must be rebuilt.

By using a rank-3 tensor to store the magnetoelastic FEM contributions,
it may be possible to keep the displacement independent of this tensor,
meaning it need not be reconstructed for every new magnetization.
Most of the math and a rough implementation of the necessary Fortran code has
been presented in this thesis.
Baking the material parameters into the magnetostrictive tensor would lower
the indices needed by the tensor, potentially reducing the storage needs
and calculation time.
However, the implementation of dirty bits, as previously mentioned, would be
needed to do this effectively.


\subsubsection{Magnetoplasticity}

The magnetoplastic coupling is a rich source of future work.
I've only written down the general form.
Some more work needs to be done in interpreting exactly what is described by
the equations, and what can be described by them.
There is also still much to do in finding solutions for the particular systems
common in GeoSciences.

The magnetoplastic coupling needs to be included in an FEM model.
I've shown what the surface anisotropy for a magnetite-ilmenite lamellar system
looks like, on a scale suitable for micromagnetic modelling.
However, I've made no effort to formulate it in a manner suitable for
surface integration for a FEM implementation.
Given all the work presented here on multi-phase surface integrals etc. this
should be reasonably straightforward though.



\section{Electron Holography}

The electron holography simulation software presented here has already proven to
be very useful, and very popular, with results already in use in publications.
Unfortunately, it's also completely untested, and prone to crashing.
The results {\it look} good, and match some experimental data, but some more
work needs to be done to add unit testing, and debug the typical cases where the
code crashes or hangs.

One significant body of work left out of this thesis, actually, is the meshing
work I've done.
My implementation for generating the outline for the grain, for example, may
well be an original contribution to computational geometry.
This, along with the 2D meshing needed to generate the projection mesh
has been completely glossed over, but represented a significant difficulty
to be overcome, and a significant amount of time in terms of implementation.


\subsection{Future Work}

Some better testing needs to be added to HoloMag.
In particular, some experimental electron holograms of a well measured geometry
with a well known magnetization is needed to ensure the results generated by
HoloMag are correct.
While I've said repeatedly throughout this thesis that a micromagnetic model
is currently the best measurement of the internal magnetization, that's not
good enough in this case, because the electron hologram might be used as a
verification of the model.
A single domain cube of magnetite would be ideal, along with a single vortex
cube.

The projection code in needs work, as it frequently crashes.
Due to the complexity of generating the outline and the mesh, and the
size of the libraries it relies upon, debugging is not straightforward.
The environment in which it's run --- as a plugin for ParaView ---
also makes it quite difficult to test and debug.



\section{Closing Remarks}

It was touched upon in the various conclusions chapters in the thesis, but the
work done here will have an impact on any GeoSciences application that deals
with ferromagnetism like paleomagnetism, rock magnetism, and magnetic fabrics.
The implications are even wider than that: it should have an impact on
any field that uses ferromagnets, like biomagnetism, nano-wires, and magnetic
ram to name a few.
We've demonstrated throughout this thesis that we have provided many of the
necessary pieces to consider all the physics occurring in a given system within
the continuum approximation, along with the mechanical deformation of the
crystal lattices, for ferromagnets of arbitrary shape and composition.

I hope the work done here and detailed derivations are useful in future research
and to future students.
I've made every effort to expand all the steps I've taken as fully as possible,
which is something I would have liked to have starting out.

I also hope the various pieces of software written for this thesis finds use in
research environments.
I've found them quite useful in my research, and I expect others will as well.
