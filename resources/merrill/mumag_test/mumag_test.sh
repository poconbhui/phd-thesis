#!/usr/bin/env bash

set -e

space_scales=(1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3)
data_dir=$PWD/data
mkdir -p "${data_dir}"
echo > ${data_dir}/intersect.dat

#cp mumag_test.template.bsc "${data_dir}"/template.bsc
module load meshrrill
cp mumag_test-irregular.template.bsc "${data_dir}"/template.bsc

# Run the test for each scale
for space_scale in ${space_scales[@]}
do
    echo $space_scale
    output_dir=${data_dir}/output_${space_scale}
    mkdir -p "${output_dir}"
    
    # Set mesh scaling in the bemscript template
    l_ex="9.587248822575673*10^(-3)" # in microns
    mesh_spacing=$( echo "$l_ex * $space_scale" | bc -l )
    sed \
        's/define mesh_spacing.*/define mesh_spacing '$mesh_spacing'/' \
        ${data_dir}/template.bsc \
        > ${output_dir}/script.bsc

    # Run MERRILL
    (
        cd ${output_dir}
        /home/paddy/prog/merrill/build/build/merrill script.bsc | tee output.txt
    )
    
    # Process the output
    echo "L AvgEdge E-Anis-Flower E-Demag-Flower E-Exch-Flower E-Tot-Flower Mx-Flower My-Flower Mz-Flower M-Flower E-Anis-Vortex E-Demag-Vortex E-Exch-Vortex E-Tot-Vortex Mx-Vortex My-Vortex M-Vortex" > ${output_dir}/stats.txt
    ##echo >> ${output_dir}/stats.txt
    cat ${output_dir}/output.txt \
        | grep '^ *L\|Avg Edge  \|E-Anis\|E-Demag\|E-Exch \|E-Tot\|<M' \
        | sed 's/Avg Edge/AvgEdge/' \
        | tr '\n' ' ' \
        | sed 's/L/\nL/g' \
        | grep '^ *L' \
        >> ${output_dir}/stats.txt

    # Find the intersection (Avg Edge, L_ex intersect)
python <<EOF
length = []
e_anis = []
e_demag = []
e_exch  = []
e_tot   = []
m_x = []
m_y = []
m_z = []
edge_average = -1

# Read all the flower and vortex energies
for i,l in enumerate(open("${output_dir}/stats.txt", "r")):
    if i == 0: continue
    s = l.split()
    length.append(float(s[1])/100)
    edge_average = float(s[3]) / $(echo ${l_ex} | bc -l)
    e_anis.append((float(s[5]), float(s[23])))
    e_demag.append((float(s[7]), float(s[25])))
    e_exch.append((float(s[9]), float(s[27])))
    e_tot.append((float(s[11]), float(s[29])))
    m_x.append((float(s[13]), float(s[31])))
    m_y.append((float(s[15]), float(s[33])))
    m_z.append((float(s[17]), float(s[35])))

# Find the index of the first flower energy > vortex energy
critical_pos = -1
for i in range(len(length)):
    if e_tot[i][0] < e_tot[i][1]:
        critical_pos = i
        break

# Find the intercept, the length where the flower and
# vortex energies are equal
t = (
    e_tot[critical_pos-1][1]-e_tot[critical_pos-1][0]
)/(
     (e_tot[critical_pos-1][1]-e_tot[critical_pos-1][0])
    -(e_tot[critical_pos][1]-e_tot[critical_pos][0])
)
def intercept(x):
    return x[critical_pos-1]*(1-t)+x[critical_pos]*t

def flower_intercept(x):
    return intercept([xi[0] for xi in x])

def vortex_intercept(x):
    return intercept([xi[1] for xi in x])

intercept_length = intercept(length[:])
intercept_energy = intercept([e[0] for e in e_tot])

# Append the result to intercept.dat
with open("${data_dir}/intersect.dat", "a") as f:
    print("%e %e %e"%(edge_average, intercept_length, intercept_energy))
    f.write(
        "%e %e %e " \
        "%e %e %e %e %e %e " \
        "%e %e %e %e %e %e\n" \
        %(
            edge_average, intercept_length, intercept_energy,
            flower_intercept(e_anis), flower_intercept(e_demag),
            flower_intercept(e_exch),
            flower_intercept(m_x), flower_intercept(m_y),
            flower_intercept(m_z),
            vortex_intercept(e_anis), vortex_intercept(e_demag),
            vortex_intercept(e_exch),
            vortex_intercept(m_x), vortex_intercept(m_y),
            vortex_intercept(m_z)
        )
    )
EOF

done

# Build the images
gnuplot intersect.plt
pvpython cube_states.py
