#!/usr/bin/env pvpython

from paraview.simple import *

servermanager.LoadState("cube_states.pvsm")

cube_view = FindView("Cube Geometry")
flower_view = FindView("Flower State")
vortex_view = FindView("Vortex State")

views = [cube_view, flower_view, vortex_view]
names = ["cube_view", "flower_view", "vortex_view"]

# Fix cube edge width
for view in views:
    GetDisplayProperties(FindSource("Cube Edges"), view).LineWidth = 3

# Fix the display sizes
for view in views:
    view.ViewSize = [1000, 1000]

# Rezoom to the biggest object
ResetCamera(flower_view)

for view, name in zip(views, names):
    view.UseOffscreenRenderingForScreenshots = 1
    view.UseOffscreenRendering = 1
    SaveScreenshot("data/%s.png"%(name), view)
