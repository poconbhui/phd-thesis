#!/usr/bin/env gnuplot

# Usage: cd into appropriate data directory, then run this script

set style line 1 lc rgb '#8b1a0e' pt 1 ps 1 lt 1 lw 2 # --- red
set style line 2 lc rgb '#5e9c36' pt 1 ps 1 lt 1 lw 2 # --- green

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12
set grid mxtics mytics

set mxtics 5
set mytics 5

set xrange [0:*]

set term png


set print "extrapolations.dat"

#
# Fit / Plot critical length
#

set output 'intersect_length.png'

set ylabel 'Critical Edge Length l_0 (units of l_{ex})'
set xlabel 'Node Spacing^2 (units of l_{ex}^2)'

set key spacing 1.8

f(x) = m*x+c
fit f(x) 'intersect.dat' using ($1**2):2 via m, c
print(sprintf("Critical Length % .8f +/- %.8f", c, c_err))

plot \
    'intersect.dat' using ($1**2):2 with lp title "MERRILL solution", \
    f(x) title sprintf(\
        "f(x) = %.4f\261%.4fx + %.4f\261%.4f\nR=%.8f", \
        m, m_err, \
        c, c_err, \
        FIT_STDFIT \
    )


#
# Fit / Plot energy
#

set output 'intersect_energy.png'

set ylabel 'Critical Energy e_0 (units of K_d V)'

f(x) = m*x+c
fit f(x) 'intersect.dat' using ($1**2):3 via m, c
print(sprintf("Critical Energy % .8f +/- %.8f", c, c_err))

plot \
    'intersect.dat' using ($1**2):3 with lp title "MERRILL solution", \
    f(x) title sprintf(\
        "f(x) = %.5f\261%.5fx + %.5f\261%.5f\nR=%.8f", \
        m, m_err, \
        c, c_err, \
        FIT_STDFIT \
    )


#
# Fit everything else
#

print("")

f(x) = m*x+c

# Flower E-Anis
fit f(x) 'intersect.dat' using ($1**2):4 via m, c
print(sprintf("E-Anis-Flower   % .8f +/- %.8f", c, c_err))

# Flower E-Demag
fit f(x) 'intersect.dat' using ($1**2):5 via m, c
print(sprintf("E-Demag-Flower  % .8f +/- %.8f", c, c_err))

# Flower E-Exch
fit f(x) 'intersect.dat' using ($1**2):6 via m, c
print(sprintf("E-Exch-Flower   % .8f +/- %.8f", c, c_err))

# Flower Mx
fit f(x) 'intersect.dat' using ($1**2):7 via m, c
print(sprintf("Mx-Flower       % .8f +/- %.8f", c, c_err))

# Flower My
fit f(x) 'intersect.dat' using ($1**2):8 via m, c
print(sprintf("My-Flower       % .8f +/- %.8f", c, c_err))

# Flower Mz
fit f(x) 'intersect.dat' using ($1**2):9 via m, c
print(sprintf("Mz-Flower       % .8f +/- %.8f", c, c_err))

print("")

# Vortex E-Anis
fit f(x) 'intersect.dat' using ($1**2):10 via m, c
print(sprintf("E-Anis-Vortex   % .8f +/- %.8f", c, c_err))

# Vortex E-Demag
fit f(x) 'intersect.dat' using ($1**2):11 via m, c
print(sprintf("E-Demag-Vortex  % .8f +/- %.8f", c, c_err))

# Vortex E-Exch
fit f(x) 'intersect.dat' using ($1**2):12 via m, c
print(sprintf("E-Exch-Vortex   % .8f +/- %.8f", c, c_err))

# Vortex Mx
fit f(x) 'intersect.dat' using ($1**2):13 via m, c
print(sprintf("Mx-Vortex       % .8f +/- %.8f", c, c_err))

# Vortex My
fit f(x) 'intersect.dat' using ($1**2):14 via m, c
print(sprintf("My-Vortex       % .8f +/- %.8f", c, c_err))

# Vortex Mz
fit f(x) 'intersect.dat' using ($1**2):15 via m, c
print(sprintf("Mz-Vortex       % .8f +/- %.8f", c, c_err))
