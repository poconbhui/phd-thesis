#!/usr/bin/env bash

set -e

build_dir=$PWD/build
mkdir -p ${build_dir}

cmake \
    -B${build_dir}/cs_octahedron -H$PWD/cs_octahedron \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -DCMAKE_BUILD_TYPE:STRING=Debug

make -C ${build_dir}/cs_octahedron

#./build/cs_octahedron/cs_octahedron 0.05 0.25 0.01 0.8


cmake \
    -B${build_dir}/core_shell -H$PWD \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -DCMAKE_BUILD_TYPE:STRING=Debug \
    -DMERRILL_DIR:PATH=/home/paddy/prog/merrill/build/install/

make -C ${build_dir}/core_shell


#oxidations=(1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 0.0)
#oxidations=(0.2 0.1 0.0)
oxidations=(0.2 0.1 0.0)
data_dir=$PWD/data
mkdir -p ${data_dir}
touch ${data_dir}/intersects.dat
for oxidation in ${oxidations[@]}
do
    output_dir=${data_dir}/oxidation_${oxidation}
    mkdir -p ${output_dir}
    (
        cd ${output_dir}
        ${build_dir}/core_shell/core_shell 0.052 1 ${oxidation}
    )
done
