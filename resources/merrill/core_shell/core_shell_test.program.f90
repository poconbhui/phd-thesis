PROGRAM Core_Shell
    USE MERRILL
    IMPLICIT NONE

    INTEGER :: argc
    CHARACTER(len=1024) :: argv

    REAL(KIND=DP) :: esvd_width
    REAL(KIND=DP) :: node_spacing

    ! Subdomain numbering
    INTEGER, PARAMETER :: magnetite_sd = 1
    INTEGER, PARAMETER :: maghemite_sd = 2

    ! Material parameters
    ! Values from http://dx.doi.org/10.1002/2014GC005265
    REAL(KIND=DP), PARAMETER :: magnetite_Ms  =  4.80e5
    REAL(KIND=DP), PARAMETER :: magnetite_K1  = -1.24e4
    REAL(KIND=DP), PARAMETER :: magnetite_Aex =  1.34e-11

    REAL(KIND=DP), PARAMETER :: maghemite_Ms  =  3.80e5
    REAL(KIND=DP), PARAMETER :: maghemite_K1  = -4.60e4
    REAL(KIND=DP), PARAMETER :: maghemite_Aex =  1.00e-11

    REAL(KIND=DP), ALLOCATABLE :: m_flower(:,:)
    REAL(KIND=DP), ALLOCATABLE :: m_hard_vortex(:,:)
    REAL(KIND=DP), ALLOCATABLE :: m_vortex(:,:)

    REAL(KIND=DP) :: flower_width
    REAL(KIND=DP) :: hard_vortex_width
    REAL(KIND=DP) :: vortex_width

    REAL(KIND=DP) :: e_flower
    REAL(KIND=DP) :: e_hard_vortex
    REAL(KIND=DP) :: e_vortex

    REAL(KIND=DP) :: flower_hard_overlap_width
    REAL(KIND=DP) :: hard_easy_overlap_width

    REAL(KIND=DP) :: Ls_0

    REAL(KIND=DP) :: oxidation

    INTEGER :: i
    INTEGER :: t_max = 1000


    CALL InitializeMerrill()

    Ls_0 = Ls


    argc = COMMAND_ARGUMENT_COUNT()
    IF(argc .NE. 3) THEN
        WRITE(*,*) &
            "Usage: core_shell_test ESVD_width node_spacing_frac_lex oxidation"
        STOP 1
    END IF

    CALL GET_COMMAND_ARGUMENT(1, argv)
    READ(argv,*) esvd_width

    CALL GET_COMMAND_ARGUMENT(2, argv)
    READ(argv, *) node_spacing

    CALL GET_COMMAND_ARGUMENT(3, argv)
    READ(argv, *) oxidation

    WRITE(*,*) "ESVD Width:   ", esvd_width
    WRITE(*,*) "Node Spacing: ", node_spacing
    WRITE(*,*) "oxidation:    ", oxidation


    WRITE(argv, *) &
        "../../build/cs_octahedron/cs_octahedron ", &
        esvd_width, 0.25, &
        node_spacing*MIN( &
            l_ex(magnetite_Aex,magnetite_Ms), &
            l_ex(maghemite_Aex,maghemite_Ms)  &
        )*SQRT(Ls), &
        oxidation

    WRITE(*,*) TRIM(argv)
    CALL EXECUTE_COMMAND_LINE(TRIM(argv))

    CALL ReadMeshPat("mesh.neu")

    anisform = ANISFORM_CUBIC

    IF(NMaterials .EQ. 2) THEN
        Ms(magnetite_sd)  = magnetite_Ms
        K1(magnetite_sd)  = magnetite_K1
        Aex(magnetite_sd) = magnetite_Aex

        Ms(maghemite_sd)  = maghemite_Ms
        K1(maghemite_sd)  = maghemite_K1
        Aex(maghemite_sd) = maghemite_Aex
    ELSE IF(oxidation .LT. 0.1) THEN
        Ms  = magnetite_Ms
        K1  = magnetite_K1
        Aex = magnetite_Aex
    ELSE ! oxidation is 1
        Ms  = maghemite_Ms
        K1  = maghemite_K1
        Aex = maghemite_Aex
    END IF


    Kd = Kd_v(magnetite_Ms)


    ! Guess safe nucleation sizes
    ! Magnetite
    !flower_width = 0.035
    !hard_vortex_width = 0.042
    !vortex_width = 0.050

    ! Maghemite
    !flower_width = 0.045
    !hard_vortex_width = 0.05
    !vortex_width = 0.055

    flower_width      = 0.035*(1-oxidation) + 0.045*oxidation
    hard_vortex_width = 0.042*(1-oxidation) + 0.050*oxidation
    vortex_width      = 0.050*(1-oxidation) + 0.055*oxidation

    ! Nucleate a flower state

    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Nucleating Flower"
    WRITE(*,*) "--"
    WRITE(*,*)

    m = 1/SQRT(3.0_DP)
    Ls = ((esvd_width/(0.5*flower_width))*SQRT(Ls_0))**2

    CALL randomchange("magnetization", "20")
    CALL EnergyMin()

    Ls = ((esvd_width/(flower_width))*SQRT(Ls_0))**2
    CALL EnergyMin()

    CALL WriteTecplot(0.0_DP, "m_flower", 1)

    ALLOCATE(m_flower, SOURCE=m)


    ! Nucleate an anti-clockwise hard vortex
    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Nucleating Hard Vortex"
    WRITE(*,*) "--"
    WRITE(*,*)

    DO WHILE(.NOT. is_hard_vortex(m))
    BLOCK
        REAL(KIND=DP) :: max_radius
        REAL(KIND=DP) :: radius

        ! Set the radius to the largest y-coord or z-coord
        ! Assumes the octahedron is centered.
        max_radius = MAXVAL(VCL(:,2:3))

        ! Set magnetization along [100] axis, with a twist
        DO i=1,NNODE
            m(i,:) = [ 1.0, 0.0, 0.0 ]

            IF(SUM(VCL(i,2:3)**2) .GT. 0) THEN
                radius = NORM2(VCL(i,2:3))/max_radius
            ELSE
                radius = 0
            END IF
            IF(radius .GT. 0.01) THEN
                m(i,:) = [ &
                    radius, &
                    -VCL(i,3)/max_radius, &
                    VCL(i,2)/max_radius &
                ]
                m(i,:) = m(i,:) / NORM2(m(i,:))
            END IF
        END DO
        Ls = ((esvd_width/hard_vortex_width)*SQRT(Ls_0))**2

        CALL randomchange("magnetization", "40")
        CALL EnergyMin()

        IF(.NOT. is_hard_vortex(m)) THEN
            WRITE(*,*) "Hard vortex nucleation failed:"
            CALL CalculateEnergy(m, e_hard_vortex)
            WRITE(*,*) "<m>: ", MeanMag
        END IF
    END BLOCK
    END DO

    CALL WriteTecplot(0.0_DP, "m_hard_vortex", 1)

    ALLOCATE(m_hard_vortex, SOURCE=m)


    ! Nucleate an anti-clockwise easy vortex state
    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Nucleating Vortex"
    WRITE(*,*) "--"
    WRITE(*,*)
    DO WHILE(.NOT. is_easy_vortex(m))
    BLOCK
        REAL(KIND=DP) :: max_radius

        ! Set the radius to the largest x-coord, y-coord or z-coord
        ! Assumes the octahedron is centered.
        max_radius = MAXVAL(VCL(:,1:3))

        ! Set magnetization along [100] axis, with a twist
        DO i=1,NNODE
            ! Set m = - VCL X [111] + |VCL X [111]|*[111]
            m(i,:) = - [ & 
                VCL(i,2) - VCL(i,3), &
                VCL(i,3) - VCL(i,1), &
                VCL(i,1) - VCL(i,2)  &
            ] / max_radius / SQRT(3.0)
            m(i,:) = m(i,:) + NORM2(m(i,:)) * [ 1.0, 1.0, 1.0 ] / SQRT(3.0)
            IF(SUM(m(i,:)**2) .GT. 0.0) THEN
                m(i,:) = m(i,:) / NORM2(m(i,:))
            ELSE
                m(i,:) = [ 1.0, 1.0, 1.0 ] / SQRT(3.0)
            END IF
        END DO

        Ls = ((esvd_width/(vortex_width))*SQRT(Ls_0))**2
        CALL randomchange("magnetization", "40")
        CALL EnergyMin()

        IF(.NOT. is_easy_vortex(m)) THEN
            WRITE(*,*) "Easy vortex nucleation failed:"
            CALL CalculateEnergy(m, e_vortex)
            WRITE(*,*) "<m>: ", MeanMag
        END IF
    END BLOCK
    END DO

    CALL WriteTecplot(0.0_DP, "m_vortex", 1)

    ALLOCATE(m_vortex, SOURCE=m)


    ! Find where the flower and the hard vortex overlap
    flower_hard_overlap_width = flower_width
    DO i=1,10
    BLOCK
        INTEGER :: j
        REAL(KIND=DP) :: t

        REAL(KIND=DP) :: old_width
        old_width = flower_hard_overlap_width

        ! Find closest overlap position
        DO j=0,t_max

            t = j/REAL(t_max)
            flower_hard_overlap_width = flower_width*(1-t) + vortex_width*(t)

            Ls = ((esvd_width/flower_hard_overlap_width)*SQRT(Ls_0))**2

            CALL CalculateEnergy(m_flower, e_flower)
            CALL CalculateEnergy(m_hard_vortex, e_hard_vortex)

            ! As soon as m_flower > m_hard_vortex, break
            IF(e_flower .GE. e_hard_vortex) EXIT

        END DO

        WRITE(*,*) "i:             ", i
        WRITE(*,*) "width:         ", flower_hard_overlap_width
        WRITE(*,*) "e_flower:      ", enscale(e_flower)
        WRITE(*,*) "e_hard_vortex: ", enscale(e_hard_vortex)
        WRITE(*,*)

        IF( &
            ABS(flower_hard_overlap_width - old_width)/ABS(old_width) &
            .LT. &
            1e-5 &
        ) THEN
            WRITE(*,*) "new: ", flower_hard_overlap_width
            WRITE(*,*) "old: ", old_width
            EXIT
        END IF


        ! Minimize flower and hard vortex at current Ls
        m = m_flower
        CALL EnergyMin()
        m_flower = m

        m = m_hard_vortex
        CALL EnergyMin()
        m_hard_vortex = m
    END BLOCK
    END DO

    WRITE(*,*) "i:             ", "final"
    WRITE(*,*) "width:         ", flower_hard_overlap_width
    WRITE(*,*) "e_flower:      ", is_flower(m_flower), enscale(e_flower)
    WRITE(*,*) "e_hard_vortex: ", &
        is_hard_vortex(m_hard_vortex), enscale(e_hard_vortex)
    WRITE(*,*)

    m = m_flower
    CALL WriteTecplot(0.0_DP, "m_fh_overlap_flower", 1)
    m = m_hard_vortex
    CALL WriteTecplot(0.0_DP, "m_fh_overlap_hard", 1)


    ! Find where the hard and easy vortex overlap
    hard_easy_overlap_width = flower_width
    DO i=1,10
    BLOCK
        INTEGER :: j
        REAL(KIND=DP) :: t
        REAL(KIND=DP) :: old_width

        old_width = hard_easy_overlap_width

        ! Find closest overlap position
        DO j=0,t_max

            t = j/REAL(t_max)
            hard_easy_overlap_width = &
                flower_width*(1-t) + vortex_width*(t)

            Ls = ((esvd_width/hard_easy_overlap_width)*SQRT(Ls_0))**2

            CALL CalculateEnergy(m_hard_vortex, e_hard_vortex)
            CALL CalculateEnergy(m_vortex, e_vortex)

            ! As soon as m_flower > m_hard_vortex, break
            IF(e_hard_vortex .GE. e_vortex) EXIT

        END DO

        IF( &
            ABS(hard_easy_overlap_width - old_width)/ABS(old_width) &
            .LT. &
            1e-5 &
        ) THEN
            WRITE(*,*) "new: ", hard_easy_overlap_width
            WRITE(*,*) "old: ", old_width
            EXIT
        END IF

        ! Minimize hard and easy vortex at current Ls
        m = m_hard_vortex
        CALL EnergyMin()
        IF(is_hard_vortex(m)) THEN
            m_hard_vortex = m
        ELSE
            WRITE(*,*) "SKIPPING: m_hard_vortex = m"
        END IF

        m = m_vortex
        CALL EnergyMin()
        IF(is_easy_vortex(m)) THEN
            m_vortex = m
        ELSE
            WRITE(*,*) "SKIPPING: m_vortex = m"
        END IF

        WRITE(*,*) "i:             ", i
        WRITE(*,*) "width:         ", hard_easy_overlap_width
        WRITE(*,*) "e_hard_vortex: ", &
            is_hard_vortex(m_hard_vortex), enscale(e_hard_vortex)
        WRITE(*,*) "e_vortex:      ", is_easy_vortex(m_vortex), enscale(e_vortex)
        WRITE(*,*)

    END BLOCK
    END DO

    WRITE(*,*) "i:             ", "final"
    WRITE(*,*) "width:         ", hard_easy_overlap_width
    WRITE(*,*) "e_hard_vortex: ", &
        is_hard_vortex(m_hard_vortex), enscale(e_hard_vortex)
    WRITE(*,*) "e_vortex:      ", is_easy_vortex(m_vortex), enscale(e_vortex)
    WRITE(*,*)
    
    m = m_hard_vortex
    CALL WriteTecplot(0.0_DP, "m_he_overlap_hard", 1)
    m = m_vortex
    CALL WriteTecplot(0.0_DP, "m_he_overlap_easy", 1)


    WRITE(*, "(3A16)")       "",            "ESVD",                    "Energy"
    WRITE(*, "(A16,2E16.7)") "Flower/Hard", flower_hard_overlap_width, e_flower
    WRITE(*, "(A16,2E16.7)") "Hard/Easy",   hard_easy_overlap_width,   e_vortex

    BLOCK
        INTEGER :: fd
        OPEN(NEWUNIT=fd, FILE="../intersects.dat", ACTION="WRITE", ACCESS="APPEND")
        WRITE(fd,*) &
            oxidation, &
            flower_hard_overlap_width, e_flower, &
            hard_easy_overlap_width,   e_vortex
        CLOSE(fd)
    END BLOCK

CONTAINS
    FUNCTION Kd_v(Ms)
        REAL(KIND=DP) :: Kd_v
        REAL(KIND=DP), INTENT(IN) :: Ms

        Kd_v = 0.5*mu*Ms**2
    END FUNCTION Kd_v

    FUNCTION l_ex(A, Ms)
        REAL(KIND=DP) :: l_ex
        REAL(KIND=DP), INTENT(IN) :: A
        REAL(KIND=DP), INTENT(IN) :: Ms

        l_ex = SQRT(A/Kd_v(Ms))
    END FUNCTION l_ex

    SUBROUTINE CalculateEnergy(target_m, etot)
        REAL(KIND=DP), INTENT(IN) :: target_m(:,:)
        REAL(KIND=DP), INTENT(OUT) :: etot

        INTEGER :: neval
        REAL(KIND=DP), ALLOCATABLE :: G(:), X(:)
        REAL(KIND=DP), ALLOCATABLE :: old_m(:,:)

        INTEGER :: i

        ALLOCATE(G(2*NNODE), X(2*NNODE))

        ! Backup m
        ALLOCATE(old_m, SOURCE=m)

        m = target_m

        neval = 1
        MeanMag = 0
        ! Theta (co-lat) indicies= 2n-1, phi (azimuth)  indicies= 2n
        DO i=1,NNODE
          X(2*i-1) = ACOS(m(i,3))
          X(2*i)   = ATAN2(m(i,2),m(i,1))
          MeanMag(:) = MeanMag(:) + m(i,:)*vbox(i)
        END DO
        MeanMag(:)=MeanMag(:)/total_volume

        CALL ET_GRAD(etot,G,X,neval)

        m = old_m
    END SUBROUTINE CalculateEnergy

    FUNCTION enscale(energy)
        REAL(KIND=DP) :: enscale
        REAL(KIND=DP), INTENT(IN) :: energy

        enscale = energy/(Kd_v(magnetite_Ms)*total_volume)
    END FUNCTION enscale

    PURE FUNCTION mean_mag(m)
        REAL(KIND=DP) :: mean_mag(3)
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        INTEGER :: i

        mean_mag = 0
        DO i=1,NNODE
          mean_mag = mean_mag + m(i,:)*vbox(i)
        END DO
        mean_mag = mean_mag/total_volume
    END FUNCTION mean_mag

    FUNCTION is_flower(m)
        LOGICAL :: is_flower
        REAL(KIND=DP), INTENT(IN) :: m(:,:)

        IF(NORM2(mean_mag(m)) .GT. 0.85) THEN
            is_flower = .TRUE.
        ELSE
            is_flower = .FALSE.
        END IF
    END FUNCTION is_flower

    FUNCTION is_hard_vortex(m)
        LOGICAL :: is_hard_vortex
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        REAL(KIND=DP) :: mm(3)

        mm = mean_mag(m)

        ! Hard vortex has |m| < 0.8 (usually around 0.7 or 0.5)
        ! and m(1) is much bigger than m(2) and m(3)
        IF( &
            NORM2(mean_mag(m)) .LE. 0.85 &
            .AND. &
            ABS(NORM2(mean_mag(m)) - mm(1))/ABS(mm(1)) .LT. 0.1 &
        ) THEN
            is_hard_vortex = .TRUE.
        ELSE
            is_hard_vortex = .FALSE.
        END IF
    END FUNCTION is_hard_vortex

    FUNCTION is_easy_vortex(m)
        LOGICAL :: is_easy_vortex
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        REAL(KIND=DP) :: mm(3)

        mm = mean_mag(m)

        ! Easy vortex is usually pretty tight, around |m| = 0.3 to 0.5
        ! m(1), m(2) and m(3) are about the same size.
        IF( &
            NORM2(mm) .LT. 0.60 &
            .AND. &
            SUM(ABS(mm)) .GT. 2*mm(1) &
        ) THEN
            is_easy_vortex = .TRUE.
        ELSE
            is_easy_vortex = .FALSE.
        END IF
    END FUNCTION is_easy_vortex
END PROGRAM Core_Shell
