set style line 1 lc rgb '#8b1a0e' pt 1 ps 1 lt 1 lw 2 # --- red
set style line 2 lc rgb '#5e9c36' pt 1 ps 1 lt 1 lw 2 # --- green

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12
set grid mxtics mytics

set mxtics 5
set mytics 5

#set xrange [0:100]

set term png
set output "data/intersects.png"

set xlabel "Oxidation"
set ylabel "Grain Size (ESVD {/Symbol m}m)"

set key bottom right
set style textbox opaque noborder
set label 1 "ESV" at 0.2,0.05 right boxed
set label 2 "HSV" at 0.3,0.045 right boxed
set label 3 "SD/FS" at 0.6,0.041 right boxed
plot \
    '< sort -n data/intersects.dat' u 1:2 w lp ti "FS/HSV critical energy", \
    '< sort -n data/intersects.dat' u 1:4 w lp ti "HSV/ESV critical energy"
#set label "S" at plot 0.5,0.5 center font "Symbol,24"
#set label 1 "ESV"  front font "sans,12pt" boxed
replot
