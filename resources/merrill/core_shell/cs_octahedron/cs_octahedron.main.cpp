#define BOOST_PARAMETER_MAX_ARITY 12

#include "Octahedron.hpp"
#include "output_to_patran.hpp"

#include <iostream>
#include <vector>
#include <array>
#include <fstream>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/Polyhedron_3.h>

#include <CGAL/Side_of_triangle_mesh.h>

#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>

#include <CGAL/Implicit_mesh_domain_3.h>
#include <CGAL/Mesh_domain_with_polyline_features_3.h>
#include <CGAL/make_mesh_3.h>


typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel>         Polyhedron;
typedef Polyhedron::HalfedgeDS             HalfedgeDS;

typedef CGAL::Side_of_triangle_mesh<Polyhedron, Kernel> SideOfPolyhedron;

typedef Kernel::RT RT;
typedef Kernel::Point_3 Point;

typedef std::function<int(const Point&)> Function;
class LMDFunction {
public:
    const Function& function;
    typedef Function::result_type return_type;
    LMDFunction(const Function& function): function(function) {}
    Function::result_type operator()(const Point& p, bool=true) const {
        return function(p);
    }
};
typedef
    CGAL::Mesh_domain_with_polyline_features_3<
        CGAL::Labeled_mesh_domain_3<LMDFunction,Kernel>
    >
    Mesh_domain;

typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
typedef
    CGAL::Mesh_complex_3_in_triangulation_3<
        Tr,Mesh_domain::Corner_index,Mesh_domain::Curve_segment_index
    >
    C3t3;
typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;


class CoreShellOctahedronLevelSet {
public:
    typedef int return_type;
    const Polyhedron& inner;
    const Polyhedron& outer;
    const SideOfPolyhedron side_of_inner;
    const SideOfPolyhedron side_of_outer;
    const return_type index_inner;
    const return_type index_outer;


    CoreShellOctahedronLevelSet(
        const Polyhedron& inner, const Polyhedron& outer,
        const return_type& index_inner, const return_type& index_outer
    ):
        inner(inner),
        outer(outer),
        side_of_inner(inner),
        side_of_outer(outer),
        index_inner(index_inner),
        index_outer(index_outer)
    {}

    int operator()(const ::Point& p) const {
        if(
            !inner.is_empty()
            && side_of_inner(p) != CGAL::ON_UNBOUNDED_SIDE
        ) {
            return index_inner;
        } else if(
            !outer.is_empty()
            && side_of_outer(p) != CGAL::ON_UNBOUNDED_SIDE
        ) {
            return index_outer;
        } else {
            return 0;
        }
    }
};

template <typename Polyhedron, typename C3t3>
void add_polyhedron_edges_to_c3t3(
    Polyhedron& polyhedron, C3t3& c3t3, double edge_len, int index
) {
    //using CGAL::Mesh_3::internal::weight_modifier;
    //typedef typename Tr::Geom_traits::Point_3 Weighted_point;
    //// Insert origin
    //{
    //    typename Polyhedron::Point point(CGAL::ORIGIN);

    //    // The cell containing the next point
    //    typename Tr::Locate_type lt;
    //    int li, lj;
    //    const typename Tr::Cell_handle ch0 =
    //        c3t3.triangulation().locate(point, lt, li, lj);

    //    // If the point is already a vertex, use that vertex handle,
    //    // otherwise add the point to the triangulation.
    //    if(lt != Tr::VERTEX) {
    //        c3t3.triangulation().insert(
    //            Weighted_point(point, edge_len*weight_modifier/2),
    //            lt, ch0, li, lj
    //        );
    //    }
    //}

    //// Insert polyhedron corners
    //typedef typename Polyhedron::Point_iterator Pit;
    //for(
    //    Pit pit = polyhedron.points_begin();
    //    pit != polyhedron.points_end();
    //    pit++
    //) {
    //    // The cell containing the next point
    //    typename Tr::Locate_type lt;
    //    int li, lj;
    //    const typename Tr::Cell_handle ch0 =
    //        c3t3.triangulation().locate(*pit, lt, li, lj);

    //    // If the point is already a vertex, use that vertex handle,
    //    // otherwise add the point to the triangulation.
    //    if(lt != Tr::VERTEX) {
    //        c3t3.triangulation().insert(
    //            Weighted_point(*pit, edge_len*weight_modifier/2),
    //            lt, ch0, li, lj
    //        );
    //    }
    //}

    typedef typename Polyhedron::Edge_iterator Hit;
    for(
        Hit hit = polyhedron.edges_begin();
        hit != polyhedron.edges_end();
        hit++
    ) {
        // Add this edge if the angle between the incident facets is
        // smaller than the requested angle_bound.

        //
        // Get the normals of the two incident facets
        //
        typedef typename Polyhedron::Traits::Vector_3 Vector_3;
        Vector_3 n1 = CGAL::Polygon_mesh_processing::compute_face_normal(
            hit->facet(), polyhedron
        );
        Vector_3 n2 = CGAL::Polygon_mesh_processing::compute_face_normal(
            hit->opposite()->facet(), polyhedron
        );


        // Use dot product to find angle between the facets
        const double dot   = std::max(std::min(n1*n2, 1.0), -1.0);
        const double pi    = (4*std::atan(1.0));
        const double angle = 180/pi*std::abs(std::acos(dot));

        // Add this edge to the set if the angle is large enough.
        const double detected_angle_bound = 30;
        if(angle > detected_angle_bound) {
            // We want to split the possibly long edge into a number of
            // edges of length edge_len. So, first we'll find how many
            // points that will take, and the distance between the points.

            typedef typename Polyhedron::Point Point;
            const Point& p1 = hit->vertex()->point();
            const Point& p2 = hit->opposite()->vertex()->point();

            // Total distance
            const double distance = CGAL::sqrt(CGAL::squared_distance(p1, p2));

            // Number of points for edges of length at most edge_len
            const Kernel::RT n_points = std::max(
                Kernel::RT(std::ceil(distance / edge_len)),
                Kernel::RT(3)
            );

            // A vector encoding the length from one point to the next
            const Vector_3 step = (p2 - p1)/n_points;

            // Radius of the protecting ball of the edge.
            // This, unfortunately, can't really be any smaller.
            const double w = CGAL::square(distance/n_points/1.85);

            // We will go from the first point to the last point, stepping
            // through each point on the way.
            // We'll check if a given point is already in the triangulation.
            // If it is, return a handle to that vertex, if not, add the
            // point and get the handle from that vertex.
            // Then add the edge consisting of the previous point and the
            // newly found or created point.

            // Handle to the vertex found in the previous iteration.
            typename Tr::Vertex_handle prev;
            for(Kernel::RT i=0; i<=n_points; i++) {
                using CGAL::Mesh_3::internal::weight_modifier;
                typedef typename C3t3::Triangulation Tr;
                typedef typename Tr::Geom_traits::Point_3 Weighted_point;

                // The next point.
                // When i = n_points, this *should* return v2->point(),
                // when using the Epick kernel, even though it's not
                // strictly floating point safe.
                const Point point = p1 + i*step;

                // The cell containing the next point
                typename Tr::Locate_type lt;
                int li, lj;
                const typename Tr::Cell_handle ch0 =
                    c3t3.triangulation().locate(point, lt, li, lj);

                // Vertex handle for the next (current) point
                typename Tr::Vertex_handle next;

                // If the point is already a vertex, use that vertex handle,
                // otherwise add the point to the triangulation.
                if(lt == Tr::VERTEX) {
                    next = ch0->vertex(li);
                } else {
                    next = c3t3.triangulation().insert(
                        Weighted_point(point, w*weight_modifier),
                        lt, ch0, li, lj
                    );
                }

                // This is needed for the first iteration, since we
                // don't have a "previous" point.
                if(prev != typename Tr::Vertex_handle()) {
                    if(!c3t3.is_in_complex(prev, next)) {
                        c3t3.add_to_complex(prev, next, index);
                    }
                }

                // set prev to next for next iteration.
                prev = next;
            }
        }
    }
}


double polyhedron_volume(const Polyhedron& polyhedron) {
    double volume = 0;

    for(
        Polyhedron::Facet_const_iterator fit = polyhedron.facets_begin();
        fit != polyhedron.facets_end();
        fit++
    ) {
        Polyhedron::Halfedge_around_facet_const_circulator hc = fit->facet_begin();
        Polyhedron::Point p1 = hc->vertex()->point();
        hc++;
        Polyhedron::Point p2 = hc->vertex()->point();
        hc++;
        Polyhedron::Point p3 = hc->vertex()->point();
        hc++;

        volume += CGAL::volume(p1, p2, p3, Polyhedron::Point(CGAL::ORIGIN));
    }

    return std::abs(volume);
}

int main(int argc, char* argv[]) {
    if(argc != 5) {
        std::cout
            << "Usage: cs_octahedron ESVD_len truncation edge_len fraction"
            << std::endl;
        return 1;
    }

    double len        = atof(argv[1]);
    double truncation = atof(argv[2]);
    double edge_len   = atof(argv[3]);
    double fraction   = atof(argv[4]);

    if(fraction < 0 || fraction > 1) {
        std::cout
            << "Error: fraction is " << fraction
            << std::endl
            << "Error: allowed values: 0 <= fraction <= 1"
            << std::endl;
        return 1;
    }

    std::cout
        << "Targets:"
        << std::endl
        << "  len:        " << len
        << std::endl
        << "  truncation: " << truncation
        << std::endl
        << "  edge_len:   " << edge_len
        << std::endl
        << "  fraction:   " << fraction
        << std::endl
        << std::endl;

    Polyhedron outer_octahedron;
    Polyhedron inner_octahedron;

    if(fraction != 0) {
        std::cout << "Setting outer octahedron" << std::endl;
        outer_octahedron = Octahedron<Polyhedron>(
            len, len, len, truncation
        );
    }
    if(fraction != 1) {
        std::cout << "Setting inner octahedron" << std::endl;
        inner_octahedron = Octahedron<Polyhedron>(
            len, len, len, truncation
        );
    }

    // Rescale polyhedra to ESVD
    double volume = std::max(
        polyhedron_volume(outer_octahedron),
        polyhedron_volume(inner_octahedron)
    );
    const double pi = 4*atan(1.0);
    const double initial_esvd  = std::pow(volume*(3.0/4.0)/pi,1/3.0);
    const double target_esvd = len;
    for(
        Polyhedron::Point_iterator pit = outer_octahedron.points_begin();
        pit != outer_octahedron.points_end();
        pit++
    ) {
        *pit = Polyhedron::Point(CGAL::ORIGIN)
            + (*pit - CGAL::ORIGIN)*target_esvd/initial_esvd;
    }
    for(
        Polyhedron::Point_iterator pit = inner_octahedron.points_begin();
        pit != inner_octahedron.points_end();
        pit++
    ) {
        *pit = Polyhedron::Point(CGAL::ORIGIN)
            + (*pit - CGAL::ORIGIN)*target_esvd/initial_esvd;
    }

    volume = std::max(
        polyhedron_volume(outer_octahedron),
        polyhedron_volume(inner_octahedron)
    );

    // Find scale for inner_polyhedron to match fraction volume
    double len_outer = std::pow(volume,1/3.0);
    double target_len_inner = std::pow((1-fraction)*volume,1/3.0);
    for(
        Polyhedron::Point_iterator pit = inner_octahedron.points_begin();
        pit != inner_octahedron.points_end();
        pit++
    ) {
        *pit = Polyhedron::Point(CGAL::ORIGIN)
            + (*pit - CGAL::ORIGIN)*target_len_inner/len_outer;
    }



    // Mesh criteria
    // Equilateral tetrahedron circumsphere radius to edge length:               
    // r = (e/4)*sqrt(6)                                                         
    // r*(4/sqrt(6)) = e 
    // Equilateral triangle circumsphere radius to edge length:
    // r = e/sqrt(3)
    using namespace CGAL::parameters;
    Mesh_criteria criteria(
        edge_size = 2*edge_len,
        facet_angle = 25, facet_size = edge_len/sqrt(3),
        facet_distance = edge_len*1e-6,
        cell_radius_edge_ratio = 2, cell_size = (edge_len/4)*sqrt(6)
    );


    Function f_inner = std::bind(
        &CoreShellOctahedronLevelSet::operator(),
        std::make_shared<CoreShellOctahedronLevelSet>(
            inner_octahedron, outer_octahedron,
            1, 2
        ),
        std::placeholders::_1
    );

    Mesh_domain domain_inner(
        LMDFunction(f_inner),
        Kernel::Sphere_3(Point(0, 0, 0), (10*len)*(10*len)),
        edge_len*1e-12
    );


    C3t3 c3t3;

    double x1_max = std::numeric_limits<double>::min();
    double x2_max = std::numeric_limits<double>::min();
    for(
        Polyhedron::Point_iterator pit = outer_octahedron.points_begin();
        pit != outer_octahedron.points_end();
        pit++
    ) {
        if(x1_max < std::abs(pit->x())) x1_max = std::abs(pit->x());
    }
    for(
        Polyhedron::Point_iterator pit = inner_octahedron.points_begin();
        pit != inner_octahedron.points_end();
        pit++
    ) {
        if(x2_max < std::abs(pit->x())) x2_max = std::abs(pit->x());
    }
    std::cout << "x1_max: " << x1_max << std::endl;
    std::cout << "x2_max: " << x2_max << std::endl;
    double feature_edge_len = std::min(edge_len, 0.7*std::abs(x1_max-x2_max));
    if(fraction == 0 || fraction == 1) feature_edge_len = edge_len;

    std::cout << "edge_len:         " << edge_len << std::endl;
    std::cout << "feature_edge_len: " << feature_edge_len << std::endl;
    add_polyhedron_edges_to_c3t3(inner_octahedron, c3t3, feature_edge_len, 1);
    add_polyhedron_edges_to_c3t3(outer_octahedron, c3t3, feature_edge_len, 2);

    refine_mesh_3(
        c3t3, domain_inner, criteria,
        exude(), perturb(), odt(), lloyd(),
        no_reset_c3t3()
    );

    // Needed to fill in potential holes created by optimizations
    refine_mesh_3(
        c3t3, domain_inner, criteria,
        no_exude(), no_perturb(), no_odt(), no_lloyd(),
        no_reset_c3t3()
    );


    double edge_average = 0;
    std::size_t edge_count = 0;
    for(
        C3t3::Triangulation::Finite_edges_iterator eit = c3t3.triangulation().finite_edges_begin();
        eit != c3t3.triangulation().finite_edges_end();
        eit++
    ) {
        edge_average += sqrt(abs(CGAL::squared_distance(
            eit->first->vertex(eit->second)->point(),
            eit->first->vertex(eit->third)->point()
        )));
        edge_count++;
    }

    double sharp_edge_average = 0;
    std::size_t sharp_edge_count = 0;
    std::cout << "edges in complex:   " << c3t3.number_of_edges_in_complex() << std::endl;
    for(
        C3t3::Edges_in_complex_iterator eit = c3t3.edges_in_complex_begin();
        eit != c3t3.edges_in_complex_end();
        eit++
    ) {
        sharp_edge_average += sqrt(abs(CGAL::squared_distance(
            eit->first->vertex(eit->second)->point(),
            eit->first->vertex(eit->third)->point()
        )));
        sharp_edge_count++;
    }

    double internal_edge_average = 0;
    std::size_t internal_edge_count = 0;
    for(
        C3t3::Triangulation::Finite_edges_iterator eit = c3t3.triangulation().finite_edges_begin();
        eit != c3t3.triangulation().finite_edges_end();
        eit++
    ) {
        if(
            ! c3t3.is_in_complex(
                eit->first->vertex(eit->second),
                eit->first->vertex(eit->third)
            )
        ) {
            internal_edge_average += sqrt(abs(CGAL::squared_distance(
                eit->first->vertex(eit->second)->point(),
                eit->first->vertex(eit->third)->point()
            )));
            internal_edge_count++;
        }
    }

    std::cout
        << "Result:"
        << std::endl
        << "  vertices:     " << c3t3.triangulation().number_of_vertices()
        << std::endl
        << "  cells:        " << c3t3.number_of_cells()
        << std::endl
        << "  n_edges:      " << edge_count
        << std::endl
        << "  edge_average: " << edge_average/edge_count
        << std::endl
        << "  n_sharp_edges:      " << sharp_edge_count
        << std::endl
        << "  sharp_edge_average: " << sharp_edge_average/sharp_edge_count
        << std::endl
        << "  internal_edge_average: " << internal_edge_average/internal_edge_count
        << std::endl
        << "  volume_inner: " << polyhedron_volume(inner_octahedron)
        << std::endl
        << "  volume_outer: " << std::max(
            polyhedron_volume(outer_octahedron)
            - polyhedron_volume(inner_octahedron),
            0.0
        )
        << std::endl
        << "  total volume: " << std::max(
            polyhedron_volume(outer_octahedron),
            polyhedron_volume(inner_octahedron)
        )
        << std::endl
        << std::endl;


    typedef std::vector<std::array<std::size_t,4>> Connectivities;
    Connectivities connectivities;
    typedef std::vector<Point> Points;
    Points points;
    typedef std::vector<int> CellIds;
    CellIds cell_ids;

    typedef std::map<C3t3::Vertex_handle,std::size_t> VertexIndexMap;
    VertexIndexMap vertex_index_map;

    std::size_t index = 0;
    typedef C3t3::Triangulation Triangulation;
    const C3t3::Triangulation& tr = c3t3.triangulation();
    for(
        Triangulation::Finite_vertices_iterator vit = tr.finite_vertices_begin();
        vit != tr.finite_vertices_end();
        vit++
    ) {
        points.push_back(vit->point());

        vertex_index_map[vit] = index;
        index++;
    }

    for(
        C3t3::Cells_in_complex_iterator cit = c3t3.cells_in_complex_begin();
        cit != c3t3.cells_in_complex_end();
        //Triangulation::Finite_cells_iterator cit = tr.finite_cells_begin();
        //cit != tr.finite_cells_end();
        cit++
    ) {
        //const int index = f_inner(
        //    CGAL::centroid(
        //        cit->vertex(0)->point().point(),
        //        cit->vertex(1)->point().point(),
        //        cit->vertex(2)->point().point(),
        //        cit->vertex(3)->point().point()
        //    )
        //);

        //if(index != 0) {
            connectivities.push_back({
                vertex_index_map[cit->vertex(0)],
                vertex_index_map[cit->vertex(1)],
                vertex_index_map[cit->vertex(2)],
                vertex_index_map[cit->vertex(3)]
            });

            cell_ids.push_back(c3t3.subdomain_index(cit));
            //cell_ids.push_back(index);
        //}
    }


    std::ofstream mesh_neu("mesh.neu");
    output_to_patran(
        mesh_neu,
        points.begin(), points.end(),
        connectivities.begin(), connectivities.end(),
        cell_ids.begin(), cell_ids.end()
    );
}
