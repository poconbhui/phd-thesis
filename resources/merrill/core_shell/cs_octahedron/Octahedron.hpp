#ifndef CS_OCTAHEDRON_OCTAHEDRON_HPP
#define CS_OCTAHEDRON_OCTAHEDRON_HPP

#include "Cube.hpp"
#include "PolyhedronConverter.hpp"

#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Nef_polyhedron_3.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>


///////////////////////////////////////////////////////////////////////////////
// OctahedronModifier                                                        //
///////////////////////////////////////////////////////////////////////////////
//
// A modifier class which generates an octahedron from a CGAL::Polyhedron.
//
template<typename Polyhedron>
class OctahedronModifier:
    public CGAL::Modifier_base<typename Polyhedron::HalfedgeDS>
{
public:
    typedef typename Polyhedron::Traits::FT FT;
    typedef typename Polyhedron::HalfedgeDS HDS;
    typedef typename HDS::Vertex Vertex;
    typedef typename Vertex::Point Point;


    OctahedronModifier(FT len_x, FT len_y, FT len_z):
        half_x(len_x/FT(2)), half_y(len_y/FT(2)), half_z(len_z/FT(2))
    {}


    void operator()(HDS& hds) {
        int num_vertices = 6;
        int num_facets = 8;

        CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);
        B.begin_surface(num_vertices, num_facets);

        // Add all the corner points
        B.add_vertex(Point( half_x, 0.0,    0.0));
        B.add_vertex(Point(-half_x, 0.0,    0.0));
        B.add_vertex(Point( 0.0,    half_y, 0.0));
        B.add_vertex(Point( 0.0,   -half_y, 0.0));
        B.add_vertex(Point( 0.0,    0.0,    half_z));
        B.add_vertex(Point( 0.0,    0.0,   -half_z));

        // Connect the top half
        B.begin_facet();
            B.add_vertex_to_facet(0);
            B.add_vertex_to_facet(2);
            B.add_vertex_to_facet(4);
        B.end_facet();

        B.begin_facet();
            B.add_vertex_to_facet(0);
            B.add_vertex_to_facet(4);
            B.add_vertex_to_facet(3);
        B.end_facet();

        B.begin_facet();
            B.add_vertex_to_facet(0);
            B.add_vertex_to_facet(3);
            B.add_vertex_to_facet(5);
        B.end_facet();

        B.begin_facet();
            B.add_vertex_to_facet(0);
            B.add_vertex_to_facet(5);
            B.add_vertex_to_facet(2);
        B.end_facet();

        // Connect the bottom half
        B.begin_facet();
            B.add_vertex_to_facet(1);
            B.add_vertex_to_facet(4);
            B.add_vertex_to_facet(2);
        B.end_facet();

        B.begin_facet();
            B.add_vertex_to_facet(1);
            B.add_vertex_to_facet(3);
            B.add_vertex_to_facet(4);
        B.end_facet();

        B.begin_facet();
            B.add_vertex_to_facet(1);
            B.add_vertex_to_facet(5);
            B.add_vertex_to_facet(3);
        B.end_facet();

        B.begin_facet();
            B.add_vertex_to_facet(1);
            B.add_vertex_to_facet(2);
            B.add_vertex_to_facet(5);
        B.end_facet();

        B.end_surface();

    }


    FT half_x, half_y, half_z;
};


///////////////////////////////////////////////////////////////////////////////
// Octahedron                                                                //
///////////////////////////////////////////////////////////////////////////////
//
// A CGAL::Polyhedron representing a truncated Octahedron.
//
template<typename Polyhedron>
class Octahedron: public Polyhedron {
public:

    Octahedron(
        double len_x, double len_y, double len_z,
        double truncation = 0.2
    ):
        Polyhedron()
    {
        if(len_x < 0.0 || len_y < 0.0 || len_z < 0.0) {
            throw std::domain_error(
                "Error: meshrrill::Octahedron: "
                "Parameters len_x, len_y len_z must be > 0."
            );
        }
        if(truncation < 0.0 || truncation >= 1.0) {
            throw std::domain_error(
                "Error: meshrrill::Octahedron: "
                "Parameter truncation must be within range [0.0, 1.0)."
            );
        }


        //
        // We'll generate a truncated octahedron by finding the intersection
        // of a full octahedron with a cube.
        //


        // Define types. We use an exact kernel type to avoid problems
        // with small, but reasonable, truncation values.
        typedef
            CGAL::Filtered_kernel<
                CGAL::Exact_predicates_exact_constructions_kernel_with_sqrt
            >
            EKernel;
        typedef CGAL::Polyhedron_3<EKernel> EPolyhedron;
        typedef CGAL::Nef_polyhedron_3<EKernel> ENef_polyhedron;


        // Generate an Octahedron
        EPolyhedron full_octahedron;
        OctahedronModifier<EPolyhedron> octahedron_modifier(
            len_x, len_y, len_z
        );
        full_octahedron.delegate(octahedron_modifier);

        ENef_polyhedron enef_octahedron(full_octahedron);


        // Generate a cube representing the truncation
        EPolyhedron cube = Cube<EPolyhedron>(
            (1-truncation)*len_x, (1-truncation)*len_y, (1-truncation)*len_z
        );
        CGAL::Polygon_mesh_processing::triangulate_faces(cube);

        ENef_polyhedron enef_cube(cube);


        // Find the intersection of the octahedron and the cube.
        ENef_polyhedron enef_truncated_octahedron = enef_octahedron*enef_cube;


        // Convert the ENef_polyhedron to a EPolyhedron
        EPolyhedron etruncated_octahedron;
        enef_truncated_octahedron.convert_to_polyhedron(etruncated_octahedron);


        // Generate this class's polyhedron from the exact polyhedron.
        PolyhedronConverter<EPolyhedron, Polyhedron> polyhedron_converter(
            etruncated_octahedron
        );
        Polyhedron::delegate(polyhedron_converter);
    }

};


#endif  // CS_OCTAHEDRON_OCTAHEDRON_HPP
