#ifndef CS_OCTAHEDRON_CUBE_HPP_
#define CS_OCTAHEDRON_CUBE_HPP_


#include <CGAL/Modifier_base.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>


///////////////////////////////////////////////////////////////////////////////
// CubeModifier                                                              //
///////////////////////////////////////////////////////////////////////////////
//
// A modifier class to generate a cube from a CGAL::Polyhedron_3
//
template<typename Polyhedron>
class CubeModifier:
    public CGAL::Modifier_base<typename Polyhedron::HalfedgeDS>
{
public:
    typedef typename Polyhedron::Traits::FT FT;
    typedef typename Polyhedron::HalfedgeDS HDS;
    typedef typename HDS::Vertex Vertex;
    typedef typename Vertex::Point Point;


    CubeModifier(FT len_x, FT len_y, FT len_z):
        half_x(len_x/FT(2)), half_y(len_y/FT(2)), half_z(len_z/FT(2))
    {}


    void operator()(HDS& hds) {
        int num_vertices = 8;
        int num_facets = 6;

        CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);
        B.begin_surface(num_vertices, num_facets);


        // Add all vertices

        // Front
        B.add_vertex(Point( half_x,  half_y,  half_z));
        B.add_vertex(Point( half_x, -half_y,  half_z));
        B.add_vertex(Point( half_x, -half_y, -half_z));
        B.add_vertex(Point( half_x,  half_y, -half_z));

        // Back
        B.add_vertex(Point(-half_x,  half_y,  half_z));
        B.add_vertex(Point(-half_x, -half_y,  half_z));
        B.add_vertex(Point(-half_x, -half_y, -half_z));
        B.add_vertex(Point(-half_x,  half_y, -half_z));


        // Add all facets

        // Front (x = half_x)
        B.begin_facet();
            B.add_vertex_to_facet(0);
            B.add_vertex_to_facet(1);
            B.add_vertex_to_facet(2);
            B.add_vertex_to_facet(3);
        B.end_facet();

        // Back (x = -half_x)
        B.begin_facet();
            B.add_vertex_to_facet(7);
            B.add_vertex_to_facet(6);
            B.add_vertex_to_facet(5);
            B.add_vertex_to_facet(4);
        B.end_facet();

        // Right (y = half_y)
        B.begin_facet();
            B.add_vertex_to_facet(0);
            B.add_vertex_to_facet(3);
            B.add_vertex_to_facet(7);
            B.add_vertex_to_facet(4);
        B.end_facet();

        // Left (y = -half_y)
        B.begin_facet();
            B.add_vertex_to_facet(2);
            B.add_vertex_to_facet(1);
            B.add_vertex_to_facet(5);
            B.add_vertex_to_facet(6);
        B.end_facet();

        // Top (z = half_z)
        B.begin_facet();
            B.add_vertex_to_facet(1);
            B.add_vertex_to_facet(0);
            B.add_vertex_to_facet(4);
            B.add_vertex_to_facet(5);
        B.end_facet();

        // Bottom (z = -half_z)
        B.begin_facet();
            B.add_vertex_to_facet(3);
            B.add_vertex_to_facet(2);
            B.add_vertex_to_facet(6);
            B.add_vertex_to_facet(7);
        B.end_facet();

        B.end_surface();

    }


    FT half_x, half_y, half_z;
};


///////////////////////////////////////////////////////////////////////////////
// Cube                                                                      //
///////////////////////////////////////////////////////////////////////////////
//
// A Polyhedron representing a cube.
//
template<typename Polyhedron>
class Cube: public Polyhedron {
public:

    Cube(double len_x, double len_y, double len_z):
        Polyhedron()
    {
        CubeModifier<Polyhedron> cube_modifier(
            len_x, len_y, len_z
        );
        Polyhedron::delegate(cube_modifier);
    }

};



#endif  // CS_OCTAHEDRON_CUBE_HPP_
