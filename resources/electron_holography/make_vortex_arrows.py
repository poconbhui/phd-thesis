#!/usr/bin/env pvpython

from sys import argv
print(argv)
import sys
sys.exit

if len(argv) < 3:
    print("Usage: pvpython %s paraview_state output_prefix")
    import sys
    sys.exit(1)

paraview_state = argv[1]
output_prefix  = argv[2]

from paraview.simple import *

servermanager.LoadPlugin("/home/paddy/tmp/install-holomag/holomag-2.0.0-Linux-x86-64-ParaView-5.3.0/lib/holomag/libholomag-pv5.3.so")
servermanager.LoadPlugin("/home/paddy/prog/merrill/demo/paraview-plugins/MERRILL-plugins.xml")
servermanager.LoadState(paraview_state)

views = [FindView("Arrows"), FindView("+1"), FindView("Arrows + 1")]
names = ["arrows", "plus_1", "arrows_plus_1"]


# Fix cube edge width
for view, name in zip(views,names):
    if name in ["arrows", "arrows_plus_1"]:
        GetDisplayProperties(FindSource("FeatureEdges1"), view).LineWidth = 8

# Fix the display sizes
for view in views:
    view.ViewSize = [1000, 1000]

for view, name in zip(views, names):
    view.UseOffscreenRenderingForScreenshots = 1
    view.UseOffscreenRendering = 1

    def find_bounds(source):
        source = FindSource(source)
        source.UpdatePipeline()
        return source.GetDataInformation().GetBounds()

    bounds = []
    if name in ["arrows", "arrows_plus_1"]:
        bounds = find_bounds("High Poly Cylinder")
        bounds = [1.5*b for b in bounds]
    if name == "plus_1":
        bounds = find_bounds("Transform1")

    #ResetCamera(view)
    view.CameraParallelProjection = 1
    view.CameraParallelScale = max([bounds[1] - bounds[0], bounds[3] - bounds[2]])/2
    # view.CameraPosition = [
    #     2*(bounds[0]+bounds[1])/2,
    #     2*(bounds[2]+bounds[3])/2,
    #     view.CameraPosition[2]
    # ]
    # view.CameraFocalPoint = [
    #     2*(bounds[0]+bounds[1])/2,
    #     2*(bounds[2]+bounds[3])/2,
    #     view.CameraFocalPoint[2]
    # ]
    SaveScreenshot("%s%s.png"%(output_prefix, name), view)
