#!/usr/bin/env bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Make demo images
mkdir -p images/demo

# pvpython make_demo_state_images.py \
#     tesselation_demo_state.pvsm \
#     images/demo/tesselation
# 
# pvpython make_demo_state_images.py \
#     projection_demo_holography_state.pvsm \
#     images/demo/projection


# # Uniform cube
# mkdir -p images/modelling/uniform_cube_100
# pushd images/modelling/uniform_cube_100
# ${script_dir}/generate_cube_state/build/generate_cube_state 0.08 sd [100]
# mv output_mult.tec uniform_cube_100nm_100.tec
# popd
# 
# pvpython make_modelling_state_images.py \
#     modelling_state.pvsm \
#     images/modelling/uniform_cube_100/uniform_cube_100nm_100.tec \
#     images/modelling/uniform_cube_100/uniform_cube_100
# 
# mkdir -p images/modelling/uniform_cube_111
# pushd images/modelling/uniform_cube_111
# ${script_dir}/generate_cube_state/build/generate_cube_state 0.08 sd [111]
# mv output_mult.tec uniform_cube_100nm_111.tec
# popd
# 
# pvpython make_modelling_state_images.py \
#     modelling_state.pvsm \
#     images/modelling/uniform_cube_111/uniform_cube_100nm_111.tec \
#     images/modelling/uniform_cube_111/uniform_cube_111
# 
# # # Vortex Cube
# mkdir -p images/modelling/vortex_cube_100
# pushd images/modelling/vortex_cube_100
# ${script_dir}/generate_cube_state/build/generate_cube_state 0.08 vortex [100]
# mv output_mult.tec vortex_cube_100nm_100.tec
# popd
# 
# pvpython make_modelling_state_images.py \
#     modelling_state.pvsm \
#     images/modelling/vortex_cube_100/vortex_cube_100nm_100.tec \
#     images/modelling/vortex_cube_100/vortex_cube_100
# 
# mkdir -p images/modelling/vortex_cube_111
# pushd images/modelling/vortex_cube_111
# ${script_dir}/generate_cube_state/build/generate_cube_state 0.08 vortex [111]
# mv output_mult.tec vortex_cube_100nm_111.tec
# popd
# 
# pvpython make_modelling_state_images.py \
#     modelling_state.pvsm \
#     images/modelling/vortex_cube_111/vortex_cube_100nm_111.tec \
#     images/modelling/vortex_cube_111/vortex_cube_111
# 
# # # Flower Cube
# mkdir -p images/modelling/flower_cube_100
# pushd images/modelling/flower_cube_100
# ${script_dir}/generate_cube_state/build/generate_cube_state 0.1 flower [100]
# mv output_mult.tec flower_cube_100nm_100.tec
# popd
# 
# pvpython make_modelling_state_images.py \
#     modelling_state.pvsm \
#     images/modelling/flower_cube_100/flower_cube_100nm_100.tec \
#     images/modelling/flower_cube_100/flower_cube_100
# 
# # Flower Cube
# mkdir -p images/modelling/flower_cube_111
# pushd images/modelling/flower_cube_111
# ${script_dir}/generate_cube_state/build/generate_cube_state 0.08 flower [111]
# mv output_mult.tec flower_cube_100nm_111.tec
# popd
# 
# pvpython make_modelling_state_images.py \
#     modelling_state.pvsm \
#     images/modelling/flower_cube_111/flower_cube_100nm_111.tec \
#     images/modelling/flower_cube_111/flower_cube_111


mkdir -p images/vortex_arrows/
pvpython make_vortex_arrows.py \
    vortex_arrows_state.pvsm \
    images/vortex_arrows/
