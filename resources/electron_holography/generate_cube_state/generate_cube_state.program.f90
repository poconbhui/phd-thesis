PROGRAM generate_cube_state
    USE MERRILL
    IMPLICIT NONE

    REAL(KIND=DP) :: cube_size

    INTEGER :: state = -1
    INTEGER, PARAMETER :: STATE_SD = 1, STATE_FLOWER = 2, STATE_VORTEX = 3

    REAL(KIND=DP) :: direction(3)

    CHARACTER(len=1024) :: argv
    INTEGER :: i


    IF(COMMAND_ARGUMENT_COUNT() .NE. 3) THEN
        CALL GET_COMMAND_ARGUMENT(1, argv)
        WRITE(*,*) "Usage: ", TRIM(argv), " cube_size state_type direction"
        STOP 1
    END IF

    ! Cube size
    CALL GET_COMMAND_ARGUMENT(1, argv)
    READ(argv,*) cube_size
    WRITE(*,*) "cube_size: ", cube_size

    ! State type
    CALL GET_COMMAND_ARGUMENT(2, argv)
    SELECT CASE(TRIM(argv))
        CASE("sd")
            state = STATE_SD
        CASE("flower")
            state = STATE_FLOWER
        CASE("vortex")
            state = STATE_VORTEX
        CASE DEFAULT
            WRITE(*,*) "Unknown state: ", TRIM(argv)
            WRITE(*,*) "Allowed states: sd , vortex"
            STOP 1
    END SELECT
    WRITE(*,*) "STATE: ", state


    ! Initialize MERRILL
    CALL InitializeMERRILL()

    CALL Magnetite(20.0_DP)

    ! Generate Cube, accurate for 100nm
    CALL GenerateCubeMesh(0.1_DP, LambdaEx*SQRT(Ls)/2)

    ! Rescale cube to requested size
    Ls = Ls / (cube_size/0.1)**2


    ! State direction
    CALL GET_COMMAND_ARGUMENT(3, argv)
    SELECT CASE(TRIM(argv))
        CASE("[100]")
            direction = [1,0,0]
            K1 = ABS(K1)
        CASE("[111]")
            direction = [1,1,1]/SQRT(3.0)
            K1 = -ABS(K1)
        CASE DEFAULT
            WRITE(*,*) "Unknown direction: ", TRIM(argv)
            WRITE(*,*) "Allowed directions: [100] , [111]"
    END SELECT



    SELECT CASE(state)
        CASE(STATE_SD)
            DO i=1,NNODE
                m(i,:) = direction
            END DO

        CASE(STATE_FLOWER)
            DO i=1,NNODE
                m(i,:) = direction
            END DO

            WRITE(*,*) "EnergyMin"
            CALL EnergyMin()

        CASE(STATE_VORTEX)
            DO i=1,NNODE
                m(i,:) = CROSS(direction, VCL(i,1:3))
                IF(NORM2(m(i,:)) .GT. 0) THEN
                    m(i,:) = m(i,:)/NORM2(m(i,:))
                ELSE
                    m(i,:) = 0
                END IF
                m(i,:) = m(i,:) + 0.4*direction
                m(i,:) = m(i,:) / NORM2(m(i,:))
            END DO

            WRITE(*,*) "EnergyMin"
            CALL EnergyMin()
    END SELECT

    CALL WriteTecplot(-1.0_DP, "output", 1)

CONTAINS
    FUNCTION CROSS(a, b)
        REAL(KIND=DP) :: CROSS(3)

        REAL(KIND=DP), INTENT(IN) :: a(3)
        REAL(KIND=DP), INTENT(IN) :: b(3)

        CROSS(1) = a(2)*b(3) - a(3)*b(2)
        CROSS(2) = a(3)*b(1) - a(1)*b(3)
        CROSS(3) = a(1)*b(2) - a(2)*b(1)
    END FUNCTION CROSS
END PROGRAM generate_cube_state
