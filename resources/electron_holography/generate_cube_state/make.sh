#!/usr/bin/bash

set -e

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
build_dir=$script_dir/build
source_dir=$script_dir

mkdir -p $build_dir
cmake \
    -B${build_dir} -H${source_dir} \
    -DMERRILL_DIR:PATH=/home/paddy/prog/merrill/build/install/
make -C ${build_dir}


${build_dir}/generate_cube_state 0.1 vortex [100]
