#!/usr/bin/env pvpython

from sys import argv
print(argv)
import sys
sys.exit

if len(argv) < 3:
    print("Usage: pvpython %s paraview_state output_prefix")
    import sys
    sys.exit(1)

paraview_state = argv[1]
output_prefix  = argv[2]

from paraview.simple import *

servermanager.LoadPlugin("/home/paddy/tmp/install-holomag/holomag-2.0.0-Linux-x86-64-ParaView-5.3.0/lib/holomag/libholomag-pv5.3.so")
servermanager.LoadPlugin("/home/paddy/prog/merrill/demo/paraview-plugins/MERRILL-plugins.xml")
servermanager.LoadState(paraview_state)

views = [
    FindView("Magnetization"), FindView("Projection Mesh"),
    FindView("Projection Magnetization"),
    FindView("Electron Phase Map"), FindView("Electron Hologram"),
    FindView("Density")
]
names = [
    "magnetization", "projection_mesh", "projection_magnetization",
    "electron_phase_map", "electron_hologram", "density"
]


# Fix cube edge width
for view, name in zip(views,names):
    if name in ["magnetization", "projection_mesh"]:
        GetDisplayProperties(FindSource("Cube Edges"), view).LineWidth = 8
    else:
        GetDisplayProperties(FindSource("Cube Outline"), view).LineWidth = 8

    if name in ["electron_phase_map", "electron_hologram"]:
        try:
            GetDisplayProperties(FindSource("Real Region Outline"), view).LineWidth = 8
        except:
            pass

    if name in ["electron_hologram"]:
        GetDisplayProperties(FindSource("Color Wheel"), view).LineWidth = 4

# Fix energy surface colours
e_range = FindSource("Helicity1").PointData.GetArray("Helicity").GetRange()
GetColorTransferFunction("Helicity").RGBPoints[0] = e_range[0]
GetColorTransferFunction("Helicity").RGBPoints[4] = (e_range[0]+e_range[1])/2
GetColorTransferFunction("Helicity").RGBPoints[8] = e_range[1]

# Fix the display sizes
for view in views:
    view.ViewSize = [1000, 1000]

for view, name in zip(views, names):
    view.UseOffscreenRenderingForScreenshots = 1
    view.UseOffscreenRendering = 1

    def find_bounds(source):
        source.UpdatePipeline()
        return source.GetDataInformation().GetBounds()

    bounds = []
    if name in ["electron_phase_map", "electron_hologram"]:
        FindSource("ElectronHolographyContours1").UpdatePipeline()
        bounds = FindSource("ElectronHolographyContours1").GetDataInformation().GetBounds()
    elif name in ["projection_mesh"]:
        try:
            bounds = find_bounds(FindSource("ResampleToImage1"))
        except:
            bounds = find_bounds(FindSource("Low Poly ProjectionElectronPhaseMapFilter"))
        bounds = [1.2*b for b in bounds]
    else:
        bounds = find_bounds(FindSource("Cube Outline"))
        bounds = [1.2*b for b in bounds]

    view.CameraParallelProjection = 1
    view.CameraParallelScale = max([bounds[1] - bounds[0], bounds[3] - bounds[2]])/2
    view.CameraPosition = [
        2*(bounds[0]+bounds[1])/2,
        2*(bounds[2]+bounds[3])/2,
        view.CameraPosition[2]
    ]
    view.CameraFocalPoint = [
        2*(bounds[0]+bounds[1])/2,
        2*(bounds[2]+bounds[3])/2,
        view.CameraFocalPoint[2]
    ]
    SaveScreenshot("%s.%s.png"%(output_prefix, name), view)
