from tensor_tools import *

import itertools

def parity(p):
    return sum(
        1 for (x,px) in enumerate(p)
          for (y,py) in enumerate(p)
          if x<y and px>py
    )%2==0

@apply_until_stable
@distribute_over_terms
def contract_e(expr, verbose = False):
    expr = expand_powers(expr)
    if verbose: print(expr)
    if verbose: print(expr.args)
    if verbose: print(expr.as_ordered_factors())
    e_list = get_tensors_with_base(expr.as_ordered_factors(), [e])
    if verbose: print(e_list)

    tensor_parity = lambda t: parity([i.sort_key() for i in t.indices])
    for ei in e_list:

        if verbose: print()
        if verbose: print(ei)

        # Ensure unique indices
        if any(ei.indices.count(i) > 1 for i in ei.indices):
            expr = expr.subs({ei: 0})
            break

        # Evaluate integer indices
        if all(i.is_Integer for i in ei.indices):
            p1 = tensor_parity(ei)
            ei_s = Indexed(ei.base, *ordered(ei.indices))
            ei_value = 1
            if tensor_parity(ei_s) != tensor_parity(ei):
                ei_value = -1
            expr = expr.subs({ei: ei_value})
            break

        # Evaluate summation over index

        # Get summed e (with no extra tensors being summed over that index)
        # Only works for e.e and e.e.e.e
        target_index = None
        target_tensors = None
        for index in ei.indices:
            if index.is_Integer: continue
            tensors = get_tensors_with_index(expr.as_ordered_factors(), [index])
            if any(t.base != e for t in tensors): continue
            elif len(tensors) > 1:
                target_index = index
                target_tensors = tensors
                break

        if target_tensors != None and len(target_tensors) % 2 == 0:
            # Get parity for moving target index to leftmost position
            p = (-1)**sum(t.indices.index(target_index) for t in target_tensors)
            if verbose: print(p)

            # Get list of indices without target_index
            inds = [
                [i for i in t.indices if i != target_index]
                for t in target_tensors
            ]
            i1s = [i[0] for i in inds]
            i2s = [i[1] for i in inds]

            if verbose: print(inds)
            contexpr = 0
            # Add all combinations of signed index swaps
            # Each odd number of swaps, mul -1
            # Only considered 2 free indices
            for n in range(len(target_tensors)+1):
                # Get list of all combinations of n tensors
                cs = list(itertools.combinations(range(len(target_tensors)), n))
                if verbose: print("n: ", n)
                if verbose: print("cs: ", list(cs))

                # Iterate over each individual combination
                for ci in cs:
                    if verbose: print()
                    if verbose: print("here")
                    if verbose: print("ci: ", ci)
                    this_i1s = []
                    this_i2s = []
                    # If i is in this combination, swap indices
                    for i in range(len(i1s)):
                        if i not in ci:
                            this_i1s.append(i1s[i])
                            this_i2s.append(i2s[i])
                        else:
                            this_i1s.append(i2s[i])
                            this_i2s.append(i1s[i])

                    # Generate delta expression for this swap
                    contexpr = (
                        contexpr
                            + (-1)**len(ci)
                            *Indexed(I, *this_i1s)*Indexed(I, *this_i2s)
                    )
                    if verbose: print("contexpr: ", contexpr)

            expr = expr.subs({Mul(*target_tensors): p*contexpr/2})


    return expr


# If script is called, run tests
if __name__ == '__main__':
    # Generate a function for expr that will set the
    # free indices from its arguments
    # eg
    #   f = e_set(a[i,j,k,l])
    #   f(1,2,3,4) -> a[1,2,3,4]
    def e_set(expr):
        return lambda *args: expr.subs(
            zip(
                ordered(get_indices(expr)[0]),
                args
            )
        )

    # Generate every combination of indices of
    # dimension 'dim' for a rank 'rank' tensor.
    def gen_indices(rank, dim):
        if rank == 0: return tuple()
        if rank == 1: return ((i,) for i in range(dim))
        return ((i,) + j for i in range(dim) for j in gen_indices(rank-1, dim))

    # Test equality of e_cont and e_sum over all combinations of indices of
    # dimension 'dim'
    def e_test(e_cont, e_sum, dim):
        print()
        n_free_idxs = len(get_indices(e_cont)[0])
        indices = list(gen_indices(n_free_idxs, dim))
        print("num tests: ", len(indices))
        for iii, ii in enumerate(indices):
            if iii%100 == 0: print("\r%d"%(iii), end="")
            v_cont = contract_delta(e_set(e_cont)(*ii))
            v_sum  = contract_e(e_set(e_sum)(*ii))

            if v_cont != v_sum:
                print()
                print("%s != %s"%(v_cont, v_sum), "idx: ", ii)
        print()

    #
    # Test e.e
    #
    e_expr = e[i,j,k]*e[k,m,n]
    e_cont = contract_e(e_expr)
    e_sum  = Sum(e_expr, (k,0,2)).doit()

    e_test(e_cont, e_sum, 3)


    #
    # Test e.e.e.e
    #
    e_expr = e[i,j,k]*e[k,m,n]*e[k,p,t]*e[k,v,w]
    e_cont = contract_e(e_expr)
    e_sum  = Sum(e_expr, (k,0,2)).doit()

    e_test(e_cont, e_sum, 3)
