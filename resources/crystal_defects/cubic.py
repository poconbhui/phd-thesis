from __future__ import print_function

from sympy import *

from tensor_tools import *
from e_contraction import *

eps, sig = symbols('eps sig', shape=2*[dim], cls=IndexedBase)
c = symbols('c', shape=2*[dim], cls=IndexedBase)

def sig_cubic(i,j):
    return (
        I[i,j]*c[1,2]*eps[u,u] + I[i,j,k,l]*(c[1,1] - c[1,2] - 2*c[4,4])*eps[k,l]
        + 2*c[4,4]*eps[i,j]
    )

def C_cubic(i,j,k,l):
    return  (
        c[1,2]*I[i,j]*I[k,l] + c[4,4]*(I[i,k]*I[j,l] + I[i,l]*I[j,k])
        + I[i,j,k,l]*(c[1,1] - c[1,2] - 2*c[4,4])
    )

def S_cubic(i,j,k,l):
    return  (
        1/(4*c[4,4])*(I[i,k]*I[j,l] + I[i,l]*I[j,k])
        + (
            - c[1,2]*I[i,j]*I[k,l]
        )/((c[1, 1] - c[1, 2])*(c[1, 1] + 2*c[1, 2]))

        - I[i,j,k,l]*((1/(2*c[4,4]))*(c[1, 1] - c[1, 2]) - 1)/(c[1, 1] - c[1, 2])
    )

@distribute_over_terms
def X_to_Xp(expr):
    X_list = get_tensors_with_base(expr.args, [X])
    for target_X in X_list:
        expr = expr.subs({
            target_X: 2*c[4,4]*(
                Indexed(Xp, *target_X.indices)
                - Xp[v,v]*2*c[1,2]*c[4,4]*Indexed(I, *target_X.indices)/(-(c[1, 1] - c[1,
                  2])*(c[1, 1] + 2*c[1, 2]) + 4*c[1, 2]*c[4, 4])
                #- (
                #    (
                #        -(c[1, 1] - c[1, 2])*(c[1, 1] + 2*c[1, 2])
                #        + 2*(c[1, 1] + c[1, 2])*c[4, 4]
                #    ) / (
                #        -(c[1, 1] - c[1, 2])*(c[1, 1] + 2*c[1, 2])
                #        + 4*(c[1, 1] + c[1, 2])*c[4, 4]
                #    )
                #)*Xp[v,v]*Indexed(I, *target_X.indices)
            )
        })

    return expr

def to_voigt_4(C, mode_S = False):
    
    def closure(i,j):
        ind = [[2,3],[3,1],[1,2]]

        i1 = None
        i2 = None
        factor = 1
        if i < 3:
            i1 = [i,i]
        else:
            i1 = ind[i-3]
            factor *= 2

        if j < 3:
            i2 = [j,j]
        else:
            i2 = ind[j-3]
            factor *= 2

        if mode_S == False: factor = 1
        return factor*C(*i1, *i2)

    return closure

def sig_cubic(i,j):
    return C_cubic(i,j,k,l)*eps[k,l]

@apply_until_stable
@distribute_over_terms
def eval_contraction_indices(expr, indices):
    tensors = get_tensors_with_index(expr.args, indices)

    if len(tensors) == 0: return expr

    for ind in indices:
        expr = Add(
            *(expr.subs({ind: i}) for i in range(ind.lower, ind.upper))
        )

    return expr
    


from functools import reduce
chain  = lambda f, *fs: reduce(lambda x, y: lambda z: y(x(z)), fs, f)
rapply = lambda x, f: f(x)

E = symbols('E')
CI = Matrix([
    [2*mu + lmbda, lmbda, lmbda, 0, 0, 0],
    [lmbda, 2*mu + lmbda, lmbda, 0, 0, 0],
    [lmbda, lmbda, 2*mu + lmbda, 0, 0, 0],
    [0, 0, 0, mu, 0, 0],
    [0, 0, 0, 0, mu, 0],
    [0, 0, 0, 0, 0, mu]
]).subs({lmbda: (2*mu*nu)/(1-2*nu)})
print()
pprint(CI)


print("Using S cubic")
pprint(
    S_cubic(i,j,k,l)
        .expand()
        .collect(
            [I[i,j,k,l], I[i,j]*I[k,l], I[i,k]*I[j,l]+I[i,l]*I[j,k]],
            chain(factor, together)
        )
)

mC  = Matrix(6,6,to_voigt_4(C_cubic))
mC  = mC.applyfunc(contract_delta)
print()
pprint(mC)

mSC = mC.inv()
mSC = mSC.applyfunc(simplify)
mSC = mSC.applyfunc(factor)

mS = Matrix(6,6, to_voigt_4(S_cubic, mode_S = True))
mS = mS.applyfunc(expand)
mS = mS.applyfunc(contract_delta)
mS = mS.applyfunc(simplify)
mS = mS.applyfunc(factor)
pprint(mS)

_ = mS * mC
_ = _.applyfunc(simplify)
print()
pprint(_ == eye(6))



_ = incSincX.subs({S[k,l,m,n]: S_cubic(k,l,m,n)})
_ = rapply(_, chain(
    expand,
    lambda x: contract_delta(x, verbose=False)
))
print()
print("-----")
print("initial delta contraction: ")
pprint(_)

_ = rapply(_, chain(
    contract_e,
    expand
))

print()
print("-----")
print("e contraction:")
pprint(_)

_ = rapply(_, chain(
    lambda x: contract_delta(x, verbose=False),
    expand
))
print()
print("-----")
print("second delta contraction:")
pprint(_)

_ = rapply(_, chain(
    consistent_d2_index
))

print()
print("-----")
print("consistent d terming:")
pprint(_)

_ = rapply(_, chain(
    lambda x: consistent_X_index(x, X)
))

print()
print("-----")
print("consistent X terming:")
pprint(_)

_ = rapply(_, chain(
    expand,
    lambda _: _.subs({w: x}).subs({y: x}).subs({z: x}),
    simplify,
    expand
))

print()
print("-----")
print("consistent terming:")
pprint(_)


_ = rapply(_, chain(
    lambda x: collect_over_tensors(
        x, [X],
        chain(
            lambda y: collect(
                y, [c[4,4]],
                chain(together, cancel, factor)
            ),
            together
        )
    )
))
print()
print()
print("incXincX simplified:")
pprint(_)


#@distribute_over_terms
#def X_to_Xp(expr):
#    X_list = get_tensors_with_base(expr.args, [X])
#    for target_X in X_list:
#        expr = expr.subs({
#            target_X: 2*c[4,4]*(
#                Indexed(Xp, *target_X.indices)
#                - (
#                    (
#                        -(c[1, 1] - c[1, 2])*(c[1, 1] + 2*c[1, 2])
#                        + 2*(c[1, 1] + c[1, 2])*c[4, 4]
#                    ) / (
#                        -(c[1, 1] - c[1, 2])*(c[1, 1] + 2*c[1, 2])
#                        + 4*(c[1, 1] + c[1, 2])*c[4, 4]
#                    )
#                )*Xp[v,v]*Indexed(I, *target_X.indices)
#            )
#        })
#
#    return expr




print()
print()
print("X_to_Xp:")
_ = rapply(_, chain(
    X_to_Xp,
    expand,
    contract_delta,
    consistent_d2_index,
    lambda x: consistent_X_index(x, Xp, trace_index = v),
    cancel,
    simplify,
    expand,
    lambda x: set_d_X_0(x, Xp),
    lambda x: collect_over_tensors(
        x, [Xp],
        chain(
            lambda y: collect(
                y, [c[4,4]],
                chain(together, cancel, factor)
            ),
            together
        )
    )
))
pprint(_)
print()
print(_)
