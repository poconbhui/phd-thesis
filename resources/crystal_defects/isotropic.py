from __future__ import print_function
from tensor_tools import *
from e_contraction import *

#
# Look at incSincX with S = isotropic S
#

print()
print()
print("- Using Compliance Tensor:")
pprint(genS_isotropic(i,j,k,l))

# Do d[i]X[i,j] = 0
print()
print()
print("--")
print("-- Running d[i]X[i,j] == 0")
print("--")
_ = incSincX.subs({S[k,l,m,n]: genS_isotropic(k,l,m,n)})
_ = contract_delta(_)
_ = contract_e(_)
_ = contract_delta(_)

print()
print()
_ = consistent_X_index(_, X)
_ = consistent_d2_index(_)
_ = _.expand()
_ = collect_over_tensors(_, [X], lambda x: together(cancel(x)))


#
# incSincX simplified before setting or substitution
#
test_expr = Add(
    1/(2*mu) * d2**2 * X[i,j]
    - nu/(2*mu*(nu+1)) * d2**2 * I[i,j] * X[u,u]
    + nu/(2*mu*(nu+1)) * d2 * d[i]*d[j] * X[u,u]
    - 1/(2*mu) * d2 * d[i] * d[s] * X[j,s]
    - 1/(2*mu) * d2 * d[j] * d[s] * X[i,s]
    + nu/(2*mu*(nu+1)) * d2 * I[i,j] * d[s] * d[t] * X[s,t]
    + 1/(2*mu*(nu+1)) * d[i] * d[j] * d[s] * d[t] * X[s,t]
)

print()
pprint(_)
pprint((_ - test_expr).simplify() == 0)

print()
print()
print("- Setting d[i]X[i,j] = 0")

_ = set_d_X_0(_, X)
_ = consistent_d2_index(_)
_ = consistent_X_index(_, X)
_ = _.expand()
_ = collect_over_tensors(_, [X], lambda x: together(cancel(x)))

#
# incSincX after setting X
#
test_expr = Add(
    1/(2*mu)* d2**2 * X[i,j],
    - nu/(2*mu*(nu+1)) * d2**2 * I[i,j] * X[u,u]
    + nu/(2*mu*(nu+1)) * d2 * d[i]*d[j] * X[u,u],
    evaluate = False
)

print()
pprint(_)
pprint((test_expr - _).simplify() == 0)

_ = X_to_Xp(_)
_ = contract_delta(_)
_ = consistent_X_index(_, Xp)
_ = _.simplify()
_ = _.expand()
_ = collect_over_tensors(
    _, [Xp],
    lambda x: collect(together(cancel(x)), [d[i]*d[j]])
)

#
# incSincX after substitution
#
test_expr = Add(
   d2**2 * Xp[i,j],
   + nu**2/(nu**2 - 1) * d2**2 * I[i,j] * Xp[u,u]
   - nu*(2*nu + 1)/(nu**2 - 1) * d2 * d[i]*d[j] * Xp[u,u],
   evaluate = False
)

print()
pprint(_)
pprint((test_expr - _).simplify() == 0)



# Do d[i]Xp[i,j] = 0
print()
print()
print("--")
print("-- Running d[i]Xp[i,j] == 0")
print("--")
_ = incSincX.subs({S[k,l,m,n]: genS_isotropic(k,l,m,n)})
_ = contract_delta(_)
_ = contract_e(_)
_ = contract_delta(_)
_ = X_to_Xp(_)
_ = contract_delta(_)
_ = consistent_d2_index(_)
_ = consistent_X_index(_, Xp)
_ = consistent_X_index(_, I)
_ = _.simplify()
_ = _.expand()
_ = collect_over_tensors(
    _, [Xp],
    lambda x: collect(together(cancel(x)), [d[i]*d[j]])
)


#
# incSincX after substitution
#
test_expr = (
    d2**2*Xp[i, j]
    - d2*d[i]*d[s]*Xp[j, s]
    - d2*d[j]*d[s]*Xp[i, s]
    + nu/(nu+1) * d2*d[s]*d[t]*I[i, j]*Xp[s, t]
    + 1/(nu+1) * d[i]*d[j]*d[s]*d[t]*Xp[s, t]
)
pprint(_)
pprint((_ - test_expr).simplify() == 0)

print()
print()
print("- Setting d[i]X[i,j] = 0")

_ = set_d_X_0(_, Xp)
_ = consistent_d2_index(_)
_ = consistent_X_index(_, Xp)
_ = consistent_X_index(_, I)
_ = _.simplify()

#
# incSincX after substitution and after setting
#
test_expr = d2**2 * Xp[i,j]

print()
pprint(_)
pprint((test_expr - _).simplify() == 0)
