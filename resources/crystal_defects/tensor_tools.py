from sympy import *

# Declare dimension
dim = 3

# Declare indices
i,j,k,l,m,n,o,p = symbols('i j k l m n o p', range=(0, dim), cls=Idx)
indices = [i,j,k,l,m,n,o,p]

# Temporary indices
x,y,z,w,v,u,t,s = symbols('x y z w v u t s', range=(0, dim), cls=Idx)

# Declare tensors
d2   = symbols('d2')
d    = symbols('d',     shape=1*[dim], cls=IndexedBase)
I    = symbols('I',     cls=IndexedBase)
X,Xp = symbols('X X\'', shape=2*[dim], cls=IndexedBase)
e    = symbols('e',     shape=3*[dim], cls=IndexedBase)
S    = symbols('S',     shape=4*[dim], cls=IndexedBase)

# Incompatibility expression generator
def genInc(i,j,k,l, t1,t2):
    return e[i,t1,k]*e[j,t2,l]*d[t1]*d[t2]

incSincX = genInc(i,j,k,l, x,y) * S[k,l,m,n] * genInc(m,n,o,p, z,w) * X[o,p]


# Elastic constants
lmbda, nu, mu = var("lambda nu mu")

# Compliance tensor for isotropic medium
def genS_isotropic(i,j,k,l):
    S_iso = (
        - lmbda/(2*mu*(3*lmbda + 2*mu))*I[i,j]*I[k,l]
        + 1/(4*mu)*(I[i,k]*I[j,l] + I[i,l]*I[j,k])
    )
    S_iso = S_iso.subs({lmbda:(2*mu*nu)/(1-2*nu)})
    S_iso = simplify(S_iso)
    return S_iso


#
# Some decorators to avoid boiler plate
#

# Expand braces, for each term in Add, run f.
def distribute_over_terms(f):
    def closure(expr, *args, **kwargs):
        expr = expr.expand()
        if expr.func == Add:
            expr = expr.func(
                *map(lambda x: closure(x, *args, **kwargs), expr.args)
            )
        else:
            expr = f(expr, *args, **kwargs)
        return expr

    return closure

# Run f until expr == f(expr)
def apply_until_stable(f):
    def closure(expr, *args, **kwargs):
        old_expr = None
        while old_expr != expr:
            old_expr = expr
            expr = f(expr, *args, **kwargs)
        return expr

    return closure



# Extract tensors in elem_list with at least one index in indices
def get_tensors_with_index(elem_list, indices):
    matched_tensors = []
    for elem in elem_list:
        if elem.func != Indexed: continue

        for ind in elem.indices:
            if ind in indices:
                matched_tensors.append(elem)
                break

    return matched_tensors


# Extract tensors in elem_list with a base in bases
def get_tensors_with_base(elem_list, bases):
    matched_tensors = []
    for elem in elem_list:
        if elem.func == Indexed and elem.base in bases:
            matched_tensors.append(elem)

    return matched_tensors


# Simplification, collect terms for each unique index of X
def collect_over_tensors(expr, bases, *collect_args, **collect_kwargs):
    expr = expr.expand()

    terms = []
    if expr.func == Add:
        terms = expr.args
    else:
        terms = [expr]

    tensors = set()
    for term in terms:
        tensors.update(get_tensors_with_base(term.args, bases))
            
    expr = expr.collect(tensors, *collect_args, **collect_kwargs)

    return expr


# Expand
#   Pow(x,i) -> Mul(x,x,...)
#   Mul(...,Pow(x,i),...) -> Mul(...,x,x,x,...)
@apply_until_stable
@distribute_over_terms
def expand_powers(expr):
    if expr.func == Pow and expr.args[1].is_Integer and expr.args[1] > 0:
        expr = Mul(*(expr.args[1]*[expr.args[0]]), evaluate=False)

    if expr.func == Mul:
        new_args = []
        for arg in expr.args:
            if arg.func == Pow:
                new_arg = expand_powers(arg)
                if new_arg.func == Mul:
                    new_args.extend(new_arg.args)
                else:
                    new_args.append(new_arg)
            else:
                new_args.append(arg)
        expr = Mul(*new_args, evaluate=False)

    return expr


# Apply delta function
# Transform all functions a[i]I[i,l] + b[m]I[n,m] -> a[l] + b[n]
# This is a bit of a beast to accommodate multiple summed indices
#
@apply_until_stable
@distribute_over_terms
def contract_delta(expr, verbose = False):
    expr = expand_powers(expr)

    #verbose = False
    if verbose: print()
    if verbose: print()
    if verbose: print("construct_delta")
    if verbose: print("before: ", expr)

    factors = None
    if expr.func == Mul:
        factors = expr.args
    else:
        factors = [expr]

    # Get a list of the deltas in the term
    delta_list = get_tensors_with_base(factors, [I])
    if verbose: print(delta_list)

    # Iterate over deltas looking for one with a summed index
    target_delta  = None
    target_tensor = None
    for delta in delta_list:

        # indices that *can* be contracted
        contractable_indices = list(set(
            i for i in delta.indices if not i.is_Integer
        ))

        # other tensors that are being contracted with this delta
        contracting_tensors = get_tensors_with_index(
            factors, contractable_indices
        )
        if len(contracting_tensors) > 0: contracting_tensors.remove(delta)

        # indices on this delta that are being contracted with other tensors
        contracting_indices = list(set(
            i for i in contractable_indices
            if len(get_tensors_with_index(contracting_tensors, [i])) > 0
        ))

        if verbose: print("at 1")


        # Combine deltas being contracted together
        # I[i,j,k]*I[k,l,m,n] -> I[i,j,k,k,l,m,n]
        contracting_deltas = get_tensors_with_base(contracting_tensors, [I])
        if len(contracting_deltas) > 0:
            indices = delta.indices
            for cdelta in contracting_deltas:
                indices = indices + cdelta.indices

            expr = expr.subs({
                Mul(delta, *contracting_deltas, evaluate=False):
                Indexed(delta.base, *indices)
            })
            if verbose: print("1: after: ", expr)

            break

        if verbose: print("at 2")

        # Unify contraction indices
        # I[i,j,k,k,l,m]a[l]b[m] -> I[i,j,k,k,k,k]a[k]b[m]
        if len(contracting_indices) > 1:
            common_ind = contracting_indices[0]
            for ind in contracting_indices:
                expr = expr.subs({ind: common_ind})

            if verbose: print("2: after: ", expr)
            break

        if verbose: print("at 3")

        # Remove delta if all indices are contracting indices
        if len(set(contracting_indices)) == len(set(delta.indices)):
            expr = expr.subs({delta: 1})
            if verbose: print("3: after: ", expr)
            break

        if verbose: print("at 4")

        # Remove duplicate contraction indices
        if len(contracting_indices) > 0 \
            and delta.indices.count(contracting_indices[0]) > 1:

            if verbose: print("here 1")
            new_indices = []
            for index in delta.indices:
                if index not in contracting_indices:
                    new_indices.append(index)
            new_indices.append(index)

            expr = expr.subs({delta: Indexed(delta.base, *new_indices)})
            if verbose: print("4: after: ", expr)
            break

        if verbose: print("at 5")

        # Order indices
        if any(i != j for i,j in zip(delta.indices, ordered(delta.indices))):
            expr = expr.subs(
                {delta: Indexed(delta.base, *ordered(delta.indices))}
            )
            if verbose: print("4: after: ", expr)
            break

        if verbose: print("at 6")

        # Evaluate delta trace or concrete value
        # I[i,i] -> dim(i)
        # I[1,1] -> 1
        # I[1,2] -> 0
        if delta.indices.count(delta.indices[0]) == len(delta.indices):
            if delta.indices[0].is_Integer:
                # Delta has numerical value inserted. Evaluate value.
                expr = expr.subs({delta: 1})
            else:
                # Delta has symbolic contraction. Evaluate contraction.
                expr = expr.subs({delta: dim})
            if verbose: print("5: after: ", expr)
            break
        else:
            # If 2 or more differing, concrete indices are set, check equality
            int_indices = [ind for ind in delta.indices if ind.is_Integer]
            if len(int_indices) > 0 \
                and int_indices.count(int_indices[0]) != len(int_indices):

                # Has concrete, but different values
                expr = expr.subs({delta: 0})
                if verbose: print("6: after: ", expr)
                break

        if verbose: print("at 7")


        # Apply delta contraction with free index to other tensor
        # for rank(I) == 2
        # if delta is contracting only one other tensor
        # I[i,j]a[j] -> a[j]
        if len(delta.indices) == 2 and len(contracting_indices) == 1:

            # Find non-contracting indices
            non_contracting_indices = [
                i for i in delta.indices if i not in contracting_indices
            ]

            if len(non_contracting_indices) == 0: continue

            free_index = non_contracting_indices[0]
            cont_index = contracting_indices[0]

            # Replace contracting index with free index only if
            # contraction is over one other tensor
            if len(get_tensors_with_index(expr.args, [cont_index])) != 2:
                continue


            # Replace delta and indices
            expr = expr.subs({delta: 1}).subs({cont_index: free_index})

            if verbose: print("7: after: ", expr)
            break


    if verbose: print("after: ", expr)
    return expr



## Apply contraction of Levi-Civita tensors according to
##   e[i,j,k] e[i,m,n] = I[j,m] I[k,n] - I[j,n] I[k,m]
#@apply_until_stable
#@distribute_over_terms
#def contract_e(expr):
#    # Get list of e[i,j,k] in term
#    e_list = get_tensors_with_base(expr.args, [e])
#
#    # Find one contracting pair
#    e1 = None
#    e2 = None
#    target_index = None
#    for ei in e_list:
#        contracting = get_tensors_with_index(e_list, ei.indices)
#        if len(contracting) > 1:
#            e1, e2 = contracting[0:2]
#            break
#
#    # If we have a contracting pair
#    if e1 != None:
#        # Find a contraction index
#        target_index = None
#        for i1 in e1.indices:
#            for i2 in e2.indices:
#                if i1 == i2:
#                    target_index = i1
#
#        # Find the signature of the permutations of the indices
#        # to the lhs of e
#        signature = (-1)**(
#            e1.indices.index(target_index) + e2.indices.index(target_index)
#        )
#
#        # Find the indices without the target index
#        j,k = [i for i in e1.indices if i != target_index]
#        m,n = [i for i in e2.indices if i != target_index]
#
#        # Build the delta expression
#        delta_expr = I[j,m]*I[k,n] - I[j,n]*I[k,m]
#
#        # Substitute e*e for delta_expr
#        expr = expr.subs({e1*e2: delta_expr})
#
#    return expr


@apply_until_stable
@distribute_over_terms
def set_d_X_0(expr, X):
    X_list = get_tensors_with_base(expr.args, [X])
    
    for target_X in X_list:
        for ind in target_X.indices:
            #print()
            #print(expr)
            tensors = get_tensors_with_index(expr.args, [ind])
            #print(tensors)
            tensors.remove(target_X)

            if len(tensors) == 1 and tensors[0].base == d:
                return expr.subs({tensors[0]*target_X: 0})

    return expr

@apply_until_stable
@distribute_over_terms
def consistent_d2_index(expr):
    expr = expand_powers(expr)
    d_list = get_tensors_with_base(expr.args, [d])

    for di in d_list:
        idx = di.indices[0]
        tensors = get_tensors_with_index(expr.args, [idx])
        if len(tensors) > 1 and all(t.base == d for t in tensors):
            expr = expr.subs({
                Mul(*tensors, evaluate=False): var('d%d'%(len(tensors)))
            })
        break


    return expr

@apply_until_stable
@distribute_over_terms
def consistent_X_index(expr, X, trace_index = u):
    expr = expand_powers(expr)
    #print()
    #print(expr)
    contraction_indices = [u,s,t]

    target_X = None
    for arg in expr.args:
        if arg.func == Indexed and arg.base == X:
            target_X = arg

    if target_X == None: return expr

    # Prefer indices in alphabetical order
    if any(i != j for i,j in zip(target_X.indices, ordered(target_X.indices))):
        new_X = Indexed(X, *ordered(target_X.indices))
        return expr.subs({target_X: new_X})

    # Tracing should be done with a private variable
    if target_X.indices[0] == target_X.indices[1] \
        and target_X.indices[0] != trace_index \
        and len(get_tensors_with_index(expr.args, [target_X.indices[0]])) == 1:

        return expr.subs({target_X.indices[0]: trace_index})

    # Set contraction indices to s and t.
    target_tensors = get_tensors_with_index(expr.args, target_X.indices)
    target_tensors.remove(target_X)

    new_index = s
    if new_index in target_X.indices:
        new_index = t

    for ind in target_X.indices:
        if ind in contraction_indices: continue

        for tensor in target_tensors:
            if ind in tensor.indices:
                return expr.subs({ind: new_index})

    return expr


@distribute_over_terms
def X_to_Xp(expr):
    X_list = get_tensors_with_base(expr.args, [X])
    for target_X in X_list:
        expr = expr.subs({
            target_X: 2*mu*(
                Indexed(Xp, *target_X.indices)
                + nu/(1-nu)*Xp[v,v]*Indexed(I, *target_X.indices)
            )
        })

    return expr
