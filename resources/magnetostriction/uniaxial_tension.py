from sympy import *

var('i:l')
var('C_11 C_12 C_44 T B_1 B_2')
alpha, gamma = symbols('alpha gamma', cls=IndexedBase)

B0 = Matrix([
    [B_1 * alpha[0]*alpha[0], B_2*alpha[0]*alpha[1], B_2*alpha[0]*alpha[2]],
    [B_2 * alpha[1]*alpha[0], B_1*alpha[1]*alpha[1], B_2*alpha[1]*alpha[2]],
    [B_2 * alpha[2]*alpha[0], B_2*alpha[2]*alpha[1], B_1*alpha[2]*alpha[2]],
])

P0 = Matrix([
    [T * gamma[0]*gamma[0], T*gamma[0]*gamma[1], T*gamma[0]*gamma[2]],
    [T * gamma[1]*gamma[0], T*gamma[1]*gamma[1], T*gamma[1]*gamma[2]],
    [T * gamma[2]*gamma[0], T*gamma[2]*gamma[1], T*gamma[2]*gamma[2]],
])

def gen_e(i, j):
    if i == j:
        return (
            (
                (C_11 + 2*C_12)*(P0[i,j] + B0[i,j])
                - C_12*(T - B_1)
            ) / (
                (C_11 - C_12)*(C_11 + 2*C_12)
            )
        )
    else:
        return (
            (P0[i,j] - B0[i,j]) / (2*C_44)
        )

C = Matrix([
    [C_11, C_12, C_12, 0, 0, 0],
    [C_12, C_11, C_12, 0, 0, 0],
    [C_12, C_12, C_11, 0, 0, 0],
    [0, 0, 0, C_44, 0, 0],
    [0, 0, 0, 0, C_44, 0],
    [0, 0, 0, 0, 0, C_44],
])
pprint(gen_e(0,1))
