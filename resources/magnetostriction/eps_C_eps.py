from sympy import *

var('exx eyy ezz exy eyx eyz ezy ezx exz')
var('C11 C12 C44')
eps = Matrix([exx, eyy, ezz, exy + eyx, eyz + ezy, ezx + exz])
C   = Matrix([
    [C11, C12, C12, 0, 0, 0],
    [C12, C11, C12, 0, 0, 0],
    [C12, C12, C11, 0, 0, 0],
    [0, 0, 0, C44, 0, 0],
    [0, 0, 0, 0, C44, 0],
    [0, 0, 0, 0, 0, C44]
])

_ = 1/Symbol('2') * eps.T * (C * eps)
_ = _[0]
_ = expand(_)
_ = collect(_, [C11, C12, C44])

pprint(_)

_ = C.inv()
_ = _.applyfunc(simplify)
_ = _.applyfunc(factor)
S = _
pprint(_)
print(_[0,0])
print(_[0,1])
print(_[3,3])

#_ = S*C
#_ = simplify(_)
#pprint(_)
