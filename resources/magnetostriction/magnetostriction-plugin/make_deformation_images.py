#!/usr/bin/env pvpython

from paraview.simple import *

servermanager.LoadState("deformation_state.pvsm")

cube_view = FindView("Magnetization")
flower_view = FindView("Deformation Pressure-Free")
vortex_view = FindView("Deformation With Pressure")

views = [
    FindView("Magnetization"),
    FindView("Deformation Pressure-Free"),
    FindView("Deformation With Pressure")
]
names = [
    "magnetization_view",
    "deformation_pressure_free_view",
    "deformation_with_pressure_view"
]

# Fix cube edge width
for view in views:
    GetDisplayProperties(FindSource("FeatureEdges1"), view).LineWidth = 3

# Fix the display sizes
for view in views:
    view.ViewSize = [1000, 1000]

# Rezoom to the biggest object
ResetCamera(views[2])

for view, name in zip(views, names):
    view.UseOffscreenRenderingForScreenshots = 1
    view.UseOffscreenRendering = 1
    SaveScreenshot("data/%s.png"%(name), view)
