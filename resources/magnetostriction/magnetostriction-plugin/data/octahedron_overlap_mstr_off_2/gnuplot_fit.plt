#!/usr/bin/env gnuplot

set term png

fsf(x) = b_fs*x + c_fs
fit fsf(x) 'fs_e_vs_l.dat' using 1:2 via b_fs, c_fs

esv(x) = a_esv*x**2 + b_esv*x + c_esv
fit esv(x) 'esv_e_vs_l.dat' using 1:2 via a_esv, b_esv, c_esv

a_hsv = a_esv
b_hsv = b_esv
c_hsv = c_esv
hsv(x) = a_hsv*x**2 + b_hsv*x + c_hsv
fit hsv(x) 'hsv_e_vs_l.dat' using 1:2 via a_hsv, b_hsv, c_hsv



set xrange [0.1:0.26]
set yrange [18:50]
set output "extrapolated_overlap_energies.png"
plot \
    'fs_e_vs_l.dat' u 1:2 title "FS", fsf(x) title "Extrapolated FS", \
    'esv_e_vs_l.dat' u 1:2 title "ESV", esv(x) title "Extrapolated ESV", \
    'hsv_e_vs_l.dat' u 1:2 title "HSV", hsv(x) title "Extrapolated HSV"

#set xrange [0.2:0.35]
#set output "further_extrapolated_overlap_energies.png"
#replot
