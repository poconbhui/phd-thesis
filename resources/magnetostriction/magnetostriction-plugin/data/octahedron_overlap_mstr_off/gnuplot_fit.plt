#!/usr/bin/env gnuplot

set term png

fss(x) = a_fs*x**2 + b_fs*x + c_fs
fit fss(x) 'fs_e_vs_l.dat' using 1:2 via a_fs, b_fs, c_fs

isv(x) = a_isv*x**2 + b_isv*x + c_isv
fit isv(x) 'isv_e_vs_l.dat' using 1:2 via a_isv, b_isv, c_isv

hsv(x) = a_hsv*x**2 + b_hsv*x + c_hsv
fit hsv(x) 'hsv_e_vs_l.dat' using 1:2 via a_hsv, b_hsv, c_hsv

esv(x) = a_esv*x**2 + b_esv*x + c_esv
fit esv(x) 'esv_e_vs_l.dat' using 1:2 via a_esv, b_esv, c_esv


set xrange [0.14:*]
set output "extrapolated_overlap_energies.png"
plot \
    'fs_e_vs_l.dat' u 1:2 title "FS",   fss(x) title "Extrapolated FS", \
    'isv_e_vs_l.dat' u 1:2 title "ISV", isv(x) title "Extrapolated ISV", \
    'esv_e_vs_l.dat' u 1:2 title "ESV", esv(x) title "Extrapolated ESV", \
    'hsv_e_vs_l.dat' u 1:2 title "HSV", hsv(x) title "Extrapolated HSV"

set xrange [0.2:0.35]
set output "further_extrapolated_overlap_energies.png"
replot
