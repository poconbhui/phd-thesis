#!/usr/bin/env bash

set -e

source_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
build_dir=$source_dir/build/build
install_dir=$source_dir/build/install
data_dir=$source_dir/data

merrill_dir=$HOME/prog/merrill/build/install/

FC=$(which gfortran)

cmake \
    -B"${build_dir}/deps" \
    -H"${source_dir}/deps" \
    -DCMAKE_INSTALL_PREFIX:PATH="${install_dir}" \
    -DBUILD_SHARED_LIBS:BOOL=ON
make -C "${build_dir}/deps"

cmake \
    -B"${build_dir}" \
    -H"${source_dir}" \
    -DCMAKE_Fortran_COMPILER=$(which gfortran) \
    -DMERRILL_DIR:PATH=${merrill_dir} \
    -DDOLFIN_DIR:PATH="${build_dir}/deps/dolfin/share/dolfin/cmake/" \
    -DFFC_DIR:PATH="${build_dir}/deps/ffc/" \
    -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -DCMAKE_Fortran_FLAGS:STRING="-std=f2008ts -Wall -Wextra -pedantic -Wsurprising -Wno-unused-parameter -Wno-unused-dummy-argument -fmax-errors=2"
    #-DCMAKE_Fortran_FLAGS:STRING="-std=f2008ts -Wall -Wextra -pedantic -Wsurprising -Wno-unused-parameter -Wno-unused-dummy-argument -fmax-errors=2 -ffpe-trap=invalid,overflow,underflow,denormal,zero"

export PYTHONPATH=$PYTHONPATH:${build_dir}/deps/ufl/lib/python
export PYTHONPATH=$PYTHONPATH:${build_dir}/deps/ffc/lib/python
export PYTHONPATH=$PYTHONPATH:${build_dir}/deps/fiat/lib/python
export PYTHONPATH=$PYTHONPATH:${build_dir}/deps/instant/lib/python
make -C "${build_dir}"
make -C "${build_dir}" test ARGS=-V
exit


mkdir -p ${data_dir}
cd ${data_dir}
#${build_dir}/regular_formulation_example

export PV_PLUGIN_PATH=/home/paddy/prog/merrill/demo/paraview-plugins
cd ${source_dir}
#pvpython make_deformation_images.py

cd ${data_dir}
#${build_dir}/sd_fs_sv_states

cd ${source_dir}
#pvpython make_sd_fs_hsv_esv_deformation_images.py

#mkdir -p ${data_dir}/cube_overlap_mstr_off
#cd ${data_dir}/cube_overlap_mstr_off
##module load meshrrill
##meshrrill cube 0.2 0.2 0.2 -c 0.010 -o mesh.neu
#time ${build_dir}/critical_grain_sizes off

#mkdir -p ${data_dir}/cube_overlap_mstr_on
#cd ${data_dir}/cube_overlap_mstr_on
#module load meshrrill
#meshrrill cube 0.25 0.25 0.25 -c 0.010 -o mesh.neu
#time ${build_dir}/critical_grain_sizes on

#mkdir -p ${data_dir}/sphere_overlap_mstr_off
#cd ${data_dir}/sphere_overlap_mstr_off
#module load meshrrill
#meshrrill ellipsoid 0.13 0.13 0.13 -c 0.010 -o mesh.neu
#time ${build_dir}/critical_grain_sizes off

#mkdir -p ${data_dir}/sphere_overlap_mstr_on
#cd ${data_dir}/sphere_overlap_mstr_on
#module load meshrrill
##meshrrill ellipsoid 0.13 0.13 0.13 -c 0.030 -o mesh.neu
#meshrrill ellipsoid 0.13 0.13 0.13 -c 0.02 -o mesh.neu
#time ${build_dir}/critical_grain_sizes on

#mkdir -p ${data_dir}/octahedron_overlap_mstr_off
#cd ${data_dir}/octahedron_overlap_mstr_off
#module load meshrrill
#meshrrill octahedron 0.35 0.35 0.35 -t 0.2 -c 0.010 -o mesh.neu
#time ${build_dir}/critical_grain_sizes

mkdir -p ${data_dir}/octahedron_overlap_mstr_off_2
cd ${data_dir}/octahedron_overlap_mstr_off_2
module load meshrrill
meshrrill octahedron 0.44 0.44 0.44 -t 0.2 -c 0.020 -o mesh.neu
time ${build_dir}/critical_grain_sizes off


#mkdir -p ${data_dir}/octahedron_overlap_mstr_on
#cd ${data_dir}/octahedron_overlap_mstr_on
#module load meshrrill
#meshrrill octahedron 0.39 0.39 0.39 -t 0.2 -c 0.020 -o mesh.neu
#time ${build_dir}/critical_grain_sizes on
