PROGRAM Critical_Grain_Sizes

    USE Merrill
    USE Magnetostriction
    IMPLICIT NONE

    !LOGICAL :: finding_magnetostriction_overlap = .TRUE.
    !LOGICAL :: finding_magnetostriction_overlap = .FALSE.
    LOGICAL :: finding_magnetostriction_overlap

    REAL(KIND=DP) :: esvd_width

    REAL(KIND=DP) :: flower_width
    REAL(KIND=DP) :: hard_vortex_width
    REAL(KIND=DP) :: easy_vortex_width
    REAL(KIND=DP) :: intermediate_vortex_width

    REAL(KIND=DP), ALLOCATABLE :: m_sd(:,:)
    REAL(KIND=DP), ALLOCATABLE :: m_flower(:,:)
    REAL(KIND=DP), ALLOCATABLE :: m_hard_vortex(:,:)
    REAL(KIND=DP), ALLOCATABLE :: m_easy_vortex(:,:)
    REAL(KIND=DP), ALLOCATABLE :: m_intermediate_vortex(:,:)

    REAL(KIND=DP) :: e_sd
    REAL(KIND=DP) :: e_flower
    REAL(KIND=DP) :: e_hard_vortex
    REAL(KIND=DP) :: e_easy_vortex
    REAL(KIND=DP) :: e_intermediate_vortex

    REAL(KIND=DP) :: fs_hsv_overlap_width
    REAL(KIND=DP) :: fs_hsv_overlap_energy

    REAL(KIND=DP) :: hsv_esv_overlap_width
    REAL(KIND=DP) :: hsv_esv_overlap_energy

    REAL(KIND=DP) :: easy_axis(3) = [1, 0, 0]
    REAL(KIND=DP) :: hard_axis(3) = [1/SQRT(3.0), 1/SQRT(3.0), 1/SQRT(3.0)]
    REAL(KIND=DP) :: intermediate_axis(3) = [1/SQRT(2.0), 1/SQRT(2.0), 0.0]

    REAL(KIND=DP) :: Ls_0

    INTERFACE
        LOGICAL FUNCTION is_vortex(m)
            IMPORT DP
            REAL(KIND=DP), INTENT(IN) :: m(:,:)
        END FUNCTION is_vortex
    END INTERFACE

    PROCEDURE(is_vortex), POINTER :: is_easy_vortex => is_100_vortex
    PROCEDURE(is_vortex), POINTER :: is_hard_vortex => is_111_vortex
    PROCEDURE(is_vortex), POINTER :: is_intermediate_vortex => is_110_vortex

    INTEGER :: i


    IF(COMMAND_ARGUMENT_COUNT() .NE. 1) THEN
    BLOCK
        CHARACTER(len=1024) :: program_name
        CALL GET_COMMAND_ARGUMENT(0, program_name)
        WRITE(*,*) "Usage: ", TRIM(program_name), " FULL_MAGNETOSTRICTION(on|off)"
        STOP 1
    END BLOCK
    ELSE
    BLOCK
        CHARACTER(len=1024) :: argv
        CALL GET_COMMAND_ARGUMENT(1, argv)

        SELECT CASE(TRIM(argv))
            CASE("on")
                finding_magnetostriction_overlap = .TRUE.
            CASE("off")
                finding_magnetostriction_overlap = .FALSE.
            CASE DEFAULT
                WRITE(*,*) "Unknown option ", TRIM(argv)
                WRITE(*,*) "Allowed options: on | off"
                STOP 1
        END SELECT
    END BLOCK
    END IF



    CALL InitializeMerrill()
    CALL InitializeCommandParser()
    CALL InitializeVariableSetter()
    CALL InitializeMagnetostriction()

    CALL SetMaterialParameters(magnetostriction = .FALSE.)

    esvd_width = 0.20
    Ls_0 = Ls
    !CALL GenerateCubeMesh(esvd_width/(2*(3.0/4.0/pi)**(1/3.0)), LambdaEx*SQRT(Ls))
    !CALL GenerateSphereMesh(esvd_width, LambdaEx*SQRT(Ls))
    CALL ReadMeshPat("mesh.neu")
    CALL ReportEnergy()

    WRITE(*,"(A,F8.5)") "esvd_width:", esvd_width
    esvd_width = (total_volume)**(1/3.0)*2*(3.0/4.0/pi)**(1/3.0)

    WRITE(*,"(A,F8.5)") "esvd_width:", esvd_width
    !STOP 1


    !
    ! Nucleate states
    !
    CALL SetMaterialParameters(magnetostriction = .FALSE.)

    ! SD State
    m = 0
    m(:,1) = 1
    ALLOCATE(m_sd, SOURCE=m)

    CALL CalculateEnergy(m, e_sd)

    CALL WriteTecplot(1.0_DP, "m_SD", 1)
    m = umstr*SQRT(Ls)
    CALL WriteTecplot(1.0_DP, "u_SD", 1)


    ! No Magnetostriction
    flower_width      = 7.85
    hard_vortex_width = 7.85
    easy_vortex_width = 7.85
    !FS/HSV overlap ~ 7.6
    !FS/ESV overlap ~ 7.85

    ! With magnetostriction
    flower_width      = 10
    hard_vortex_width = 10
    easy_vortex_width = 10
    !FS/HSV overlap ~ 7.6
    !FS/ESV overlap ~ 7.85
    !FS > ESV upper bound at 10

    ! Minima/maxima of state sizes
    !flower_width = 6
    !hard_vortex_width = 8
    !easy_vortex_width = 12
    ! esvd
    flower_width = 0.10
    hard_vortex_width = 0.16
    easy_vortex_width = 0.16
    intermediate_vortex_width = esvd_width

    ! Nucleate a flower state

    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Nucleating Flower"
    WRITE(*,*) "--"
    WRITE(*,*)

    m = 0
    m(:,1) = 1
    Ls = ((esvd_width/(0.5*flower_width))*SQRT(Ls_0))**2
    magnetostriction_dirty = .TRUE.

    CALL randomchange("magnetization", "20")
    CALL SetMaterialParameters(magnetostriction = .FALSE.)
    CALL EnergyMin()

    Ls = ((esvd_width/(flower_width))*SQRT(Ls_0))**2
    CALL SetMaterialParameters(magnetostriction = .FALSE.)
    CALL EnergyMin()

    CALL SetMaterialParameters(magnetostriction = finding_magnetostriction_overlap)
    magnetostriction_dirty = .TRUE.
    CALL EnergyMin()

    ALLOCATE(m_flower, SOURCE=m)

    CALL CalculateEnergy(m_flower, e_flower)
    WRITE(*,*) "Initial flower energy:", e_flower
    WRITE(*,*) "<m>: ", MeanMag

    CALL WriteTecplot(1.0_DP, "m_flower", 1)
    m = umstr*SQRT(Ls)
    CALL WriteTecplot(1.0_DP, "u_flower", 1)


    ! Nucleate a clockwise hard vortex
    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Nucleating Hard Vortex"
    WRITE(*,*) "--"
    WRITE(*,*)

    m = 1
    DO WHILE(.NOT. is_hard_vortex(m))
        ! Set magnetization along hard axis, with a twist
        DO i=1,NNODE
            ! Set m = - VCL X hard + |VCL X hard|*hard
            m(i,:) = CROSS(VCL(i,:), hard_axis)
            m(i,:) = m(i,:) + NORM2(m(i,:)) * hard_axis / SQRT(3.0)
            IF(SUM(m(i,:)**2) .GT. 0.0) THEN
                m(i,:) = m(i,:) / NORM2(m(i,:))
            ELSE
                m(i,:) = hard_axis / SQRT(3.0)
            END IF
        END DO

        Ls = ((esvd_width/(hard_vortex_width))*SQRT(Ls_0))**2
        magnetostriction_dirty = .TRUE.
        CALL randomchange("magnetization", "40")
        CALL SetMaterialParameters(magnetostriction = .FALSE.)
        CALL EnergyMin()
        CALL SetMaterialParameters( &
            magnetostriction = finding_magnetostriction_overlap &
        )
        magnetostriction_dirty = .TRUE.
        CALL EnergyMin()
        CALL randomchange("magnetization", "20")
        CALL EnergyMin()

        IF(.NOT. is_hard_vortex(m)) THEN
            WRITE(*,*) "Hard vortex nucleation failed:"
            CALL CalculateEnergy(m, e_hard_vortex)
            WRITE(*,*) "<m>: ", MeanMag
        END IF
    END DO

    ALLOCATE(m_hard_vortex, SOURCE=m)

    CALL CalculateEnergy(m_hard_vortex, e_hard_vortex)
    WRITE(*,*) "Initial hard vortex energy:", e_hard_vortex
    WRITE(*,*) "<m>: ", MeanMag

    CALL WriteTecplot(1.0_DP, "m_hard_vortex", 1)
    m = umstr*SQRT(Ls)
    CALL WriteTecplot(1.0_DP, "u_hard_vortex", 1)



    ! Nucleate a clockwise easy vortex state
    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Nucleating Easy Vortex"
    WRITE(*,*) "--"
    WRITE(*,*)

    m = 1
    DO WHILE(.NOT. is_easy_vortex(m))
        ! Set magnetization along easy axis, with a twist
        DO i=1,NNODE
            ! Set m = - VCL X easy + |VCL X easy|*easy
            m(i,:) = CROSS(VCL(i,:), easy_axis)
            m(i,:) = m(i,:) + NORM2(m(i,:)) * easy_axis / SQRT(3.0)*2
            IF(SUM(m(i,:)**2) .GT. 0.0) THEN
                m(i,:) = m(i,:) / NORM2(m(i,:))
            ELSE
                m(i,:) = easy_axis / SQRT(3.0)
            END IF
        END DO

        Ls = ((esvd_width/(easy_vortex_width))*SQRT(Ls_0))**2
        magnetostriction_dirty = .TRUE.
        CALL randomchange("magnetization", "40")
        CALL SetMaterialParameters(magnetostriction = .FALSE.)
        CALL EnergyMin()
        CALL SetMaterialParameters( &
            magnetostriction = finding_magnetostriction_overlap &
        )
        magnetostriction_dirty = .TRUE.
        CALL EnergyMin()

        IF(.NOT. is_easy_vortex(m)) THEN
            WRITE(*,*) "Easy vortex nucleation failed:"
            CALL CalculateEnergy(m, e_easy_vortex)
            WRITE(*,*) "<m>: ", MeanMag
        END IF
    END DO

    ALLOCATE(m_easy_vortex, SOURCE=m)

    CALL CalculateEnergy(m_easy_vortex, e_easy_vortex)
    WRITE(*,*) "Initial easy vortex energy:", e_easy_vortex
    WRITE(*,*) "<m>: ", MeanMag

    CALL WriteTecplot(1.0_DP, "m_easy_vortex", 1)
    m = umstr*SQRT(Ls)
    CALL WriteTecplot(1.0_DP, "u_easy_vortex", 1)


    !! Nucleate a clockwise easy vortex state
    !WRITE(*,*)
    !WRITE(*,*) "--"
    !WRITE(*,*) "-- Nucleating Intermediate Vortex"
    !WRITE(*,*) "--"
    !WRITE(*,*)

    !m = 1
    !DO WHILE(.NOT. is_intermediate_vortex(m))
    !    ! Set magnetization along intermediate axis, with a twist
    !    DO i=1,NNODE
    !        ! Set m = - VCL X intermediate + |VCL X intermediate|*intermediate
    !        m(i,:) = CROSS(VCL(i,:), intermediate_axis)
    !        m(i,:) = m(i,:) + NORM2(m(i,:)) * intermediate_axis / SQRT(3.0)
    !        IF(SUM(m(i,:)**2) .GT. 0.0) THEN
    !            m(i,:) = m(i,:) / NORM2(m(i,:))
    !        ELSE
    !            m(i,:) = intermediate_axis / SQRT(3.0)
    !        END IF
    !    END DO

    !    Ls = ((esvd_width/(intermediate_vortex_width))*SQRT(Ls_0))**2
    !    magnetostriction_dirty = .TRUE.
    !    CALL randomchange("magnetization", "40")
    !    CALL SetMaterialParameters(magnetostriction = .FALSE.)
    !    CALL EnergyMin()
    !    CALL SetMaterialParameters( &
    !        magnetostriction = finding_magnetostriction_overlap &
    !    )
    !    magnetostriction_dirty = .TRUE.
    !    CALL EnergyMin()

    !    IF(.NOT. is_intermediate_vortex(m)) THEN
    !        WRITE(*,*) "Intermediate vortex nucleation failed:"
    !        CALL CalculateEnergy(m, e_intermediate_vortex)
    !        WRITE(*,*) "<m>: ", MeanMag
    !    END IF
    !END DO

    !ALLOCATE(m_intermediate_vortex, SOURCE=m)

    !CALL CalculateEnergy(m_intermediate_vortex, e_intermediate_vortex)
    !WRITE(*,*) "Initial intermediate vortex energy:", e_intermediate_vortex
    !WRITE(*,*) "<m>: ", MeanMag

    !CALL WriteTecplot(1.0_DP, "m_intermediate_vortex", 1)
    !m = umstr*SQRT(Ls)
    !CALL WriteTecplot(1.0_DP, "u_intermediate_vortex", 1)


    WRITE(*,*)
    WRITE(*,*) "Nucleation Finished"
    WRITE(*,*)
    !STOP 1

    CALL SetMaterialParameters( &
        magnetostriction = finding_magnetostriction_overlap &
    )

    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Plotting Energy vs. ESVD"
    WRITE(*,*) "--"
    WRITE(*,*)
    BLOCK
        INTEGER :: unit
        INTEGER :: t, tmax = 10
        REAL(KIND=DP) :: width

        REAL(KIND=DP) :: mean_fs(3)
        REAL(KIND=DP) :: mean_hsv(3)
        REAL(KIND=DP) :: mean_esv(3)

        OPEN(NEWUNIT=unit, FILE="energy_vs_length.dat", ACTION="WRITE")

        WRITE(unit,"(13A16)") &
            "ESVD", &
            "E(FS)", "E(HSV)", "E(ESV)", &
            "M_FS_X", "M_FS_Y", "M_FS_Z", &
            "M_HSV_X", "M_HSV_Y", "M_HSV_Z", &
            "M_ESV_X", "M_ESV_Y", "M_ESV_Z"
        CLOSE(unit)

        DO t=0,tmax
            width = (flower_width*(tmax-t) + esvd_width*t)/tmax
            Ls = ((esvd_width/(width))*SQRT(Ls_0))**2
            magnetostriction_dirty = .TRUE.

            WRITE(*,*) "-- Width: ", t, width

            WRITE(*,*) "-- Minimizing FS"
            m = m_flower
            CALL EnergyMin()
            CALL CalculateEnergy(m, e_flower)
            mean_fs = MeanMag
            WRITE(*,*) "m_FS:  ", mean_fs

            WRITE(*,*) "-- Minimizing HSV"
            m = m_hard_vortex
            CALL EnergyMin()
            CALL CalculateEnergy(m, e_hard_vortex)
            mean_hsv = MeanMag
            WRITE(*,*) "m_HSV: ", mean_hsv

            WRITE(*,*) "-- Minimizing ESV"
            m = m_easy_vortex
            CALL EnergyMin()
            CALL CalculateEnergy(m, e_easy_vortex)
            mean_esv = MeanMag
            WRITE(*,*) "m_ESV: ", mean_esv

            OPEN(NEWUNIT=unit, FILE="energy_vs_length.dat", ACTION="WRITE", POSITION="APPEND")
            WRITE(unit,"(13ES16.7)") &
                width, &
                e_flower, e_hard_vortex, e_easy_vortex, &
                mean_fs, mean_hsv, mean_esv
            CLOSE(unit)
        END DO
    END BLOCK


    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Finding FS/HSV Overlap"
    WRITE(*,*) "--"
    WRITE(*,*)

    WRITE(*,*) "-- Optimizing at HSV width"
    Ls = ((esvd_width/(hard_vortex_width))*SQRT(Ls_0))**2
    magnetostriction_dirty = .TRUE.

    m = m_flower
    CALL EnergyMin()
    m_flower = m

    m = m_hard_vortex
    CALL EnergyMin()
    m_hard_vortex = m

    CALL FindOverlap( &
        m_flower, m_hard_vortex, &
        flower_width, esvd_width, &
        fs_hsv_overlap_width, fs_hsv_overlap_energy &
    )


    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Finding HSV/ESV Overlap"
    WRITE(*,*) "--"
    WRITE(*,*)

    WRITE(*,*) "-- Optimizing at FS/HSV overlap width"
    Ls = ((esvd_width/(easy_vortex_width))*SQRT(Ls_0))**2
    magnetostriction_dirty = .TRUE.

    m = m_hard_vortex
    CALL EnergyMin()
    m_hard_vortex = m

    m = m_easy_vortex
    CALL EnergyMin()
    m_easy_vortex = m

    CALL FindOverlap( &
        m_hard_vortex, m_easy_vortex, &
        flower_width, esvd_width, &
        hsv_esv_overlap_width, hsv_esv_overlap_energy &
    )

    !WRITE(*,"(A3,4A16)")    "  ",  "SD", "FS", "HSV", "ESV"
    !WRITE(*,"(A3,4ES16.7)") "E  ", e_sd, e_flower, e_hard_vortex, e_easy_vortex
    !WRITE(*,"(A3,4ES16.7)") "L  ", esvd_width, flower_width, hard_vortex_width, easy_vortex_width

    WRITE(*,*)
    WRITE(*,*) "FS/HSV Overlap:  ", fs_hsv_overlap_width, fs_hsv_overlap_energy
    WRITE(*,*) "HSV/ESV Overlap: ", hsv_esv_overlap_width, hsv_esv_overlap_energy
    WRITE(*,*)

    BLOCK
        INTEGER :: unit
        OPEN(NEWUNIT=unit, FILE="overlap.dat", ACTION="WRITE")
        WRITE(unit,*) &
            "FS/HSV Overlap:  ", fs_hsv_overlap_width, fs_hsv_overlap_energy
        WRITE(unit,*) &
            "HSV/ESV Overlap: ", hsv_esv_overlap_width, hsv_esv_overlap_energy
        CLOSE(unit)
    END BLOCK
CONTAINS
    FUNCTION CROSS(a, b)
        REAL(KIND=DP) :: CROSS(3)
        REAL(KIND=DP), INTENT(IN) :: a(3)
        REAL(KIND=DP), INTENT(IN) :: b(3)

        CROSS = [ &
            a(2)*b(3) - a(3)*b(2), &
            a(3)*b(1) - a(1)*b(3), &
            a(1)*b(2) - a(2)*b(1)  &
        ]
    END FUNCTION CROSS

    PURE FUNCTION mean_mag(m)
        REAL(KIND=DP) :: mean_mag(3)
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        INTEGER :: i

        mean_mag = 0
        DO i=1,NNODE
          mean_mag = mean_mag + m(i,:)*vbox(i)
        END DO
        mean_mag = mean_mag/total_volume
    END FUNCTION mean_mag

    FUNCTION is_flower(m)
        LOGICAL :: is_flower
        REAL(KIND=DP), INTENT(IN) :: m(:,:)

        IF(NORM2(mean_mag(m)) .GT. 0.85) THEN
            is_flower = .TRUE.
        ELSE
            is_flower = .FALSE.
        END IF
    END FUNCTION is_flower

    FUNCTION is_100_vortex(m)
        LOGICAL :: is_100_vortex
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        REAL(KIND=DP) :: mm(3)
        REAL(KIND=DP) :: mm_100, mm_110, mm_111

        mm = mean_mag(m)

        mm_100 = DOT_PRODUCT(ABS(mean_mag(m)), [1.0, 0.0, 0.0])
        mm_110 = DOT_PRODUCT(ABS(mean_mag(m)), [1.0/SQRT(2.0), 1.0/SQRT(2.0), 0.0])
        mm_111 = DOT_PRODUCT(ABS(mean_mag(m)), [1.0/SQRT(3.0), 1.0/SQRT(3.0), 1.0/SQRT(3.0)])

        ! m(1) is much bigger than m(2) and m(3)
        IF( &
            NORM2(mean_mag(m)) .LE. 0.85 &
            .AND. &
            mm_100 .GT. mm_110 &
            .AND. &
            mm_100 .GT. mm_111 &
        ) THEN
            is_100_vortex = .TRUE.
        ELSE
            is_100_vortex = .FALSE.
        END IF
    END FUNCTION is_100_vortex

    FUNCTION is_110_vortex(m)
        LOGICAL :: is_110_vortex
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        REAL(KIND=DP) :: mm(3)
        REAL(KIND=DP) :: mm_100, mm_110, mm_111

        mm = mean_mag(m)

        mm_100 = DOT_PRODUCT(ABS(mean_mag(m)), [1.0, 0.0, 0.0])
        mm_110 = DOT_PRODUCT(ABS(mean_mag(m)), [1.0/SQRT(2.0), 1.0/SQRT(2.0), 0.0])
        mm_111 = DOT_PRODUCT(ABS(mean_mag(m)), [1.0/SQRT(3.0), 1.0/SQRT(3.0), 1.0/SQRT(3.0)])

        ! <m>.[110] > anything else
        IF( &
            NORM2(mm) .LT. 0.80 &
            .AND. &
            mm_110 .GT. mm_100 &
            .AND. &
            mm_110 .GT. mm_111 &
        ) THEN
            is_110_vortex = .TRUE.
        ELSE
            is_110_vortex = .FALSE.
        END IF
    END FUNCTION is_110_vortex

    FUNCTION is_111_vortex(m)
        LOGICAL :: is_111_vortex
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        REAL(KIND=DP) :: mm(3)
        REAL(KIND=DP) :: mm_100, mm_110, mm_111

        mm = mean_mag(m)

        mm_100 = DOT_PRODUCT(ABS(mean_mag(m)), [1.0, 0.0, 0.0])
        mm_110 = DOT_PRODUCT(ABS(mean_mag(m)), [1.0/SQRT(2.0), 1.0/SQRT(2.0), 0.0])
        mm_111 = DOT_PRODUCT(ABS(mean_mag(m)), [1.0/SQRT(3.0), 1.0/SQRT(3.0), 1.0/SQRT(3.0)])

        ! m(1), m(2) and m(3) are about the same size.
        IF( &
            NORM2(mm) .LT. 0.80 &
            .AND. &
            mm_111 .GT. mm_110 &
            .AND. &
            mm_111 .GT. mm_100 &
        ) THEN
            is_111_vortex = .TRUE.
        ELSE
            is_111_vortex = .FALSE.
        END IF
    END FUNCTION is_111_vortex

    SUBROUTINE CalculateEnergy(target_m, etot)
        REAL(KIND=DP), INTENT(IN) :: target_m(:,:)
        REAL(KIND=DP), INTENT(OUT) :: etot

        INTEGER :: neval
        REAL(KIND=DP), ALLOCATABLE :: G(:), X(:)
        REAL(KIND=DP), ALLOCATABLE :: old_m(:,:)

        INTEGER :: i

        ALLOCATE(G(2*NNODE), X(2*NNODE))

        ! Backup m
        ALLOCATE(old_m, SOURCE=m)

        m = target_m

        neval = 1
        MeanMag = 0
        ! Theta (co-lat) indicies= 2n-1, phi (azimuth)  indicies= 2n
        DO i=1,NNODE
          X(2*i-1) = ACOS(m(i,3))
          X(2*i)   = ATAN2(m(i,2),m(i,1))
          MeanMag(:) = MeanMag(:) + m(i,:)*vbox(i)
        END DO
        MeanMag(:)=MeanMag(:)/total_volume

        CALL ET_GRAD(etot,G,X,neval)

        m = old_m
    END SUBROUTINE CalculateEnergy

    FUNCTION enscale(energy)
        REAL(KIND=DP) :: enscale
        REAL(KIND=DP), INTENT(IN) :: energy

        enscale = energy/(Kd*total_volume)
    END FUNCTION enscale

    SUBROUTINE SetMaterialParameters(magnetostriction)
        LOGICAL, INTENT(IN) :: magnetostriction

        calculating_magnetostriction = magnetostriction
        !calculating_magnetostriction = .FALSE.

        anisform = ANISFORM_CUBIC

        IF(magnetostriction) THEN
            ! K1 without magnetostrictive correction
            K1  = 784.0527497320004
        ELSE
            ! K1 with magnetostrictive correction
            K1  = 2020.0
        END IF
        K2  = 0
        Aex = 1.82349e-12
        Ms  = 115738.0

        b1 = -17823159.0
        b2 = -17985695.220000003

        c11 = 136468000000.0
        c12 = 53084800000.0
        c44 = 62843100000.0

        Kd       = mu*MAXVAL(Ms)*MAXVAL(Ms)*0.5
        LambdaEx = SQRT(Aex(MAXLOC(Ms,1))/Kd)
    END SUBROUTINE SetMaterialParameters

    SUBROUTINE FindOverlap(m1, m2, lmin, lmax, length, energy)
        REAL(KIND=DP), INTENT(IN) :: m1(:,:), m2(:,:)
        REAL(KIND=DP), INTENT(IN) :: lmin, lmax
        REAL(KIND=DP), INTENT(OUT) :: length, energy

        REAL(KIND=DP), ALLOCATABLE :: m_test1(:,:), m_test2(:,:)
        REAL(KIND=DP) :: e1, e2
        REAL(KIND=DP) :: avg_m1(3), avg_m2(3)

        INTEGER :: t, t_old, t_high, t_low, tmax = 2**10

        ALLOCATE(m_test1, SOURCE=m1)
        ALLOCATE(m_test2, SOURCE=m2)


        ! Find stable t overlap after minimization
        t_old = -1
        DO
            !
            ! Sanity Check
            !

            ! assert e1(lmin) < e2(lmin)
            Ls = ((esvd_width/lmin)*SQRT(Ls_0))**2
            magnetostriction_dirty = .TRUE.
            CALL CalculateEnergy(m1, e1)
            avg_m1 = MeanMag
            CALL CalculateEnergy(m2, e2)
            avg_m2 = MeanMag
            IF(e2 .LE. e1) THEN
                WRITE(*,*) "ERROR: e2 <= e1 at lmin"
                WRITE(*,*) "lmin: ", lmin
                WRITE(*,*) "e1: ", e1, avg_m1
                WRITE(*,*) "e2: ", e2, avg_m2
                STOP 1
            END IF

            ! assert e1(lmax) > e2(lmax)
            Ls = ((esvd_width/lmax)*SQRT(Ls_0))**2
            magnetostriction_dirty = .TRUE.
            CALL CalculateEnergy(m1, e1)
            avg_m1 = MeanMag
            CALL CalculateEnergy(m2, e2)
            avg_m2 = MeanMag
            IF(e1 .LE. e2) THEN
                WRITE(*,*) "ERROR: e1 <= e2 at lmax"
                WRITE(*,*) "lmax: ", lmax
                WRITE(*,*) "e1: ", e1, avg_m1
                WRITE(*,*) "e2: ", e2, avg_m2
                STOP 1
            END IF


            ! Find t for overlap using binary search
            t_high = tmax
            t_low  = 0
            DO
                ! Break when t_high and t_low converge
                IF(t_high .EQ. t_low) EXIT

                t = (t_high+t_low)/2

                length = (lmin*(tmax-t) + lmax*t)/tmax
                Ls = ((esvd_width/length)*SQRT(Ls_0))**2
                magnetostriction_dirty = .TRUE.

                CALL CalculateEnergy(m_test1, e1)
                avg_m1 = MeanMag
                CALL CalculateEnergy(m_test2, e2)
                avg_m2 = MeanMag

                WRITE(*,*) t, t_high, t_low
                WRITE(*,*) e1, e2
                WRITE(*,*) "m1: ", avg_m1
                WRITE(*,*) "m2: ", avg_m2
                WRITE(*,*)
                IF(e1 .GT. e2) THEN
                    t_high = t
                ELSE
                    t_low = t+1
                END IF
            END DO

            WRITE(*,*) "Overlap energies: ", e1, e2
            WRITE(*,*) "Overlap width:    ", t, length
            WRITE(*,*) "m1: ", avg_m1
            WRITE(*,*) "m2: ", avg_m2
            WRITE(*,*)

            ! Break when the same t is found twice
            IF(t .EQ. t_old) EXIT
            t_old = t

            ! Minimize on the current length
            m = m_test1
            CALL EnergyMin()
            m_test1 = m

            m = m_test2
            CALL EnergyMin()
            m_test2 = m
        END DO

        energy = (e1+e2)/2
    END SUBROUTINE FindOverlap

END PROGRAM Critical_Grain_Sizes
