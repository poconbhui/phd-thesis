#!/usr/bin/env pvpython

from paraview.simple import *

servermanager.LoadState("sd_fs_hsv_esv_deformation_state.pvsm")

views = [
    [FindView("M SD"), FindView("U SD")  ],
    [FindView("M FS"), FindView("U FS")  ],
    [FindView("M HSV"), FindView("U HSV")],
    [FindView("M ESV"), FindView("U ESV")]
]
names = [
    ["m_sd_cube_view", "u_sd_cube_view"  ],
    ["m_fs_cube_view", "u_fs_cube_view"  ],
    ["m_hsv_cube_view", "u_hsv_cube_view"],
    ["m_esv_cube_view", "u_esv_cube_view"]
]

# Fix cube edge width
for view in views:
    GetDisplayProperties(FindSource("FeatureEdges1"), view[0]).LineWidth = 3
    GetDisplayProperties(FindSource("FeatureEdges1"), view[1]).LineWidth = 3

# Fix the display sizes
for view in views:
    view[0].ViewSize = [1000, 1000]
    view[1].ViewSize = [1000, 1000]

# Rezoom to the biggest object
for view in views:
    ResetCamera(view[0])

for view, name in zip(views, names):
    view[0].UseOffscreenRenderingForScreenshots = 1
    view[0].UseOffscreenRendering = 1
    SaveScreenshot("data/%s.png"%(name[0]), view[0])
    view[1].UseOffscreenRenderingForScreenshots = 1
    view[1].UseOffscreenRendering = 1
    SaveScreenshot("data/%s.png"%(name[1]), view[1])
