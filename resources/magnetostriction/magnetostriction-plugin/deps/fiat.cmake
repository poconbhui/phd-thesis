include(ExternalProject)

find_package(PythonInterp REQUIRED)


set(
    FIAT_URL "https://bitbucket.org/fenics-project/fiat/downloads/fiat-1.6.0.tar.gz" CACHE STRING
    "The URL to get the fiat tarball."
)
set(
    FIAT_MD5 "f4509d05c911fd93cea8d288a78a6c6f" CACHE STRING
    "MD5 hash of the fiat tarball"
)


set(FIAT_PREFIX       "${CMAKE_BINARY_DIR}/fiat"   CACHE PATH "")
set(FIAT_DOWNLOAD_DIR "${FIAT_PREFIX}/src"         CACHE PATH "")
set(FIAT_BUILD_DIR    "${FIAT_PREFIX}/build/build" CACHE PATH "")
set(FIAT_SOURCE_DIR   "${FIAT_PREFIX}/build/src"   CACHE PATH "")
set(FIAT_TMP_DIR      "${FIAT_PREFIX}/build/tmp"   CACHE PATH "")
set(FIAT_INSTALL_DIR  "${FIAT_PREFIX}"             CACHE PATH "")

set(FIAT_PYTHON_INSTALL_DIR "${FIAT_INSTALL_DIR}/lib/python" CACHE PATH "")


ExternalProject_Add(
    fiat

    PREFIX ${FIAT_PREFIX}

    SOURCE_DIR ${FIAT_SOURCE_DIR}
    BINARY_DIR ${FIAT_BUILD_DIR}
    INSTALL_DIR ${FIAT_INSTALL_DIR}

    STAMP_DIR ${FIAT_BUILD_DIR}
    TMP_DIR   ${FIAT_TMP_DIR}

    URL ${FIAT_URL}
    URL_MD5 ${FIAT_MD5}
    DOWNLOAD_DIR ${FIAT_DOWNLOAD_DIR}

    CONFIGURE_COMMAND
        ${CMAKE_COMMAND} -E make_directory ${FIAT_BUILD_DIR}
    COMMAND
        ${CMAKE_COMMAND} -E copy_directory ${FIAT_SOURCE_DIR} ${FIAT_BUILD_DIR}

    BUILD_COMMAND
        ${CMAKE_COMMAND} -E chdir ${FIAT_BUILD_DIR}
            ${PYTHON_EXECUTABLE} setup.py build
    INSTALL_COMMAND
        ${CMAKE_COMMAND} -E make_directory ${FIAT_PYTHON_INSTALL_DIR}
    COMMAND
        ${CMAKE_COMMAND} -E env PYTHONPATH=${FIAT_PYTHON_INSTALL_DIR}:$ENV{PYTHONPATH}
        ${CMAKE_COMMAND} -E chdir ${FIAT_BUILD_DIR}
            ${PYTHON_EXECUTABLE} setup.py install
                "--prefix=${FIAT_INSTALL_DIR}"
                "--install-lib=${FIAT_PYTHON_INSTALL_DIR}"
)
