PROGRAM Regular_Formulation_Example

    USE Merrill
    USE Magnetostriction
    IMPLICIT NONE

    TYPE(EnergyCalculator) :: mc

    REAL(KIND=DP) :: lex_width

    REAL(KIND=DP) :: flower_width
    REAL(KIND=DP) :: hard_vortex_width
    REAL(KIND=DP) :: easy_vortex_width

    REAL(KIND=DP), ALLOCATABLE :: m_flower(:,:)
    REAL(KIND=DP), ALLOCATABLE :: m_hard_vortex(:,:)
    REAL(KIND=DP), ALLOCATABLE :: m_easy_vortex(:,:)

    REAL(KIND=DP) :: e_sd
    REAL(KIND=DP) :: e_flower
    REAL(KIND=DP) :: e_hard_vortex
    REAL(KIND=DP) :: e_easy_vortex

    REAL(KIND=DP) :: easy_axis(3) = [1, 0, 0]
    REAL(KIND=DP) :: hard_axis(3) = [1/SQRT(3.0), 1/SQRT(3.0), 1/SQRT(3.0)]

    REAL(KIND=DP) :: Ls_0

    INTERFACE
        LOGICAL FUNCTION is_vortex(m)
            IMPORT DP
            REAL(KIND=DP), INTENT(IN) :: m(:,:)
        END FUNCTION is_vortex
    END INTERFACE

    PROCEDURE(is_vortex), POINTER :: is_easy_vortex => is_100_vortex
    PROCEDURE(is_vortex), POINTER :: is_hard_vortex => is_111_vortex

    INTEGER :: i


    CALL InitializeMerrill()
    CALL InitializeCommandParser()
    CALL InitializeVariableSetter()
    CALL InitializeMagnetostriction()

    calculating_magnetostriction = .TRUE.
    !calculating_magnetostriction = .FALSE.

    anisform = ANISFORM_CUBIC

    ! K1 without magnetostrictive correction
    K1  = 784.0527497320004
    ! K1 with magnetostrictive correction
    !K1  = 2020.0
    K2  = 0
    Aex = 1.82349e-12
    Ms  = 115738.0

    b1 = -17823159.0
    b2 = -17985695.220000003

    c11 = 136468000000.0
    c12 = 53084800000.0
    c44 = 62843100000.0

    Kd       = mu*MAXVAL(Ms)*MAXVAL(Ms)*0.5
    LambdaEx = SQRT(Aex(MAXLOC(Ms,1))/Kd)

    lex_width = 20
    Ls_0 = Ls
    CALL GenerateCubeMesh(lex_width*LambdaEx*SQRT(Ls), LambdaEx*SQRT(Ls))

    ! SD State
    m = 0
    m(:,1) = 1
    CALL CalculateEnergy(m, e_sd)

    CALL WriteTecplot(1.0_DP, "m_SD", 1)
    m = umstr*SQRT(Ls)
    CALL WriteTecplot(1.0_DP, "u_SD", 1)


    ! No Magnetostriction
    flower_width      = 7.85
    hard_vortex_width = 7.85
    easy_vortex_width = 7.85
    !FS/HSV overlap ~ 7.6
    !FS/ESV overlap ~ 7.85

    ! With magnetostriction
    flower_width      = 10
    hard_vortex_width = 10
    easy_vortex_width = 10
    !FS/HSV overlap ~ 7.6
    !FS/ESV overlap ~ 7.85
    !FS > ESV upper bound at 10

    ! Nucleate a flower state

    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Nucleating Flower"
    WRITE(*,*) "--"
    WRITE(*,*)

    m = 0
    m(:,1) = 1
    Ls = ((lex_width/(0.5*flower_width))*SQRT(Ls_0))**2
    magnetostriction_dirty = .TRUE.

    CALL randomchange("magnetization", "20")
    CALL EnergyMin()

    Ls = ((lex_width/(flower_width))*SQRT(Ls_0))**2
    magnetostriction_dirty = .TRUE.
    CALL EnergyMin()

    ALLOCATE(m_flower, SOURCE=m)

    CALL CalculateEnergy(m_flower, e_flower)
    WRITE(*,*) "Initial flower energy:", e_flower
    WRITE(*,*) "<m>: ", MeanMag

    CALL WriteTecplot(1.0_DP, "m_flower", 1)
    m = umstr*SQRT(Ls)
    CALL WriteTecplot(1.0_DP, "u_flower", 1)


    ! Nucleate a clockwise hard vortex
    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Nucleating Hard Vortex"
    WRITE(*,*) "--"
    WRITE(*,*)

    m = 1
    DO WHILE(.NOT. is_hard_vortex(m))
        ! Set magnetization along hard axis, with a twist
        DO i=1,NNODE
            ! Set m = - VCL X hard + |VCL X hard|*hard
            m(i,:) = CROSS(VCL(i,:), hard_axis)
            m(i,:) = m(i,:) + NORM2(m(i,:)) * hard_axis / SQRT(3.0)
            IF(SUM(m(i,:)**2) .GT. 0.0) THEN
                m(i,:) = m(i,:) / NORM2(m(i,:))
            ELSE
                m(i,:) = hard_axis / SQRT(3.0)
            END IF
        END DO

        Ls = ((lex_width/(hard_vortex_width))*SQRT(Ls_0))**2
        magnetostriction_dirty = .TRUE.
        CALL randomchange("magnetization", "40")
        CALL EnergyMin()

        IF(.NOT. is_hard_vortex(m)) THEN
            WRITE(*,*) "Hard vortex nucleation failed:"
            CALL CalculateEnergy(m, e_hard_vortex)
            WRITE(*,*) "<m>: ", MeanMag
        END IF
    END DO

    ALLOCATE(m_hard_vortex, SOURCE=m)

    CALL CalculateEnergy(m_hard_vortex, e_hard_vortex)
    WRITE(*,*) "Initial hard vortex energy:", e_hard_vortex
    WRITE(*,*) "<m>: ", MeanMag

    CALL WriteTecplot(1.0_DP, "m_hard_vortex", 1)
    m = umstr*SQRT(Ls)
    CALL WriteTecplot(1.0_DP, "u_hard_vortex", 1)



    ! Nucleate a clockwise easy vortex state
    WRITE(*,*)
    WRITE(*,*) "--"
    WRITE(*,*) "-- Nucleating Easy Vortex"
    WRITE(*,*) "--"
    WRITE(*,*)

    m = 1
    DO WHILE(.NOT. is_easy_vortex(m))
        ! Set magnetization along easy axis, with a twist
        DO i=1,NNODE
            ! Set m = - VCL X easy + |VCL X easy|*easy
            m(i,:) = CROSS(VCL(i,:), easy_axis)
            m(i,:) = m(i,:) + NORM2(m(i,:)) * easy_axis / SQRT(3.0)
            IF(SUM(m(i,:)**2) .GT. 0.0) THEN
                m(i,:) = m(i,:) / NORM2(m(i,:))
            ELSE
                m(i,:) = easy_axis / SQRT(3.0)
            END IF
        END DO

        Ls = ((lex_width/(easy_vortex_width))*SQRT(Ls_0))**2
        magnetostriction_dirty = .TRUE.
        CALL randomchange("magnetization", "40")
        CALL EnergyMin()

        IF(.NOT. is_easy_vortex(m)) THEN
            WRITE(*,*) "Easy vortex nucleation failed:"
            CALL CalculateEnergy(m, e_easy_vortex)
            WRITE(*,*) "<m>: ", MeanMag
        END IF
    END DO

    ALLOCATE(m_easy_vortex, SOURCE=m)

    CALL CalculateEnergy(m_easy_vortex, e_easy_vortex)
    WRITE(*,*) "Initial easy vortex energy:", e_easy_vortex
    WRITE(*,*) "<m>: ", MeanMag

    CALL WriteTecplot(1.0_DP, "m_easy_vortex", 1)
    m = umstr*SQRT(Ls)
    CALL WriteTecplot(1.0_DP, "u_easy_vortex", 1)


    WRITE(*,*) "DONE"

    WRITE(*,"(A3,4A16)")    "  ",  "SD", "FS", "HSV", "ESV"
    WRITE(*,"(A3,4ES16.7)") "E  ", e_sd, e_flower, e_hard_vortex, e_easy_vortex
    WRITE(*,"(A3,4ES16.7)") "L  ", lex_width, flower_width, hard_vortex_width, easy_vortex_width
CONTAINS
    FUNCTION CROSS(a, b)
        REAL(KIND=DP) :: CROSS(3)
        REAL(KIND=DP), INTENT(IN) :: a(3)
        REAL(KIND=DP), INTENT(IN) :: b(3)

        CROSS = [ &
            a(2)*b(3) - a(3)*b(2), &
            a(3)*b(1) - a(1)*b(3), &
            a(1)*b(2) - a(2)*b(1)  &
        ]
    END FUNCTION CROSS

    PURE FUNCTION mean_mag(m)
        REAL(KIND=DP) :: mean_mag(3)
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        INTEGER :: i

        mean_mag = 0
        DO i=1,NNODE
          mean_mag = mean_mag + m(i,:)*vbox(i)
        END DO
        mean_mag = mean_mag/total_volume
    END FUNCTION mean_mag

    FUNCTION is_flower(m)
        LOGICAL :: is_flower
        REAL(KIND=DP), INTENT(IN) :: m(:,:)

        IF(NORM2(mean_mag(m)) .GT. 0.85) THEN
            is_flower = .TRUE.
        ELSE
            is_flower = .FALSE.
        END IF
    END FUNCTION is_flower

    FUNCTION is_100_vortex(m)
        LOGICAL :: is_100_vortex
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        REAL(KIND=DP) :: mm(3)

        mm = mean_mag(m)

        ! m(1) is much bigger than m(2) and m(3)
        IF( &
            NORM2(mean_mag(m)) .LE. 0.85 &
            .AND. &
            ABS(NORM2(mean_mag(m)) - mm(1))/ABS(mm(1)) .LT. 0.1 &
        ) THEN
            is_100_vortex = .TRUE.
        ELSE
            is_100_vortex = .FALSE.
        END IF
    END FUNCTION is_100_vortex

    FUNCTION is_111_vortex(m)
        LOGICAL :: is_111_vortex
        REAL(KIND=DP), INTENT(IN) :: m(:,:)
        REAL(KIND=DP) :: mm(3)

        mm = mean_mag(m)

        ! m(1), m(2) and m(3) are about the same size.
        IF( &
            NORM2(mm) .LT. 0.80 &
            .AND. &
            SUM(ABS(mm)) .GT. 2*mm(1) &
        ) THEN
            is_111_vortex = .TRUE.
        ELSE
            is_111_vortex = .FALSE.
        END IF
    END FUNCTION is_111_vortex

    SUBROUTINE CalculateEnergy(target_m, etot)
        REAL(KIND=DP), INTENT(IN) :: target_m(:,:)
        REAL(KIND=DP), INTENT(OUT) :: etot

        INTEGER :: neval
        REAL(KIND=DP), ALLOCATABLE :: G(:), X(:)
        REAL(KIND=DP), ALLOCATABLE :: old_m(:,:)

        INTEGER :: i

        ALLOCATE(G(2*NNODE), X(2*NNODE))

        ! Backup m
        ALLOCATE(old_m, SOURCE=m)

        m = target_m

        neval = 1
        MeanMag = 0
        ! Theta (co-lat) indicies= 2n-1, phi (azimuth)  indicies= 2n
        DO i=1,NNODE
          X(2*i-1) = ACOS(m(i,3))
          X(2*i)   = ATAN2(m(i,2),m(i,1))
          MeanMag(:) = MeanMag(:) + m(i,:)*vbox(i)
        END DO
        MeanMag(:)=MeanMag(:)/total_volume

        CALL ET_GRAD(etot,G,X,neval)

        m = old_m
    END SUBROUTINE CalculateEnergy

    FUNCTION enscale(energy)
        REAL(KIND=DP) :: enscale
        REAL(KIND=DP), INTENT(IN) :: energy

        enscale = energy/(Kd*total_volume)
    END FUNCTION enscale

END PROGRAM Regular_Formulation_Example
