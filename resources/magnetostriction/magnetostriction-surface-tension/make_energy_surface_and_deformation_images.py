#!/usr/bin/env pvpython

from sys import argv
print(argv)
import sys
sys.exit

if len(argv) < 5:
    print("Usage: pvpython %s energy_surface_source deformation_source tension_arrows output_prefix"%(argv[0]))
    import sys
    sys.exit(1)

energy_surface_source = argv[1]
deformation_source    = argv[2]
tension_arrows        = argv[3]
pressure              = argv[4]
output_prefix         = argv[5]

from paraview.simple import *

servermanager.LoadState("energy_surface_and_deformation_state.pvsm")

views = [FindView("Energy Surface"), FindView("Deformation")]
names = ["energy_surface", "deformation"]

# Load files
FindSource("Energy Surface").FileNames = energy_surface_source
FindSource("Displacement").FileNames   = deformation_source

# Set tension arrows
arrows_111 = FindSource("[111] Arrows")
arrows_100 = FindSource("[100] Arrows")
Hide(arrows_111, views[1])
Hide(arrows_100, views[1])
if tension_arrows == "[111]":
    Show(arrows_111, views[1])
if tension_arrows == "[100]":
    Show(arrows_100, views[1])

# Fix cube edge width
GetDisplayProperties(FindSource("FeatureEdges1"), views[1]).LineWidth = 3

# Fix energy surface colours
e_range = FindSource("E").PointData.GetArray("E").GetRange()
GetColorTransferFunction("E").RGBPoints[0] = e_range[0]
GetColorTransferFunction("E").RGBPoints[4] = (e_range[0]+e_range[1])/2
GetColorTransferFunction("E").RGBPoints[8] = e_range[1]

# Set warp by vector scaling
if pressure == "1e8":
    FindSource("WarpByVector1").ScaleFactor = 800
else:
    FindSource("WarpByVector1").ScaleFactor = 2e3

# Fix the display sizes
for view in views:
    view.ViewSize = [1000, 1000]

# Rezoom to the biggest object
for view in views:
    ResetCamera(view)

# Move energy surface up a bit to make room for the colour bar
print()
print("CameraPosition: " + str(view.CameraPosition))
print("CameraFocalPoint: " + str(view.CameraFocalPoint))
print()
FindSource("E").UpdatePipeline()
e_bounds = FindSource("E").GetDataInformation().GetBounds()
e_height = e_bounds[4] - e_bounds[3]
views[0].CameraPosition = [
    views[0].CameraPosition[0],
    views[0].CameraPosition[1] + e_height*0.22,
    views[0].CameraPosition[2]
]
views[0].CameraFocalPoint = [
    views[0].CameraFocalPoint[0],
    views[0].CameraFocalPoint[1] + e_height*0.22,
    views[0].CameraFocalPoint[2]
]
views[0].CameraParallelScale = views[0].CameraParallelScale * 1.05

for view, name in zip(views, names):
    view.UseOffscreenRenderingForScreenshots = 1
    view.UseOffscreenRendering = 1
    SaveScreenshot("%s.%s.png"%(output_prefix, name), view)
