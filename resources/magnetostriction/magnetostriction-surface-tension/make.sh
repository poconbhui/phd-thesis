#!/usr/bin/bash

data_dir=$PWD/pressure_surface/data/tm60/

function make_image() (
    tension=$1
    solver=$2
    pressure=$3
    filebase=${data_dir}/${tension}/${solver}/cube_100nm_${pressure}Pa
    energy_surface=${filebase}.mstr.rel.stl
    deformation=${filebase}.displacement.tec

    outbase=$PWD/images/t_${tension}_${solver}_${pressure}

    pvpython make_energy_surface_and_deformation_images.py \
        ${energy_surface} \
        ${deformation} \
        ${tension} \
        ${pressure} \
        ${outbase}
)


for tension in [111] [100] static
do
    for solver in dynamic static-0 static-[111] static-[100]
    do
        for pressure in 0 1e6 1e7 1e8
        do
            make_image ${tension} ${solver} ${pressure}
        done
    done
done
