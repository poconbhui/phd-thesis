#!/usr/bin/env bash

set -e

source_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
build_dir=$source_dir/build/build
install_dir=$source_dir/build/install
data_dir=$source_dir/data

merrill_dir=$HOME/prog/merrill/build/install/

FC=$(which gfortran)

cmake \
    -B"${build_dir}/deps" \
    -H"${source_dir}/deps" \
    -DCMAKE_INSTALL_PREFIX:PATH="${install_dir}" \
    -DBUILD_SHARED_LIBS:BOOL=ON
make -C "${build_dir}/deps"

cmake \
    -B"${build_dir}" \
    -H"${source_dir}" \
    -DCMAKE_Fortran_COMPILER=$(which gfortran) \
    -DMERRILL_DIR:PATH=${merrill_dir} \
    -DDOLFIN_DIR:PATH="${build_dir}/deps/dolfin/share/dolfin/cmake/" \
    -DFFC_DIR:PATH="${build_dir}/deps/ffc/" \
    -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -DCMAKE_Fortran_FLAGS:STRING="-std=f2008ts -Wall -Wextra -pedantic -Wsurprising -Wno-unused-parameter -Wno-unused-dummy-argument -fmax-errors=2"
    #-DCMAKE_Fortran_FLAGS:STRING="-std=f2008ts -Wall -Wextra -pedantic -Wsurprising -Wno-unused-parameter -Wno-unused-dummy-argument -fmax-errors=2 -ffpe-trap=invalid,overflow,underflow,denormal,zero"

export PYTHONPATH=$PYTHONPATH:${build_dir}/deps/ufl/lib/python
export PYTHONPATH=$PYTHONPATH:${build_dir}/deps/ffc/lib/python
export PYTHONPATH=$PYTHONPATH:${build_dir}/deps/fiat/lib/python
export PYTHONPATH=$PYTHONPATH:${build_dir}/deps/instant/lib/python
make -C "${build_dir}"
make -C "${build_dir}" test ARGS=-V
exit


mkdir -p ${data_dir}
cd ${data_dir}
${build_dir}/material_free_rhs

cd ${source_dir}
export PV_PLUGIN_PATH=/home/paddy/prog/merrill/demo/paraview-plugins
pvpython make_deformation_images.py
