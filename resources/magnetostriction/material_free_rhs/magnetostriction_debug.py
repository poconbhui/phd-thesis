#!/usr/bin/env python
from ufl import *
set_level(DEBUG)
from ufl import *

# Generate a 3D vector space over tetrahedral cells.
cell = tetrahedron
element = VectorElement("Lagrange", cell, 1, dim=3)

# Set up trial function u, test function v
# Solution in trial function is the equilibrium displacement field
# for a given magnetization.
u = TrialFunction(element)
v = TestFunction(element)

# Magnetization of the system.
m  = Coefficient(element)

# Surface traction of the system.
t = Coefficient(element)

# The equilibrium displacement for m
u0 = Coefficient(element)

# The scale of the coordinates (eg scale = 1e-6 for coords in microns)
scale = Constant(cell)


###############################################################################


#
# Construct the mechanical equilibrium RHS linear form, mechanical_L(v).
#


# The crystal axes.
e1 = VectorConstant(cell, dim=3)
e2 = VectorConstant(cell, dim=3)
e3 = VectorConstant(cell, dim=3)

# The directional cosines with the crystal axes.
alpha = as_vector((dot(m, e1), dot(m, e2), dot(m, e3)))

# The (cubic) magnetoelastic coefficients.
b = VectorConstant(cell, dim=2)

# The elastic tensor coefficients for a cubic crystal in voigt notation
c11 = Constant(cell)
c12 = Constant(cell)
c44 = Constant(cell)
c = as_matrix((
    (c11, c12, c12, 0.0, 0.0, 0.0),
    (c12, c11, c12, 0.0, 0.0, 0.0),
    (c12, c12, c11, 0.0, 0.0, 0.0),
    (0.0, 0.0, 0.0, c44, 0.0, 0.0),
    (0.0, 0.0, 0.0, 0.0, c44, 0.0),
    (0.0, 0.0, 0.0, 0.0, 0.0, c44)
))

# The compliance tensor
s_denom = (c11-c12)*(c11+2*c12)
s = as_matrix((
    ( (c11+c12)/s_denom, -c12/s_denom,      -c12/s_denom,      0.0, 0.0, 0.0 ),
    ( -c12/s_denom,      (c11+c12)/s_denom, -c12/s_denom,      0.0, 0.0, 0.0 ),
    ( -c12/s_denom,      -c12/s_denom,      (c11+c12)/s_denom, 0.0, 0.0, 0.0 ),
    ( 0.0, 0.0, 0.0, 1/c44, 0.0,   0.0   ),
    ( 0.0, 0.0, 0.0, 0.0,   1/c44, 0.0   ),
    ( 0.0, 0.0, 0.0, 0.0,   0.0,   1/c44 )
))


# The equilibrium strain due to m.
epsilon_m = as_matrix((
    (b[0]*alpha[0]*alpha[0], b[1]*alpha[0]*alpha[1], b[1]*alpha[0]*alpha[2]),
    (b[1]*alpha[1]*alpha[0], b[0]*alpha[1]*alpha[1], b[1]*alpha[1]*alpha[2]),
    (b[1]*alpha[2]*alpha[0], b[1]*alpha[2]*alpha[1], b[0]*alpha[2]*alpha[2])
))
eps_m = sym(epsilon_m)

eps_v = sym(grad(v))

# Voigt notation for epsilon(m) and epsilon(v)
voigt_eps_v = as_vector((
    eps_v[0,0], eps_v[1,1], eps_v[2,2],
    (eps_v[1,2]+eps_v[2,1]), (eps_v[0,2]+eps_v[2,0]), (eps_v[0,1]+eps_v[1,0])
))

#eps_m = epsilon_m
voigt_eps_m = as_vector((
    eps_m[0,0], eps_m[1,1], eps_m[2,2],
    (eps_m[1,2]+eps_m[2,1])/2, (eps_m[0,2]+eps_m[2,0])/2, (eps_m[0,1]+eps_m[1,0])/2
))

#x = SpatialCoordinate(cell)
xmx0 = Coefficient(cell)
#xmx0 = SpatialCoordinate(cell)
eps_tx = as_matrix((
    (t[0]*xmx0[0], t[0]*xmx0[1], t[0]*xmx0[2]),
    (t[1]*xmx0[0], t[1]*xmx0[1], t[1]*xmx0[2]),
    (t[2]*xmx0[0], t[2]*xmx0[1], t[2]*xmx0[2]),
))
eps_tx = sym(eps_tx)

voigt_eps_tx = as_vector((
    eps_tx[0,0], eps_tx[1,1], eps_tx[2,2],
    (eps_tx[1,2]+eps_tx[2,1]), (eps_tx[0,2]+eps_tx[2,0]), (eps_tx[0,1]+eps_tx[1,0])
))
voigt_eps_tx2 = as_vector((
    eps_tx[0,0], eps_tx[1,1], eps_tx[2,2],
    (eps_tx[1,2]+eps_tx[2,1])/2, (eps_tx[0,2]+eps_tx[2,0])/2, (eps_tx[0,1]+eps_tx[1,0])/2
))
voigt_eps_v2 = as_vector((
    eps_v[0,0], eps_v[1,1], eps_v[2,2],
    (eps_v[1,2]+eps_v[2,1])/2, (eps_v[0,2]+eps_v[2,0])/2, (eps_v[0,1]+eps_v[1,0])/2
))


# The mechanical equilibrium RHS
mechanical_L_density = -voigt_eps_m[i]*s[i,j]*voigt_eps_v[j]*(1/scale)
mechanical_L = (
    mechanical_L_density*(scale**3)*dx
    + voigt_eps_tx2[i]*s[i,j]*voigt_eps_v[j]*(scale**2)*ds
)

#mechanical_L_density = -inner(epsilon_m, (1/scale)*sym(grad(v)))
new_form = True
if not new_form:
    mechanical_L_density = -voigt_eps_m[i]*voigt_eps_v[i]*(1/scale)
    mechanical_L = mechanical_L_density*(scale**3)*dx + inner(t, v)*(scale**2)*ds


###############################################################################


#
# Construct the mechanical equilibrium LHS bilinear form, mechanical_a(u,v)
#



# The strain, epsilon, due to a displacement u.
# Also the same for v due to some math trickery.
eps_u = sym(grad(u))
eps_v = sym(grad(v))

# Voigt notation for epsilon(u) and epsilon(v)
voigt_eps_u = as_vector((
    eps_u[0,0], eps_u[1,1], eps_u[2,2],
    (eps_u[1,2]+eps_u[2,1]), (eps_u[0,2]+eps_u[2,0]), (eps_u[0,1]+eps_u[1,0])
))

voigt_eps_v = as_vector((
    eps_v[0,0], eps_v[1,1], eps_v[2,2],
    (eps_v[1,2]+eps_v[2,1]), (eps_v[0,2]+eps_v[2,0]), (eps_v[0,1]+eps_v[1,0])
))


# The mechanical equilibrium LHS
#mechanical_a_density = dot(
#    (1/scale)*voigt_eps_u,
#    dot(c, (1/scale)*voigt_eps_v)
#)
#mechanical_a_density = (1/scale)**2 * inner(eps_u, eps_v)
mechanical_a_density = (1/scale)**2 * voigt_eps_u[i]*voigt_eps_v[i]
mechanical_a = mechanical_a_density*(scale**3)*dx

if not new_form:
    mechanical_a_density = (1/scale)**2 * voigt_eps_u[i]*c[i,j]*voigt_eps_v[j]
    mechanical_a = mechanical_a_density*(scale**3)*dx

mechanical_a_perturb = 2*inner(u,v)*dx


###############################################################################

#
# Define the null space for the elastic problem with free boundaries
#

# Note on solving the mechanical equilibrium equations for the
# case of elasticity:
#
# With pure neumann conditions, any solution u including
# a rigid body motion should solve our problem.
# Therefore, for two solutions q,s to our linear system
# Ax=b, we get A(q-s) = (b-b) = 0. So, we find A is singular
# since for a nontrivial e=q-s, Ae = 0.
#
# For a stable solver, we must provide the null space vectors
# to ensure <x,e> = 0 for each e in Kernel(A) (ie null space of A)
# to ensure a unique solution to Ax=b, since A(x+e)=b is also a
# solution. Without the restriction, a minizing solver would be free
# to travel along any vector e indefinitely, as it defines a line
# of constant energy.
#
# We must also remove any contributions of e to b since there is
# no x for which Ax=e for e in Kernel(A) meaning the problem
# is ill-posed and our solver will fail. This can be shown
# by expanding x in terms of eigenvectors of A with eigenvalues
# of eigenvectors in Kernel(A) equal to 0. We can justify the
# orthogonalisation of b wrt e by saying if b=c+e,
# for Ax=b=c+e, we also get (A^2)x=A(c+e)=Ac, and we solve for
# this x instead.
#


x = SpatialCoordinate(cell)

mechanical_null_space = as_matrix((
    # Displacement
    (1, 0, 0),
    (0, 1, 0),
    (0, 0, 1),

    # Rotation
    (x[1], -x[0],  0.0 ),
    (x[2],  0.0,  -x[0]),
    (0.0,   x[2], -x[1])
))


###############################################################################


#
# Construct the magnetostrictive LHS bilinear form, magnetic_a(u,v)
#


magnetic_a_density = dot(u, v)
magnetic_a = magnetic_a_density*(scale**3)*dx


###############################################################################


#
# Construct the magnetostrictive RHS linear form, magnetic_L(v)
#


#
# The RHS defining the derivative of the magnetoelastic energy for
# the magnetisation, m. This is correct only for the case where
# the displacement field has been minimised for the supplied m as
# it takes advantage of the fact that
#     (d/d(q2_i) E)(m, u0) = 0
# for a minimised u0 at a given m where d/d(q2_i) represents the
# derivative with respect to the ith element of the 2nd argument, so
#     (d/d(m_j) E)(m_0, m_1, ... , u0_0, u0_1, ...)
#     = ((d/d(q1_i) E)*(d/d(m_j) q1_i))(m_0,...,u_0,...)
#         + ((d/d(q2_i) E)*(d/d(m_j) q2_i))(m_o,...,u_0,...)
#     = ((d/d(q1_i) E)*(d/d(m_j) q1_i))(m_0,...,u_0,...) + 0
#
# In the case of the magnetostrictive energy density, this leaves
# only the cross terms as non-zero. (In Kittel's paper, we ignore
# the anisotropy energy term included, because this is considered
# separately.)
#
# We assume unit m and unit axes.
#


# The saturation magnetization of the material
Ms = Constant(cell)


# Derivatives of the directional cosines wrt m_i with unit m.
def f_d_alpha(m, e, i):
    return e[i] - dot(m, e)*m[i]

d_alpha = as_matrix((
    ( f_d_alpha(m, e1, 0), f_d_alpha(m, e1, 1), f_d_alpha(m, e1, 2) ),
    ( f_d_alpha(m, e2, 0), f_d_alpha(m, e2, 1), f_d_alpha(m, e2, 2) ),
    ( f_d_alpha(m, e3, 0), f_d_alpha(m, e3, 1), f_d_alpha(m, e3, 2) )
))


# The minimised strain from the minimised displacement
eps_u0 = sym(grad(u0))

voigt_eps_u0 = as_vector((
    eps_u0[0,0], eps_u0[1,1], eps_u0[2,2],
    (eps_u0[1,2]+eps_u0[2,1]),
    (eps_u0[0,2]+eps_u0[2,0]),
    (eps_u0[0,1]+eps_u0[1,0])
))


# h_eff(i) = - (1/Ms) * (d/d(m_i) E)
def h_eff(i):
    return - 1.0/Ms*(
        b[0]*(
              voigt_eps_u0[0]*2*alpha[0]*d_alpha[0,i]
            + voigt_eps_u0[1]*2*alpha[1]*d_alpha[1,i]
            + voigt_eps_u0[2]*2*alpha[2]*d_alpha[2,i]
        )
        + b[1]*(
              voigt_eps_u0[3]*(
                d_alpha[1,i]*alpha[2] + alpha[1]*d_alpha[2,i]
            )
            + voigt_eps_u0[4]*(
                d_alpha[0,i]*alpha[2] + alpha[0]*d_alpha[2,i]
            )
            + voigt_eps_u0[5]*(
                d_alpha[0,i]*alpha[1] + alpha[0]*d_alpha[1,i]
            )
        )
    )


magnetic_L_density = inner(
    (1/scale)*as_vector(( h_eff(0), h_eff(1), h_eff(2) )),
    v
)

magnetic_L = magnetic_L_density*(scale**3)*dx


###############################################################################


#
# Define the magnetostrictive energy for a given m and u0(m).
#


offset = - (b[0]**2) * (c11 + c12) / ( 2 * (c11 - c12) * (c11 + 2*c12) )

energy_density = (
    b[0]*(1/scale)*(
          (alpha[0]**2)*eps_u0[0,0]
        + (alpha[1]**2)*eps_u0[1,1]
        + (alpha[2]**2)*eps_u0[2,2]
    )
    + b[1]*(1/scale)*(
          alpha[1]*alpha[2]*(eps_u0[1,2]+eps_u0[2,1])
        + alpha[0]*alpha[2]*(eps_u0[0,2]+eps_u0[2,0])
        + alpha[0]*alpha[1]*(eps_u0[0,1]+eps_u0[1,0])
    )
    + 0.5*c11*((1/scale)**2)*(
        eps_u0[0,0]**2 + eps_u0[1,1]**2 + eps_u0[2,2]**2
    )
    + 0.5*c44*((1/scale)**2)*(
          (eps_u0[0,1]+eps_u0[1,0])**2
        + (eps_u0[1,2]+eps_u0[2,1])**2
        + (eps_u0[0,2]+eps_u0[2,0])**2
    )
    + c12*((1/scale)**2)*(
          eps_u0[0,0]*eps_u0[1,1]
        + eps_u0[1,1]*eps_u0[2,2]
        + eps_u0[0,0]*eps_u0[2,2]
    )
) - offset

energy = energy_density*(scale**3)*dx


###############################################################################


forms = [
    mechanical_a, mechanical_a_perturb, mechanical_L,
    magnetic_a, magnetic_L,
    energy
]
