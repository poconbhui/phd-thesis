PROGRAM Material_Free_RHS

    USE Merrill
    USE Magnetostriction

    TYPE(EnergyCalculator) :: mc
    REAL(KIND=DP) :: pressure

    CALL InitializeMerrill()
    CALL InitializeCommandParser()
    CALL InitializeVariableSetter()
    CALL InitializeMagnetostriction()

    calculating_magnetostriction = .TRUE.

    ! K1 without magnetostrictive correction
    K1  = 784.0527497320004
    K2  = 0
    Aex = 1.82349e-12
    Ms  = 115738.0

    b1 = -17823159.0
    b2 = -17985695.220000003

    c11 = 136468000000.0
    c12 = 53084800000.0
    c44 = 62843100000.0

    Kd       = mu*MAXVAL(Ms)*MAXVAL(Ms)*0.5
    LambdaEx = SQRT(Aex(MAXLOC(Ms,1))/Kd)

    traction_fnptr => uniaxial_tension

    CALL GenerateCubeMesh(5*LambdaEx, LambdaEx)

    mc = MagnetostrictionCalculator()
    CALL mc%Initialize()

    pressure = 0.0
    magnetostriction_dirty = .TRUE.
    CALL mc%Build()
    m = 0
    m(:,1) = 1
    CALL mc%Run()

    CALL WriteTecplot(1.0_DP, "m", 1)
    m = umstr*SQRT(Ls)
    CALL WriteTecplot(1.0_DP, "u", 1)


    pressure = 1e7
    magnetostriction_dirty = .TRUE.
    CALL mc%Build()
    m = 0
    m(:,1) = 1
    CALL mc%Run()

    CALL WriteTecplot(1.0_DP, "m_p", 1)
    m = umstr*SQRT(Ls)/2
    CALL WriteTecplot(1.0_DP, "u_p", 1)

    WRITE(*,*) "DONE"

CONTAINS
    ! Uniaxial tension in the x-direction
    SUBROUTINE uniaxial_tension(value, position, normal)
        USE, INTRINSIC :: ISO_C_BINDING, ONLY: C_DOUBLE

        REAL(C_DOUBLE), INTENT(OUT) :: value(3)
        REAL(C_DOUBLE), INTENT(IN)  :: position(3), normal(3)

        value(1) = pressure*normal(1)
        value(2) = 0
        value(3) = 0
    END SUBROUTINE uniaxial_tension
END PROGRAM Material_Free_RHS
