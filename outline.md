Introduction
============

- Set out a bunch of terminology we'll be using extensively throughout
  the thesis.
  Present the case for Rock Magnetism (RM) and Palaeomagnetism (PM)
  There should be plenty of justification in D&0 and Tauxe.
  Present the case for Theoretical RM and PM.

## Single Domain, Multi Domain, Pseudo-Single Domain
- We'll ignore practical, experimental definitions of SD, MD, PSD grains
  and consider only 3 cases of Uniform, Two-Domain and Vortex states.
  Maybe flowers, if we're stuck.

## Micromagnetic Energies
- The basic micromagnetic energies

### Exchange
- The straightener.
  Provide the continuum equation, provide some arrow diagrams to show good,
  not so good and bad alignments.

### Anisotropy
- The aligner.
  Provide continuum equation, again some arrows, and the energy surface.

### Demag
- The jumbler.
  Provide the continuum equation. Roughly state that a jumbled mess is good,
  and a uniform magnetization is bad.

### Interactions
- Interactions of the three energies.
- Demag and exchange - Chaos vs Order
  - Pure chaos will never win because negative exchange energy is unbounded
    in the continuum (derivative of rapidly changing value)
  - Primary drivers for SD, PSD and MD states.
- Exchange and Anisotropy - Conformity vs Variation
  - Primary drivers for domain wall thicknesses.
  - Not so important for PSD (?),
    since grain size << theoretical domain wall size.

### FEniCS
- Refer to appendix for FEM
- Layout quick intro to setting up weak forms
- A panacea of sorts.
- Quick to set up
- Slow to set up very nicely
- Can be a bit slow to run
- Can be even slower to eventually scoop out of the project.
- Need some sort of header-only classy solution for wrapping UFL to
  linear element functions and generating matrices, even at the cost of
  parallelism. Maybe it already exists?



Magnetostriction
================

## Kittel's Formulation
- From Kittel 1949 - Variation of Anis only
- Anisotropy approximation
- Show Kittel's constant stress derivation is wrong, provide
  correct derivation and result.

## Brown's Formulation
- From Brown's Magnetostriction - Variation of everything, including M and H.
- Show Brown's formulation is a superset of Kittel's formulation, so
  functions derived in good generality there for variations of the energy
  can be used for Kittel's stuff as well. Overkill, but useful to know
  the equations are definitely correct.

## Kroner and Fabian's Formulation
- Using equilibrium strain due to local magnetization as a local plastic
  deformation.
- Showing inc e = 0, because the final deformation has no cracks, the
  final displacement can be described using a continuous vector field.
- Show Karl's formulation and my formulation are the same.
- Maybe leave this for the dislocations section?

## FEM/Weak Form Formulation
- How we can generate a simple formulation by using instant elastic
  equilibriation.
- How the usual formulation also assumes instant elastic equilibriation,
  so the LLG method still isn't quite right. - How wrong?

## Magnetostriction For Free Grains
- Show how to describe free grains with a surface pressure.
- Results from Castle Meeting: surprise result that pure magnetostriction
  in free grains looks exactly like the anisotropy approximation
- Show what local anisotropy energy for a magnetization in an SD and PSD
  state looks like with positions frozen and unfrozen.
- Maybe look at results with non-instant elastic equilibriation.
  - Could be surprising! ... or not...

## Magnetostriction For Embedded Grains
- Show how to describe grains with fixed surfaces and coupled surfaces.
- Look at results for grain with frozen surface.
  - Some results from Castle meeting - Kittel's derivation visibly wrong.
- Generate mesh or a grain embedded in a larger mesh.
- Look at grain size in PSD regime when free.
- See if stiffness of embedding matrix changes coercivity or PSD nucleation
  - According to Karl, stiffness near the stiffness of the grain should
    significantly affect PSD nucleation.



Crystal Defects
===============

- Add crystal defects, mediated by magnetostriction.
- Much of this depends on deriving Kittel's stress equations for arbitrary
  boundary conditions. Pure Neumann could be painful...

## Kroner's formulation of crystal defects
- Shown earlier? Discuss derivation of internal stress field due to
  plastic deformation.

## Burger's Vectors And Crystal Defects
- Describe relation of Burger's Vectors to various crystal defects.
- Derivation? Kroner's book is pretty hard to come by.

## Coupling Of Crystal Defects Via Magnetostriction
- Show how to add plastic deformations to the mechanical equilibrium
  equations for magnetostriction.

## Some Effects Of Crystal Defects On Magnetization
- ???
- Line defects and slip planes
- Defects to minimize strain of initial magnetization - mimicking cooling
  with remanence.



MERRILL
=======

## Guide to modifying MERRILL

## Source code organization

## BEMScript optimization

## Plugins

## Main program


Meshing
=======

## Introduction
- General usefulness for FEM
### CGAL
- Open source computer geometry package
- Much more than just meshing
- Can be super painful
  - Reading the documentation is ESSENTIAL
  - Tracking classes through the source code can be a route to madness
  - Trusting conventions is necessary, even if they're a bit inconsistent
  - Error messages from gcc are absolutely NUTS and completely useless
    - Look into error messages from clang?

## Stack Processing
- Fib to voxel data
- Good for Finite Difference methods, bad for FEM.
- Corners can cause magnetic anomalies in FEM.
- Need some way of approximating data to a smooth surface
- The real problem is the detail needed for micromagnetic models is
  finer than the detail we have for our fib images.
- Need some way of interpolating between steps, and even for un-smoothing
  flat walls that are unlikely to actually be flat.

## Poisson Surface Reconstruction
- Packages/Routines Used:
  * CGAL_ImageIO
  * Poisson Surface Reconstruction
  * Surface Mesh Generation
  * Jet surface smoothing
- Home grown routines:
  * Percolator
### Josh's grains
- Surfaces generated, model results, comparison to experiments

## Elastic Surface Reconstruction
- Packages/Routines Used:
  * Polyhedral Surfaces
  * Fill Polyhedron Hole
  * Pluck connected components
- Home Grown Routines
  * TIFF scanning to (mostly) properly connected facets
  * Elastic surface relaxation
  * Local angles based flattening
#### Takeshi's grains
- Surfaces generated, model results
- Get on to Takeshi about results... also about using his data.

### Volume Meshing
- Delaunay Triangulation
- CGAL gotchas

### Outline Generation
- Polyhedron to projection outline routines.



Electron Holography
===================

## Introduction
- Usefulness in interpreting magnetizations

## Background and Theory
- Derivation from Aharonov and Bohm
- Derivation from Keimpema
- Phi in terms of M

## Block Formulation
- Split integral over M into blocks, a-la FEM formulation.
- Show some results

## Poisson Formulation
- Rearrange Magnetization vs Magnetic Vector Potential into Poisson equation.
- Show some results

## ParaView plugins
- Some discussion of writing a ParaView plugin
- Compiling ParaView from source - ParaView Superbuild - v.nice idea!



Appendices
==========

## The Finite Element Method
- As rough an intro as possible. Basic description of turning an equation
  into a matrix problem by discretisation of an equation over cells.
### Weak Forms
- Outline setting up weak forms for FEM methods
### The Boundary Element Method
- A rough description of turning a Poisson equation into a BEM equation
  by using an integro-differential transformation.

## Integro-Differential Identities
- Set out the three main integro-differential identities used.
- Give examples of using them with weak forms.



Conclusions
===========

## Micromagnetism
- Derivations all in one place.
- - FEM and BEM formulations for energies and effective fields.
- - Should be useful for future students.
- Derivations of multi-phase
- - Energies and effective fields
- - For element-wise and point-wise, as appropriate

- MERRILL passes muMAG 3!
- Good sign that MERRILL is correct.

- Multi-phase BEM feasible
- Multi-phase ferromagnetism feasible
- Multi-phase lattice interactions feasible
- Therefore, Multi-phase modelling with mixed lattices feasible

- Super important for titanomagnetites, oldest recorders on earth
- Super important for any embedded grain.
- - Magnetostrictive effects currently ignored!


## Magnetostriction
- Kittel is wrong!
- - Kittel's uniaxial anisotropy is wrong!!
- - But we've presented here a nicer way to write it down and play with it
- - - although this is pretty much the formulation used by Brown
- - - Frequent use of the compliance tensor in KCS solutions is new though!
- - - Clearly shows Magnetostriction as an offset to the equilibrium deformation

- Fabian and Heider is wrong!(ish) and difficult to implement and fix
- - Lots of discussion after that chapter

- My FEM formulation is fast(er), much simpler, and includes lattice shape
- - It also fails to predict Kittel's uniaxial anisotropy!
- - But it does produce the mechanical deformation of the grain too!
- - Generates effective fields, so it's compatible with MERRILL
- - Suitable for embedding in an infinite matrix!

- Simple solve using FEM magnetostriction immediately shows some interesting
  deformations
- - particularly a Wiedermann-style effect at remanence!

- Solving critical grain sizes for KCS vs KME

- Magnetoplastic coupling discovered here
- - Easily included in FEM formulation
- - Recovers Kittel's uniaxial anisotropy
- - Slightly more convoluted, but easier to reason about
- - In particular, can think in pure elastic system for plastic deformation,
    then add it to the magnetic part
- - A lot of work has been done on elasticity so ... ?!

- A number of predictions have been made
- - List predictions

- A simulation technique for micromagnetic models, directly comparible with
  physical results has been outlined.
- Guidelines for interpreting these images has been outlined
- Therefore, direct measurement of my predictions is possible.



Discussion
==========

- General structure is theory -> computer experiment.
- A LOT of development has been left out of this thesis.
- The thesis doesn't reflect the actual order of discovery.

## Micromagnetism
- MERRILL code is a hot mess
- Code can't really be cleaned up because of target developers
- Code is parallelizable - look at the preconditioner
- Calculators should be split up and made more generic
- Should implement a "dirty" bit in the calculators, to allow for interdependent
  calculators, e.g. Brown-ish demag-magnetostriction.
- I've done some work on an improved parser.
- - More complex, but much better approach
- - - Allows for nested loops, scoped variables, storing function returns
      in variables (e.g. getting the exchange length of a material!)

- Multi-phase implemented in MERRILL
- - Should be taken advantage of


## Magnetostriction
- Found Kittel result from simulations, not the other way around!
- There's a lot of scope for going back over old papers with this method
- A non-FEniCS implementation should be investigated
- - Current implementation rebuilds the matrix on every iteration.
- - Les's code could suffer from a similar issue... worth double checking.
- Comparing LEM results needs more work.
- - Current results show not a huge change from uniform approximation
- - Should be done for more geometries, more sizes, and with NEB
- - I suspect old results will still be reasonable.

- Magnetoplastics is a filthy guess, but it looks like a good one.
- - Definitely worth looking into more.
- Shive & Butler / Merwe approaches should be useful for new materials.
- Should pair with experimental data to verify surface anisotropy strengths.

## Electron Holography
- Bit of a mess, needs a lot of documentation, testing and debugging.
- Also needs to be organized better.
- Needs to be compiled for windows.
- - GMP/MPFR/CGAL prerequisites make that awkward.
- Super useful though!
- Not mentioned in the paper it's a ParaView plugin
- - Took significant work to integrate into ParaView though.
- - ParaView documentation isn't super great.

## Missing bits
- Meshing work
- MEshRRILL!
- - Looking at getting people to use it
- - Easy to use CLI!
- - CLI may be daunting, but it's much clearer than a GUI for simple geometries
- - Programming interface is a bit of a pain, but more reproducible than GUI.
- - Potential for ParaView interface
- Every paper I'm on...
