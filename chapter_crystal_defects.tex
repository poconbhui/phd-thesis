\newcommand{\Def}[0]{\texttt{def}}
\newcommand{\Inc}[0]{\texttt{inc}}
\newcommand{\Div}[0]{\texttt{div}}
\newcommand{\Curl}[0]{\texttt{curl}}

\chapter{Crystal Defects}

A continuum theory of crystal defects was outlined by Kr\"oner (CITE Kontinuumstheorie, Les Hoches).
He outlined a method for calculating the ``internal'' stress of a material due to plastic deformation.
In Karl's approach to magnetostriction, he considered the local deformation due to magnetostriction as a plastic deformation of the material.
Here, we will use the theory do calculate the internal stress due to some crystal defects in terms of dislocation densities, and then plug those stresses into the mechanical part of our magnetostriction equations.
A continuum theory of crystal defects was outlined by Kr\"oner (CITE Kontinuumstheorie, Les Hoches).
He outlined a method for calculating the ``internal'' stress of a material due to plastic deformation.
In Karl's approach to magnetostriction, he considered the local deformation due to
magnetostriction as a plastic deformation of the material.  Here, we will use
the theory do calculate the internal stress due to some crystal defects in terms
of dislocation densities, and then plug those stresses into the mechanical part
of our magnetostriction equations.


\section{Stress due to Continuum Defects}

Much of the difficulty with plastic distortions lies in how to describe the
material after plastic deformation in terms of the material before.
In the elastic case, points that were close before deformation should still
be close after deformation.
The transformation can be described in terms of a continuous vector field
representing the ``displacement''.
However, in the plastic case, since the underlying crystal lattice may be
cut and rearranged, points close together before deformation may not be close
afterwards.
Therefore, the function relating the pre-deformation material to the
post-deformation material is non-continuous and therefore non-differentiable.
So, the usual approach taken by linear elasticity theory cannot be used.


\subsection{The Incompatibility Operator}

A deformation is usually described in terms of a
strain
$\varepsilon_{ij}$ ($\Tensor{\varepsilon}$).
In the case of linear elasticity, the strain is described in terms of a
displacement field $u_{i}$ ($\Vector{u}$)
\begin{align}
    \label{eqn:the_def_operator}
    \Tensor{\varepsilon} & = \Def\ \Vector{u} \\
    (\Def\ \Vector{u})_{ij}
        &= \frac{1}{2} ( \partial_i u_j + \partial_j u_i )
\end{align}
with ``$\Def$'' being the terminology introduced by Kr\"oner to
describe this symmetric gradient operation, to be read eg ``the deformation of
$u$.''

The condition for the strain to be described in terms of this symmetric
gradient of a continuous vector was given by Saint-Venant
\begin{align}
    \label{eqn:the_inc_operator}
    \Inc\ \Tensor{\varepsilon} & = \Tensor{0} \\
    (\Inc\ \Tensor{\varepsilon})_{ij}
        &= -\ e_{iam} e_{jbn}\ \partial_a \partial_b\ \varepsilon_{mn} \\
    \Inc_{ijmn}
        &= -\ e_{iam} e_{jbn}\ \partial_a \partial_b
\end{align}
where
    $e_{ijk}$ is the anti-symmetric tensor
    (
        $e_{ijk} = - e_{jik} = e_{jki}$ and so on for permutations of $i,j,k$
        with $i,j,k \in {0,1,2}$.
        $e_{ijk} = 0$ if $i = j$, $j = k$ or $k = i$ and $e_{012} = 1$
    ).
This should be read eg ``the incompatibility of $\varepsilon$.''

Since
\begin{equation*}
    \varepsilon = \Def\ \Vector{u}
    \Rightarrow
    \Inc\ \Tensor{\varepsilon} = 0
\end{equation*}
for any $\Vector{u}$, it therefore follows that
\begin{equation}
    \Inc\ \Def = \Vector{0}
\end{equation}
From \eqref{eqn:the_inc_operator}, because of the anti-symmetric tensors
\begin{equation}
    (\Div\ \Tensor{\varepsilon})_j = \partial_i \varepsilon_{ij}
\end{equation}
\begin{equation}
    \begin{aligned}
    (\Div\ \Inc\ \Tensor{\varepsilon})_j
        & = \partial_i\ (\Inc\ \Tensor{\varepsilon})_{ij} \\
        & = -\ \partial_i\ e_{iam} e_{jbn}\ 
            \partial_a \partial_b\ \varepsilon_{mn} \\
        & = 0_j
    \end{aligned}
\end{equation}
for any $\Tensor{\varepsilon}$, it therefore follows that
\begin{equation}
    \label{eqn:div_inc_eq_0}
    \Div\ \Inc = \Vector{0}
\end{equation}


\subsection{Strain in terms of Incompatibility}
For any $\Tensor{\varepsilon}$ describing a combination of elastic and plastic
deformations, we have
\begin{equation}
    \Inc\ \Tensor{\varepsilon} = \Tensor{\eta}
\end{equation}
for $\Tensor{\eta} \ne \Tensor{0}$, the elasticity cannot be described in terms of a
pure linear elastic theory.
This represents the case where some internal plastic deformations have
occurred where neighbouring volume elements no longer align in a fluid,
continuous manner.
The typical approach for considering this problem is to take the
``incompatible'' volume elements, elastically deform them (inducing a stress
in response) back into compatible cubes, stitch them back together,
and then allow them to come to an equilibrium elastic deformation.

This is outlined as follows:
\begin{equation}
    \Tensor{\varepsilon}^\texttt{F} =
        \Tensor{\varepsilon}^\texttt{P}
        + \Tensor{\varepsilon}^{\overline{\texttt{P}}}
        + \Tensor{\varepsilon}^\texttt{E}
\end{equation}
where
    $\Tensor{\varepsilon}^\texttt{F}$ is the final strain,
    $\Tensor{\varepsilon}^\texttt{P}$ is the strain due to plastic deformation,
    $\Tensor{\varepsilon}^{\overline{\texttt{P}}}$ is the elastic deformation
    we perform after the incompatible plastic deformation to make the material
    compatible again
    and $\Tensor{\varepsilon}^\texttt{E}$ is the elastic deformation necessary
    to bring the material to equilibrium after
    $\Tensor{\varepsilon}^{\overline{\texttt{P}}}$ has been applied.

The obvious choice for $\Tensor{\varepsilon}^{\overline{\texttt{P}}}$ is
$- \Tensor{\varepsilon}^\texttt{P}$.
This also has some nice properties, namely
\begin{equation}
    \Inc\ \Tensor{\varepsilon}^{\overline{\texttt{P}}} =
        - \Inc\ \Tensor{\varepsilon}^\texttt{P} =
        - \Tensor{\eta}
\end{equation}
Since the final state of the material is a compatible deformation, ie
there are no cracks, we can say
\begin{align}
    \Inc\ \Tensor{\varepsilon}^\texttt{F} = 0 \\
    \Rightarrow \Inc\ \Tensor{\varepsilon}^\texttt{E} = 0
\end{align}

Finally, writing the total elastic strain that will result in an
elastic stress as
\begin{equation}
    \Tensor{\varepsilon}^\texttt{T} =
        \Tensor{\varepsilon}^{\overline{\texttt{P}}}
        + \Tensor{\varepsilon}^\texttt{E}
\end{equation}
we can say of the total elastic strain
\begin{equation}
    \label{eqn:elastic_incompatibility}
    \Inc\ \Tensor{\varepsilon}^\texttt{T} = - \Tensor{\eta}
\end{equation}

From here on, we will refer to $\Tensor{\varepsilon}^\texttt{T}$ simply
as $\Tensor{\varepsilon}$.


\subsection{Stress in terms of Incompatibility}
It is important to note that, by definition, plastic deformations
do not induce stress in a material.
However, the elastic deformations necessary to keep the material compatible
after a plastic deformation can.

We define the elastic stress tensor $\sigma_{ij}$ ($\Tensor{\sigma}$) defined
\begin{equation}
    \Tensor{\sigma} = \frac{\partial E}{\partial \Tensor{\varepsilon}}
\end{equation}
For small strains, the energy is assumed to be in the form
$E = \frac{1}{2} \Tensor{\varepsilon} : \Tensor{C} : \Tensor{\varepsilon}
    + \Tensor{\varepsilon^0} : \Tensor{\varepsilon}$
leading to the Hooke relation
\begin{equation}
    \label{eqn:general_hooke_relation}
    \begin{aligned}
        \Tensor{\sigma}
            &= \Tensor{C} : \Tensor{\varepsilon}  + \Tensor{\varepsilon^0}\\
        \sigma_{ij}     &= C_{ijkl} \varepsilon_{kl} + \varepsilon^0_{ij}
    \end{aligned}
\end{equation}
with $C_{ijkl}$ the elastic stiffness tensor,
and $\varepsilon^0_{ij}$ the equilibrium strain tensor.

We will assume $\Tensor{\varepsilon^0} = \Tensor{0}$ and use the inverse
elastic stiffness tensor $\Tensor{S}$ where
\begin{equation}
    S_{ijkl} C_{klmn}
        = \frac{1}{2} (\delta_{im} \delta_{jn} + \delta_{in} \delta_{jm})
\end{equation}
and get the relations
\begin{align}
    \label{eqn:hooke_relation}
    \Tensor{\sigma} &= \Tensor{C} : \Tensor{\varepsilon} \\
    \label{eqn:inverse_hooke_relation}
    \Tensor{\varepsilon} &= \Tensor{S} : \Tensor{\sigma}
\end{align}

By considering energy minimization in an elastic theory, one can derive
the mechanical equilibrium condition
\begin{equation}
    \Div\ \Tensor{\sigma} = \Vector{0}
\end{equation}

As with magnetism, where
\begin{align*}
    \Div\ \Vector{B} &= 0 \\
    \partial_i\ B_i &= 0 \\
    \Rightarrow
    \Vector{B} &= \Curl\ \Vector{A} \\
    B_i &= e_{ijk} \partial_j A_k \\
\end{align*}
for some $\Vector{A}$ since $\Div\ \Curl\ \Vector{A} = 0$,
we can say
\begin{align}
    \Div\ \Tensor{\sigma} &= \Vector{0} \\
    \label{eqn:stress_and_stress_tensor_relation}
    \Rightarrow
    \Tensor{\sigma} &= \Inc \Tensor{\chi}
\end{align}
for some $\chi_{ij}$ ($\Tensor{\chi}$) since
$\Div\ \Inc\ \Tensor{\chi} = \Vector{0}$.

Just as $\Vector{A}$ is not fully defined from this relation, neither is
$\Tensor{\chi}$.
Indeed, adding any other tensor such that $\Inc\ \Tensor{\xi} = 0$
will suffice.
In general, if $\Tensor{\chi}$ is a solution to
$\Div\ \Tensor{\sigma} = 0$
\begin{equation}
    \chi'_{ij}
        = \chi_{ij} + \partial_i \lambda_j + \partial_j \lambda_i + c_{ij}
\end{equation}
is as well, where $\Vector{\lambda}$ is an arbitrary vector valued function
and $c_{ij}$ is a constant.
This looks analogous to the gauge freedoms of $\Vector{A}$, except
for the extra index $j$, and symmetric application of $\Vector{\partial}$.
It therefore follows that whatever gauge transformation and fixing
tricks work with $\Vector{A}$ should work similarlty for $\Tensor{\chi}$
as well.

Tying this back to the incompatibility, using
\eqref{eqn:elastic_incompatibility}, \eqref{eqn:hooke_relation}
and \eqref{eqn:stress_and_stress_tensor_relation}, and defining the
inverse elastic stiffness tensor,
\begin{equation}
    S_{ijkl} C_{klmn} = \delta_{im} \delta_{jn}
\end{equation}
\begin{equation}
    \begin{aligned}
    \Inc_{ijkl} \chi_{kl}
        &= \sigma_{ij} \\
        &= C_{ijmn}\ \varepsilon_{mn}
    \end{aligned}
\end{equation}
\begin{align}
    S_{ghij} \Inc_{ijkl}\ \chi_{mn} &= \varepsilon_{gh} \\
    \Inc_{efgh} S_{ghij} \Inc_{ijkl} \chi_{mn} &= \Inc_{efgh} \varepsilon_{gh} \\
    \Inc_{efgh} S_{ghij} \Inc_{ijkl} \chi_{mn} &= \eta_{ef} \\
    \label{eqn:stress_potential_and_incompatibility_relation}
    \Inc \Tensor{S} \Inc \Tensor{\chi} &= \Tensor{\eta}
\end{align}

As you can imagine, when
\eqref{eqn:stress_potential_and_incompatibility_relation} is expanded in full,
it's quite a mess.



\section{Finite Element Formulation}

Rather than dealing directly with the expansion of
\eqref{eqn:stress_potential_and_incompatibility_relation}
we will split the equation up into a coupled PDE
\begin{subequations}
\begin{align}
    \Inc \Tensor{\varepsilon}     &= \Tensor{\eta} \\
    \label{eqn:early_coupled_incompatibility_eqns_b}
    \Tensor{S} \Inc \Tensor{\chi} &= \Tensor{\varepsilon}
\end{align}
\end{subequations}
Expanding this in terms of derivatives and indices, multiplying 
\eqref{eqn:early_coupled_incompatibility_eqns_b}
by $\Tensor{C}$, multiplying/contracting the equations by $Y$ and $Z$,
and taking integrals over a domain $\Omega$,
we get the beginnings of a weak form
\begin{subequations}
\begin{align}
    \label{eqn:incompatibility_weak_form_pair_initial_a}
    \int_\Omega
        Y_{ij} e_{iam} e_{jbn} \partial_a \partial_b \varepsilon_{mn} Y_{ij} dx
        &= \int_\Omega Y_{ij} \eta_{ij} dx \\
    \label{eqn:incompatibility_weak_form_pair_initial_b}
    \int_\Omega Z_{ij} e_{iam} e_{jbn} \partial_a \partial_b \chi_{mn} dx
        &= \int_\Omega Z_{ij} C_{ijkl} \varepsilon_{kl} dx
\end{align}
\end{subequations}

Using the vector identity
\begin{equation}
    \label{eqn:div_curl_cross_vector_identity}
    \begin{aligned}
        \Div (\Vector{A} \times \Vector{B})
            = B \cdot (\Curl \Vector{A}) - A \cdot (\Curl \Vector{B})\\
        \partial_i (e_{ijk} A_j B_k)
            = B_i e_{ijk} (\partial_j A_k) - A_i e_{ijk} (\partial_j B_k)
    \end{aligned}
\end{equation}
with $A_j = \chi_{jq}$ and $B_k = Z_{kr}$
and using the divergance theorem
\begin{equation}
    \int_\Omega
        \partial_{i_q} T_{i_1 \ldots i_q \ldots q_n} dx
        = \int_{\partial \Omega} n_{i_q} T_{i_1 \ldots i_q \ldots i_n} dS
\end{equation}
we get
\begin{equation}
    \begin{aligned}
    \int_\Omega Z_{ij} e_{iam} e_{jbn} \partial_a \partial_b \chi_{mn} dx
        = & \int_\Omega
            (\partial_a Z_{mj}) e_{iam} e_{jbn} (\partial_b \chi_{in}) dx \\
          & + \int_{\partial \Omega}
            Z_{ij} e_{iam} e_{jbn} n_a \partial_b \chi_{mn} dS
    \end{aligned}
\end{equation}
and symetrizing the result of the surface integral by applying the same
operation to the $e_{jbn}$ contraction and with $Z_{ij} = Z_{ji}$:
\begin{equation}
    \begin{aligned}
    \int_\Omega Z_{ij} e_{iam} e_{jbn} \partial_a \partial_b \chi_{mn} dx
        = & \int_\Omega
            (\partial_a Z_{jm}) e_{iam} e_{jbn} (\partial_b \chi_{in}) dx \\
          & + \int_{\partial \Omega}
            Z_{ij} e_{iam} e_{jbn} (n_a \partial_b + n_b \partial_a) \chi_{mn} dS
    \end{aligned}
\end{equation}

Applying this transformation to
\eqref{eqn:incompatibility_weak_form_pair_initial_a} and
\eqref{eqn:incompatibility_weak_form_pair_initial_b},
we get
\begin{subequations}
\begin{align}
    \label{eqn:incompatibility_weak_form_pair_transformed_a}
    \begin{split}
        \int_\Omega
            (\partial_a Y_{mj}) e_{iam} e_{jbn} (\partial_b \varepsilon_{in}) dx
        + \int_{\partial \Omega}
            Y_{ij} e_{iam} e_{jbn} (n_a \partial_b + n_b \partial_a)
                \varepsilon_{mn} dS \\
        = \int_\Omega Y_{ij} \eta_{ij} dx
    \end{split} \\
    \label{eqn:incompatibility_weak_form_pair_transformed_b}
    \begin{split}
        \int_\Omega
            (\partial_a Z_{mj}) e_{iam} e_{jbn} (\partial_b \chi_{in}) dx
        + \int_{\partial \Omega}
            Z_{ij} e_{iam} e_{jbn} (n_a \partial_b + n_b \partial_a)
                \chi_{mn} dS \\
        = \int_\Omega Z_{ij} C_{ijkl} \varepsilon_{kl} dx
    \end{split}
\end{align}
\end{subequations}

Let's consider boundary conditions to deal with those boundary integrals
and get a nice, well defined problem.

The stress normal to the surface of the grain is zero.
This can be written
\begin{subequations}
\begin{align}
    n_i \sigma_{ij} &= n_i e_{iam} e_{jbn} \partial_a \partial_b \chi_{mn} = 0
        \quad\texttt{on}\ \partial \Omega \\
    n_j \sigma_{ij} &= n_j e_{iam} e_{jbn} \partial_a \partial_b \chi_{mn} = 0
        \quad\texttt{on}\ \partial \Omega
\end{align}
\end{subequations}
and using the identity \eqref{eqn:div_curl_cross_vector_identity},
\begin{subequations}
\begin{align}
    \label{eqn:incompatibility_boundary_conditions_on_omega_a}
    \begin{aligned}
    n_i \sigma_{ij}
    &= (e_{jbn} \partial_b \chi_{in}) e_{iam} (\partial_a n_m)
        - \partial_i (e_{iam} n_a (e_{jbn} \partial_b \chi_{mn})) \\
    &= e_{iam} e_{jbn} (\partial_b \chi_{in}) (\partial_a n_m)
        - \partial_i (e_{iam} e_{jbn} n_a \partial_b \chi_{mn}) \\
    &= 0
    \end{aligned} \\
    \label{eqn:incompatibility_boundary_conditions_on_omega_b}
    \begin{aligned}
    n_j \sigma_{ij}
    &= (e_{iam} \partial_a \chi_{mj}) e_{jbn} (\partial_b n_n)
        - \partial_j (e_{jbn} n_b (e_{iam} \partial_a \chi_{mn})) \\
    &= e_{iam} e_{jbn} (\partial_a \chi_{mj}) (\partial_b n_n)
        - \partial_j (e_{iam} e_{jbn} n_b \partial_a \chi_{mn}) \\
    &= 0
    \end{aligned}
\end{align}
\end{subequations}

Now for something more dubious. We consider the surface $\partial \Omega$
as a polyhedron. That is the surface is a union of triangles
with $T_i$ the $i$th triangle, $E_i$ the set of its edges and $V_i$
the set of its vertices
\begin{equation}
    \begin{aligned}
        \partial \Omega &= \cup_i T_i \\
        T_i \cap T_j    &\subset (E_i \cap E_j) \cup (V_i \cap V_j) \quad\forall\ i,j
    \end{aligned}
\end{equation}
which is to say triangles overlap only in shared edges and vertices
so that
\begin{equation}
    \int_\Omega f(x) dx = \int_{\cup_i T_i} f(x) dx = \sum_i \int_{T_i} f(x) dx
\end{equation}
We do this so the normal on each triangle $T_i$ is a constant function and so
\begin{equation}
    \partial_i n_j = 0_{ij} \quad\texttt{on}\ T_i
\end{equation}
The only place where this value changes is on the shared 1d edges and the
shared 0d vertices. However, these points can be left out of the integral
without affecting the result, since they have measure zero on the 2d domain
of inegration.

We can now say for
\eqref{eqn:incompatibility_boundary_conditions_on_omega_a}
and
\eqref{eqn:incompatibility_boundary_conditions_on_omega_b}
\begin{subequations}
\begin{align}
    \partial_i (e_{iam} e_{jbn} n_a \partial_b \chi_{mn}) &= 0
        \quad\texttt{on}\ T_i \\
    \partial_j (e_{iam} e_{jbn} n_b \partial_a \chi_{mn}) &= 0
        \quad\texttt{on}\ T_i
\end{align}
\end{subequations}
which is satisfied if 
\begin{subequations}
\begin{align}
    e_{iam} e_{jbn} n_a \partial_b \chi_{mn} = \texttt{constant} \\
    e_{iam} e_{jbn} n_b \partial_a \chi_{mn} = \texttt{constant} \\
    \Rightarrow
    \label{eqn:incompatibibility_chi_neumann_boundary_condition}
    e_{iam} e_{jbn} (n_a \partial_b + n_b \partial_a) \chi_{mn}
        = \texttt{constant} \\
\end{align}
\end{subequations}

for $\chi_{ij}$ a solution to
$e_{iam} e_{jbn} n_a \partial_b \chi_{mn} = \alpha_{ij}$
any
$\xi_{ij} = \chi_{ij} + \partial_i \lambda_j + \partial_j \lambda_i + C_{ij}$
is also a solution.
In particular, we want 
$e_{iam} e_{jbn} n_a \partial_b \xi_{mn} = 0_{ij}$
so we will choose a $\Vector{\lambda}$ to solve
\begin{equation}
    e_{iam} e_{jbn} n_a \partial_b \partial_m \lambda_n
    + e_{iam} e_{jbn} n_a \partial_b \partial_n \lambda_m
    = \alpha_{ij}
\end{equation}
Since $\partial_b \partial_n = \partial_n \partial_b$, we have
\begin{equation}
    \begin{aligned}
        e_{jbn} \partial_b \partial_n
            &= - e_{jnb} \partial_b \partial_n \\
            &= - e_{jnb} \partial_n \partial_b \\
            &= - e_{jbn} \partial_b \partial_n
    \end{aligned}
\end{equation}
and since $e_{jbn} \partial_b \partial_n = - e_{jbn} \partial_b \partial_n$,
we must have $e_{jbn} \partial_b \partial_n = 0$.
Simplifying our previous statement,
\begin{equation}
    e_{iam} e_{jbn} n_a \partial_b \partial_m \lambda_n = \alpha_{ij}
\end{equation}
for which there should be some solution for $\Vector{\lambda}$, and so,
$e_{iam} e_{jbn} n_a \partial_b \xi_{mn} = 0_{ij}$ on $\partial \Omega$
is a reasonable gauge fixing condition for us to impose.
And so, a $\chi_{ij}$ can be chosen such that the $\texttt{constant}$ of 
\eqref{eqn:incompatibibility_chi_neumann_boundary_condition}
can be chosen to be 0.

For the boundary conditions for $\Tensor{\varepsilon}$, we have
\begin{align}
    n_i C_{ijkl} \varepsilon_{kl} = n_i \sigma_{ij} = 0_j
        \quad\texttt{on}\ \partial\Omega
\end{align}
